**Introduction** 

We want to predict displacement of people linked to catastrophic events by through machine learning.
For this, we enrich hazard data with geographical, infrastructure and geopolitical data a

**Useful Links:**

*  [H4G HS19 Edition Google Drive](https://drive.google.com/drive/u/0/folders/0AM7mpR5vOlDDUk9PVA)
*  [Internal Displacement Monitoring Centre Website](http://www.internal-displacement.org)
*  [IDMC Google Doc about sources](https://docs.google.com/document/d/1TaKPe7CfJ6mBzC7hcieL8PqhC1LSxupqt6vPs09QqZk/edit)
*  [IDMC displacement data](http://www.internal-displacement.org/database/displacement-data)
*  [Online GEOjson tool for data visualization](http://geojson.tools/)
*  [GDACS event description](https://www.gdacs.org/resources.aspx?eventid=37639&episodeid=8&eventtype=TC)


**Workshop Dates:**

| Event | Date | Location |
| ------ | ------ | ------ |
| Kick-off |  02.10.19 (Wed) , 17:30 - 21:30| SPH |
| Hackday 1 |  05.10.19 (Sat) , 09:00 - 18:00| SPH |
| Workshop 1: Agile Workshop |  09.10.19 (Wed) , 17:30 - 21:30| SPH |
| Workshop 2: Feedback Workshop |  16.10.19 (Wed) , 17:30 - 21:30| SPH |
| Workshop 3: Impact Workshop |  23.10.19 (Wed) , 17:30 - 21:30| SPH |
| Workshop 4: Pitch Workshop |  30.10.19 (Wed) , 17:30 - 21:30| SPH |
| Hackday 2 |  10.11.19 (Sat) , 09:00 - 18:00| SPH |
| Workshop 5: Dry-run Workshop |  13.11.19 (Wed) , 17:30 - 21:30| SPH |
| Final Event |  20.11.19 (Wed) , 17:30 - 21:30| SPH |

**Folder Structure**

```
├── api_request [46 entries exceeds filelimit, not opening dir]
├── climada -> /media/lionel/data/climada_python/climada/
├── ConfigurationClass.py
├── configuration_local.toml
├── configuration_template.toml
├── data
│   ├── Additional Features for Machine Learning
│   ├── exposure
│   │   ├── folder_size_11GB.txt
│   │   ├── OutputsCapra1x1
│   │   ├── pt_5x5.shp
│   │   └── pt_5x5.shx
│   ├── gdacs
│   │   ├── EQ
│   │   ├── FL
│   │   └── TC
│   ├── hazard
│   │   ├── Flood
│   │   └── folder_size_190MB.txt
│   ├── idmc
│   │   ├── idmc_disaster_all_dataset.xlsx
│   │   └── idmc_displacement_all_dataset.xlsx
│   ├── open_fvu.py
│   ├── open_shape_file.py
│   ├── README.md
│   ├── risk
│   │   └── folder_size_40MB.txt
│   └── vulnerability
│       └── folder_size_12MB.txt
├── environment.yml
├── notebooks
├── out
├── pipeline
│   ├── analyse_pipe.py
│   ├── climada_pipe.py
│   └── identified_labels.py
├── README.md
├── reports
│   └── figures
├── results
│   ├── models
│   └── outputs
├── scores
├── src
│   ├── __init__.py
│   ├── main.py
│   ├── ml_machinery.py
│   └── utils
│       ├── evaluation.py
│       ├── exploration.py
│       ├── __init__.py
│       ├── subtasks_no_climada.py
│       └── subtasks.py
├── tutorial
│   ├── baseline.py
│   ├── Exposure_from_csv.py
│   ├── Exposure_from_GeoPandas.py
│   ├── get_prediction.py
│   ├── join_gdacs_to_idmc.py
│   ├── match_exposure.py
│   ├── parse_gdacs_hazard.py
│   ├── parse_nasa_haz.py
│   ├── Read_flooding.py
│   └── README.md

```


**Python environment**

Open a console and move to the folder where your environment file is stored.

* create a python env based on a list of packages from environment.yml

  ```conda env create -f environment.yml -n env_your_proj```

* update a python env based on a list of packages from environment.yml

  ```conda env update -f environment.yml -n env__your_proj```

* activate the env  

  ```activate env_your_proj```
  
* in case of an issue clean all the cache in conda

   ```conda clean -a -y```

* delete the env to recreate it when too many changes are done  

  ```conda env remove -n env_your_proj```
  
  