# Description of the data

This section will collect all information related to (potential) data sources including but not limited to:

1. General overview of available data sets (preferrably in the form of an UML diagram)
2. Functions to open various data extensions
3. Variable explanation
4. Some interactive visualizations (jupyter notebooks)
4. Links to other relevant data sources

## Functions to open various data extensions 

### Shape file formats
[Wikipedia](https://en.wikipedia.org/wiki/Shapefile) 

1. **.shp**: the file that contains the geometry for all features.   ([GIS in Python](https://www.earthdatascience.org/workshops/gis-open-source-python/intro-vector-data-python/))
2. **.shx**: the file that indexes the geometry.
3. **.dbf**: the file that stores feature attributes in a tabular format.
 

[Tutorial](https://www.earthdatascience.org/workshops/gis-open-source-python/intro-vector-data-python/): open GIS files with python.\\
Sample to open a shapefile (add more detailed dataset description later): open shape file.py\\
These types of files can be read by climada directly as demonstrated in Exposure from GeoPandas.py

### GRID

### MDB

Explain the difference between [.MDB and .LDB files](https://fileinfo.com/extension/ldb)

**Currently a bit stuck**

## Data Description

https://drive.google.com/drive/folders/1hmF-ejBoqKZNEZVPxS06ECR6By1cOgXH



## Data Sources

1. First overview of interesting data sources can be found here (created by Mian): https://docs.google.com/document/d/1TaKPe7CfJ6mBzC7hcieL8PqhC1LSxupqt6vPs09QqZk/edit
2. Proposal from Vincent: https://worldview.earthdata.nasa.gov/?v=12.855619502331745,-36.31221960146352,58.37526333815913,-9.605725958341068&t=2013-01-25-T12%3A00%3A00Z&l=MODIS_Terra_CorrectedReflectance_Bands721,Coastlines


## Getting additional features for Machine Learning

We used additional features for our machine learning algorithms. Those come from two sources. How to get this data?

* Data World Bank, for global measures on a country state (GDP, Population, Surface…)
    * [Example] (https://data.worldbank.org/indicator/NY.GDP.MKTP.CD). Then just click on "Download" CSV or Excel. For another measure, just tap in the research bar the name of the new measure you want (eg. "Population").

* Disaster Risk Management Knowledge Center, from the European Commission
    * Search for "INFORM GRI 2020 TREND 2011-2020 (13/9/2019)" or updated version
    * Click on this and you'll can download the Excel file.
    * See the "Guidance Note" or "Full methodology" pdf for a better understanding of the meaning of the variables
