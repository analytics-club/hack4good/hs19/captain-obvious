#!/usr/bin/python3
#encoding=utf8
#jbaumer@student.ethz.ch

import pandas as pd

############################################################################################################
"""
what is this file for?
The following functions take a country (iso3 value) and a year as an input and output additional information to that country in a certain year
The actual code takes some country data (e.g. the climada prediction) as a csv file and makes a dataframe, to which the add. features are added
(each feature in a single additional column)
"""

def get_attribute_for_country(country, year):
    #only working for years between 2011 and 2018
    pathRawData = './Final_AllCountries_PerYearV3.xlsx'
    # read excel files, sheet per sheet
    rawDataGDP = pd.read_excel(pathRawData, sheet_name="GDP", index_col="ISO3")
    rawDataSocio_Economic_Vulnerability = pd.read_excel(pathRawData, sheet_name="Socio_Economic_Vulnerability", index_col="ISO3")
    rawDataPhysical_Infrastructure = pd.read_excel(pathRawData, sheet_name="Physical_Infrastructure", index_col="ISO3")
    rawDataSurface = pd.read_excel(pathRawData, sheet_name="Surface", index_col="ISO3")
    rawDataPopulation = pd.read_excel(pathRawData, sheet_name="Population", index_col="ISO3")

    country = str(country)
    if year < 2011:
        print ("no additional information (only for years between 2011 and 2018)")
    elif year > 2018:
        print("no additional information (only for years between 2011 and 2018)")
    else:
        #returns list of extra country attributes for country names
        specificCountryAllYears_GDP = rawDataGDP.loc[country, year]
        specificCountryAllYears_Socio_Economic_Vulnerability = rawDataSocio_Economic_Vulnerability.loc[country, year]
        specificCountryAllYears_Physical_Infrastructure = rawDataPhysical_Infrastructure.loc[country, year]
        specificCountryAllYears_Surface = rawDataSurface.loc[country, 2018]    # assumption that country surface does not change over years
        specificCountryAllYears_Population = rawDataPopulation.loc[country, year]

        addInfoList = [specificCountryAllYears_GDP, specificCountryAllYears_Socio_Economic_Vulnerability, specificCountryAllYears_Physical_Infrastructure, specificCountryAllYears_Surface, specificCountryAllYears_Population]
    return addInfoList




def get_GDP_for_country(country, year):
    #only working for years between 2011 and 2018
    pathRawData = './Final_AllCountries_PerYearV3.xlsx'
    # read excel files, sheet per sheet
    rawDataGDP = pd.read_excel(pathRawData, sheet_name="GDP", index_col="ISO3")
    #rawDataSocio_Economic_Vulnerability = pd.read_excel(pathRawData, sheet_name="Socio_Economic_Vulnerability", index_col="ISO3")
    #rawDataPhysical_Infrastructure = pd.read_excel(pathRawData, sheet_name="Physical_Infrastructure", index_col="ISO3")
    #rawDataSurface = pd.read_excel(pathRawData, sheet_name="Surface", index_col="ISO3")
    #rawDataPopulation = pd.read_excel(pathRawData, sheet_name="Population", index_col="ISO3")

    country = str(country)
    if year < 2011:
        print ("no additional information (only for years between 2011 and 2018)")
    elif year > 2018:
        print("no additional information (only for years between 2011 and 2018)")
    else:
        #returns list of extra country attributes for country names
        specificCountryAllYears_GDP = rawDataGDP.loc[country, year]
        #specificCountryAllYears_Socio_Economic_Vulnerability = rawDataSocio_Economic_Vulnerability.loc[country, year]
        #specificCountryAllYears_Physical_Infrastructure = rawDataPhysical_Infrastructure.loc[country, year]
        #specificCountryAllYears_Surface = rawDataSurface.loc[country, 2018]    # assumption that country surface does not change over years
        #specificCountryAllYears_Population = rawDataPopulation.loc[country, year]

        #addInfoList = [specificCountryAllYears_GDP, specificCountryAllYears_Socio_Economic_Vulnerability, specificCountryAllYears_Physical_Infrastructure, specificCountryAllYears_Surface, specificCountryAllYears_Population]
        addInfoGDP = [specificCountryAllYears_GDP]
    return addInfoGDP

def get_Socio_Economic_Vulnerability_for_country(country, year):
    #only working for years between 2011 and 2018
    pathRawData = './Final_AllCountries_PerYearV3.xlsx'
    # read excel files, sheet per sheet
    #rawDataGDP = pd.read_excel(pathRawData, sheet_name="GDP", index_col="ISO3")
    rawDataSocio_Economic_Vulnerability = pd.read_excel(pathRawData, sheet_name="Socio_Economic_Vulnerability", index_col="ISO3")
    #rawDataPhysical_Infrastructure = pd.read_excel(pathRawData, sheet_name="Physical_Infrastructure", index_col="ISO3")
    #rawDataSurface = pd.read_excel(pathRawData, sheet_name="Surface", index_col="ISO3")
    #rawDataPopulation = pd.read_excel(pathRawData, sheet_name="Population", index_col="ISO3")

    country = str(country)
    if year < 2011:
        print ("no additional information (only for years between 2011 and 2018)")
    elif year > 2018:
        print("no additional information (only for years between 2011 and 2018)")
    else:
        #returns list of extra country attributes for country names
        #specificCountryAllYears_GDP = rawDataGDP.loc[country, year]
        specificCountryAllYears_Socio_Economic_Vulnerability = rawDataSocio_Economic_Vulnerability.loc[country, year]
        #specificCountryAllYears_Physical_Infrastructure = rawDataPhysical_Infrastructure.loc[country, year]
        #specificCountryAllYears_Surface = rawDataSurface.loc[country, 2018]    # assumption that country surface does not change over years
        #specificCountryAllYears_Population = rawDataPopulation.loc[country, year]

        #addInfoList = [specificCountryAllYears_GDP, specificCountryAllYears_Socio_Economic_Vulnerability, specificCountryAllYears_Physical_Infrastructure, specificCountryAllYears_Surface, specificCountryAllYears_Population]
        addInfoSocio_Economic_Vulnerability = [specificCountryAllYears_Socio_Economic_Vulnerability]
    return addInfoSocio_Economic_Vulnerability

def get_Physical_Infrastructure_for_country(country, year):
    #only working for years between 2011 and 2018
    pathRawData = './Final_AllCountries_PerYearV3.xlsx'
    # read excel files, sheet per sheet
    #rawDataGDP = pd.read_excel(pathRawData, sheet_name="GDP", index_col="ISO3")
    #rawDataSocio_Economic_Vulnerability = pd.read_excel(pathRawData, sheet_name="Socio_Economic_Vulnerability", index_col="ISO3")
    rawDataPhysical_Infrastructure = pd.read_excel(pathRawData, sheet_name="Physical_Infrastructure", index_col="ISO3")
    #rawDataSurface = pd.read_excel(pathRawData, sheet_name="Surface", index_col="ISO3")
    #rawDataPopulation = pd.read_excel(pathRawData, sheet_name="Population", index_col="ISO3")

    country = str(country)
    if year < 2011:
        print ("no additional information (only for years between 2011 and 2018)")
    elif year > 2018:
        print("no additional information (only for years between 2011 and 2018)")
    else:
        #returns list of extra country attributes for country names
        #specificCountryAllYears_GDP = rawDataGDP.loc[country, year]
        #specificCountryAllYears_Socio_Economic_Vulnerability = rawDataSocio_Economic_Vulnerability.loc[country, year]
        specificCountryAllYears_Physical_Infrastructure = rawDataPhysical_Infrastructure.loc[country, year]
        #specificCountryAllYears_Surface = rawDataSurface.loc[country, 2018]    # assumption that country surface does not change over years
        #specificCountryAllYears_Population = rawDataPopulation.loc[country, year]

        #addInfoList = [specificCountryAllYears_GDP, specificCountryAllYears_Socio_Economic_Vulnerability, specificCountryAllYears_Physical_Infrastructure, specificCountryAllYears_Surface, specificCountryAllYears_Population]
        addInfoPhysical_Infrastructure = [specificCountryAllYears_Physical_Infrastructure]
    return addInfoPhysical_Infrastructure

def get_Surface_for_country(country, year):
    #only working for years between 2011 and 2018
    pathRawData = './Final_AllCountries_PerYearV3.xlsx'
    # read excel files, sheet per sheet
    #rawDataGDP = pd.read_excel(pathRawData, sheet_name="GDP", index_col="ISO3")
    #rawDataSocio_Economic_Vulnerability = pd.read_excel(pathRawData, sheet_name="Socio_Economic_Vulnerability", index_col="ISO3")
    #rawDataPhysical_Infrastructure = pd.read_excel(pathRawData, sheet_name="Physical_Infrastructure", index_col="ISO3")
    rawDataSurface = pd.read_excel(pathRawData, sheet_name="Surface", index_col="ISO3")
    #rawDataPopulation = pd.read_excel(pathRawData, sheet_name="Population", index_col="ISO3")

    country = str(country)
    if year < 2011:
        print ("no additional information (only for years between 2011 and 2018)")
    elif year > 2018:
        print("no additional information (only for years between 2011 and 2018)")
    else:
        #returns list of extra country attributes for country names
        #specificCountryAllYears_GDP = rawDataGDP.loc[country, year]
        #specificCountryAllYears_Socio_Economic_Vulnerability = rawDataSocio_Economic_Vulnerability.loc[country, year]
        #specificCountryAllYears_Physical_Infrastructure = rawDataPhysical_Infrastructure.loc[country, year]
        specificCountryAllYears_Surface = rawDataSurface.loc[country, 2018]    # assumption that country surface does not change over years
        #specificCountryAllYears_Population = rawDataPopulation.loc[country, year]

        #addInfoList = [specificCountryAllYears_GDP, specificCountryAllYears_Socio_Economic_Vulnerability, specificCountryAllYears_Physical_Infrastructure, specificCountryAllYears_Surface, specificCountryAllYears_Population]
        addInfoSurface = [specificCountryAllYears_Surface]
    return addInfoSurface

def get_Population_for_country(country, year):
    #only working for years between 2011 and 2018
    pathRawData = './Final_AllCountries_PerYearV3.xlsx'
    # read excel files, sheet per sheet
    #rawDataGDP = pd.read_excel(pathRawData, sheet_name="GDP", index_col="ISO3")
    #rawDataSocio_Economic_Vulnerability = pd.read_excel(pathRawData, sheet_name="Socio_Economic_Vulnerability", index_col="ISO3")
    #rawDataPhysical_Infrastructure = pd.read_excel(pathRawData, sheet_name="Physical_Infrastructure", index_col="ISO3")
    #rawDataSurface = pd.read_excel(pathRawData, sheet_name="Surface", index_col="ISO3")
    rawDataPopulation = pd.read_excel(pathRawData, sheet_name="Population", index_col="ISO3")

    country = str(country)
    if year < 2011:
        print ("no additional information (only for years between 2011 and 2018)")
    elif year > 2018:
        print("no additional information (only for years between 2011 and 2018)")
    else:
        #returns list of extra country attributes for country names
        #specificCountryAllYears_GDP = rawDataGDP.loc[country, year]
        #specificCountryAllYears_Socio_Economic_Vulnerability = rawDataSocio_Economic_Vulnerability.loc[country, year]
        #specificCountryAllYears_Physical_Infrastructure = rawDataPhysical_Infrastructure.loc[country, year]
        #specificCountryAllYears_Surface = rawDataSurface.loc[country, 2018]    # assumption that country surface does not change over years
        specificCountryAllYears_Population = rawDataPopulation.loc[country, year]

        #addInfoList = [specificCountryAllYears_GDP, specificCountryAllYears_Socio_Economic_Vulnerability, specificCountryAllYears_Physical_Infrastructure, specificCountryAllYears_Surface, specificCountryAllYears_Population]
        addInfoPopulation = [specificCountryAllYears_Population]
    return addInfoPopulation









filepathWithoutAddInfo = './example1_withoutAddData.csv'
fileWithoutAddInfo = pd.read_csv(filepathWithoutAddInfo)#, index_col="iso3")

#shortening dataframe, so that it can be used as an input to get_attribute_for_country
#deleting all rows for which country is not in range [2011,2018], as we only have additional Information in these years.
shortenedWithoutInfo = fileWithoutAddInfo[(fileWithoutAddInfo['year'] <= 2018) & (fileWithoutAddInfo['year'] >= 2011)]
#And_df = df[(df['Rating']>8) & (df['Votes']>100000)]
#print(fileWithoutAddInfo)
#print(shortenedWithoutInfo, "short before")


shortenedWithoutInfo['[GDP, Socio-Economic-Vulnerability, Physical Infrastructure, Surface, Population]'] = shortenedWithoutInfo.apply(lambda x: get_attribute_for_country(x['iso3'], x['year']), axis=1)
shortenedWithoutInfo['[GDP]'] = shortenedWithoutInfo.apply(lambda x: get_GDP_for_country(x['iso3'], x['year']), axis=1)

shortenedWithoutInfo['[Socio_Economic_Vulnerability]'] = shortenedWithoutInfo.apply(lambda x: get_Socio_Economic_Vulnerability_for_country(x['iso3'], x['year']), axis=1)
shortenedWithoutInfo['[Physical_Infrastructure]'] = shortenedWithoutInfo.apply(lambda x: get_Physical_Infrastructure_for_country(x['iso3'], x['year']), axis=1)
shortenedWithoutInfo['[Surface]'] = shortenedWithoutInfo.apply(lambda x: get_Surface_for_country(x['iso3'], x['year']), axis=1)
shortenedWithoutInfo['[Population]'] = shortenedWithoutInfo.apply(lambda x: get_Population_for_country(x['iso3'], x['year']), axis=1)
print("Final Dataframe: \n" , shortenedWithoutInfo)