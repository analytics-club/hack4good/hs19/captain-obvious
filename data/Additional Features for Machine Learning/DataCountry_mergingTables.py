#!/usr/bin/python3
# encoding=utf8
#jbaumer@student.ethz.ch

import pandas as pd
import country_converter as coco
import pycountry_convert as pc

#read in the huge data file
filepathHuge = './INFORM2020_TREND_2010_2020_v039_ALL_1.xlsx'
rawDataHuge = pd.read_excel(filepathHuge)#, index_col="INFORMYear")
rawDataHuge = rawDataHuge.rename(columns={"INFORMYear": "year", "Iso3": "ISO3"})
rawDataHuge = rawDataHuge.set_index('year')
#shows exactly in the way Lionel wanted!
print("raw Huge \n",rawDataHuge)
print("\n")

#GDP
#read in Nathan file (GDP)
filepathNathanData = './Final_AllCountries_PerYearV3.xlsx'
rawDataNathan_GDP = pd.read_excel(filepathNathanData, sheet_name="GDP")
#restructure data so that years with country names in first two columns
structuredDataNathan_draft_GDP = pd.melt(rawDataNathan_GDP, id_vars='ISO3', value_vars=[2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018]) #make a long table
#renaming year and GDP
structuredDataNathan_GDP = structuredDataNathan_draft_GDP.rename(columns={"variable": "year", "value": "GDP"})
structuredDataNathan_GDP_final = structuredDataNathan_GDP.set_index('year')
print("structured Nathan GDP \n",structuredDataNathan_GDP_final.head(5))
print("\n")


#Socio_Economic_Vulnerability
#read in Nathan file (Socio_Economic_Vulnerability)
filepathNathanData = './Final_AllCountries_PerYearV3.xlsx'
rawDataNathan_Socio_Economic_Vulnerability = pd.read_excel(filepathNathanData, sheet_name="Socio_Economic_Vulnerability")
#restructure data so that years with country names in first two columns
structuredDataNathan_draft_Socio_Economic_Vulnerability = pd.melt(rawDataNathan_Socio_Economic_Vulnerability, id_vars='ISO3', value_vars=[2011,2012,2013,2014,2015,2016,2017,2018,2019]) #make a long table
#renaming year and Socio_Economic_Vulnerability
structuredDataNathan_Socio_Economic_Vulnerability = structuredDataNathan_draft_Socio_Economic_Vulnerability.rename(columns={"variable": "year", "value": "Socio_Economic_Vulnerability"})
structuredDataNathan_Socio_Economic_Vulnerability_final = structuredDataNathan_Socio_Economic_Vulnerability.set_index('year')
print("structured Nathan Socio_Economic_Vulnerability \n",structuredDataNathan_Socio_Economic_Vulnerability_final.head(5))
print("\n")

#Physical_Infrastructure
#read in Nathan file (Physical Infrastructure)
filepathNathanData = './Final_AllCountries_PerYearV3.xlsx'
rawDataNathan_Physical_Infrastructure = pd.read_excel(filepathNathanData, sheet_name="Physical_Infrastructure")
#restructure data so that years with country names in first two columns
structuredDataNathan_draft_Physical_Infrastructure = pd.melt(rawDataNathan_Physical_Infrastructure, id_vars='ISO3', value_vars=[2011,2012,2013,2014,2015,2016,2017,2018,2019]) #make a long table
#renaming year and Physical_Infrastructure
structuredDataNathan_Physical_Infrastructure = structuredDataNathan_draft_Physical_Infrastructure.rename(columns={"variable": "year", "value": "Physical_Infrastructure"})
structuredDataNathan_Physical_Infrastructure_final = structuredDataNathan_Physical_Infrastructure.set_index('year')
print("structured Nathan Physical_Infrastructure \n",structuredDataNathan_Physical_Infrastructure_final.head(5))
print("\n")

#Surface
#read in Nathan file (Surface)
filepathNathanData = './Final_AllCountries_PerYearV3.xlsx'
rawDataNathan_Surface = pd.read_excel(filepathNathanData, sheet_name="Surface")
#restructure data so that years with country names in first two columns
structuredDataNathan_draft_Surface = pd.melt(rawDataNathan_Surface, id_vars='ISO3', value_vars=[2018]) #make a long table
#renaming year and Surface
structuredDataNathan_Surface = structuredDataNathan_draft_Surface.rename(columns={"variable": "year", "value": "Surface"})
structuredDataNathan_Surface_final = structuredDataNathan_Surface.set_index('year')
print("structured Nathan Surface \n",structuredDataNathan_Surface_final.head(5))
print("\n")

#Population
#read in Nathan file (Population)
filepathNathanData = './Final_AllCountries_PerYearV3.xlsx'
rawDataNathan_Population = pd.read_excel(filepathNathanData, sheet_name="Population")
#restructure data so that years with country names in first two columns
structuredDataNathan_draft_Population = pd.melt(rawDataNathan_Population, id_vars='ISO3', value_vars=[2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018]) #make a long table
#renaming year and Population
structuredDataNathan_Population = structuredDataNathan_draft_Population.rename(columns={"variable": "year", "value": "Population"})
structuredDataNathan_Population_final = structuredDataNathan_Population.set_index('year')
print("structured Nathan Population \n",structuredDataNathan_Population_final.head(5))
print("\n")


#join2frames = rawDataHuge.merge(structuredDataNathan_GDP_final)
#print("join2frames \n", join2frames)
#join3frames = join2frames.merge(structuredDataNathan_Socio_Economic_Vulnerability_final)
#join4frames = join3frames.merge(structuredDataNathan_Physical_Infrastructure_final)
#join5frames = join4frames.merge(structuredDataNathan_Surface_final)
#join6frames = join5frames.merge(structuredDataNathan_Population_final)
#print("join3frames \n", join6frames)


