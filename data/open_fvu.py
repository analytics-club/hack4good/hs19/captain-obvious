#############################################
# Script to open different vulnerability files
#
# TODO: 1) what are intensity parametrs for different hazards ?
#       2) if the additional information is present, are the fields the same ?
#       3) add assertions wrt number of points on the curve
#       4) write tests separately
#############################################

import pandas as pd
from pandas import Series, DataFrame
import matplotlib
import matplotlib.pyplot as plt
import re


class vulnerability_curve:
    """
    collect all relevant information about vulnerability curves
    collected in .fvu files

    TODO: 1) implement plotting function
          2) explore all types vulnerabilities in the files
          3) write tests
    """
    def __init__(self, filepath):
        """

        :param self:
        :param filepath: location of the .fvu file
        :return:
        """
        # OPEN the file:        alternative commants open(filename_case1, 'rb') as f
        with open(filepath, encoding='utf-8', errors='ignore') as f:
            raw_lines = f.readlines()
        # INITIALIZE all attributes:
        self.header_attr = ''  # I think it's always the same but need to check
        self.vuln_phys = DataFrame(columns=['intensity', 'MDR', 'MDR_VAR'])
        self.vuln_hum = DataFrame(columns=['intensity', 'MDR', 'MDR_VAR'])
        self.curve_info = {}
        self.curve_params = {}

        # Iterate over lines
        i = 0
        while len(raw_lines[i].split(" ")) < 3:
            tmp = re.sub('\t?\n', '', raw_lines[i])
            self.header_attr = "{} {}".format(self.header_attr, tmp)
            i += 1
        self.header_attr = self.header_attr.strip()
        # print("header {}".format(i))
        # get information about vulnerability curve
        if i < len(raw_lines):
            while len(raw_lines[i].split(" ")) == 3:
                #print("{} {}".format(i, tmp))
                try:
                    tmp = list(map(float, re.sub('\t?\n', '', raw_lines[i]).split(" ")))
                except ValueError:
                    print("{} Could not convert {}".format(i, tmp))
                else:
                    self.vuln_phys = self.vuln_phys.append(Series({'intensity': tmp[0], \
                        'MDR': tmp[1], 'MDR_VAR': tmp[2]}),ignore_index=True)
                finally:
                    i += 1
        while (len(raw_lines[i].split(" ")) < 3) & (i < len(raw_lines)):
            i += 1

        # print("vul done {}".format(i))
        # add "human" vulnerability
        if i < len(raw_lines):
            while (len(raw_lines[i].split(" ")) == 3)&((i < len(raw_lines) - 1)):
                try:
                    tmp = list(map(float, re.sub('\t?\n', '', raw_lines[i]).split(" ")))
                except ValueError:
                    print("{} Could not convert {}".format(i, tmp))
                else:
                    self.vuln_hum = self.vuln_hum.append(Series({'intensity': tmp[0], \
                         'MDR': tmp[1], 'MDR_VAR': tmp[2]}), ignore_index=True)
                finally:
                    i += 1

        # print("hum done {}".format(i))
        # collect general information about vulnerability curve
        if i < len(raw_lines)-1:
            assert raw_lines[i][1] == "*", "error at {}: {}".format(i, raw_lines[i])
            i += 1
            while raw_lines[i][1] != "*":
                tmp = raw_lines[i].split(":")
                self.curve_info.update({tmp[0].strip(): re.sub('\t?\n', '', tmp[1]).strip()})
                i += 1
        # print("curve info ready {}".format(i))
        # collect parameters of vulnerability curve
        if i < len(raw_lines)-1:  # i = 218
            assert raw_lines[i][1] == "*", "error at {}: {}".format(i, raw_lines[i])
            i += 1
            while (raw_lines[i][1] != "*") & (i < len(raw_lines) - 1):
                tmp = raw_lines[i].split(":")
                # print(tmp)
                self.curve_params.update({tmp[0].strip(): re.sub('\t?\n', '', tmp[1]).strip()})
                i += 1
    def plot(self, damage_type = "physical"):
        """

        :param damage_type: either physical or human
        :return:

        TODO: add option so save the figure
        """
        assert (damage_type == "physical") or (damage_type == "human"),\
               "UNKNOWN DAMAGE TYPE, need one of 'physical', 'human'"
        fig = plt.figure()
        plt.plot(self.vuln_phys["intensity"], self.vuln_phys["MDR"])
        plt.fill_between(self.vuln_phys["intensity"], self.vuln_phys["MDR"] + self.vuln_phys['MDR_VAR'],\
                         self.vuln_phys["MDR"] - self.vuln_phys['MDR_VAR'], alpha = 0.5)
        plt.axhline(y=1, color='r', linestyle='--')
        fig.suptitle('Vulnerability curve', fontsize=20) # add more inforamtion about hazard
        plt.ylabel('MDR', fontsize=16)
        #fig.savefig('test.jpg')
        plt.show()






"""

# test 1: all information is present
filename_case1 = "/Users/sychevaa/Desktop/Hack4Good/Datasets/C2M_H(gal).fvu"
vul1 = vulnerability_curve(filename_case1)
vul1.plot()

vul1.vuln_phys
vul1.vuln_hum
vul1.curve_info
vul1.curve_info

# test 2: human and physical, but nothing else
filename_case2 = "/Users/sychevaa/Desktop/Hack4Good/Datasets/S_C1_L(gal).fvu"
vul2 = vulnerability_curve(filename_case2)
#vul2.plot()

vul2.vuln_phys
vul2.vuln_hum
vul2.curve_info
vul2.curve_params

# test 3 only physical 

"""

