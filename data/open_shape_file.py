########
# Script to open and explore shape files (.shp, .shx, .dbf)
# Based on the tutorial: https://www.earthdatascience.org/workshops/gis-open-source-python/intro-vector-data-python/

# TODO: create a more detailed jupyter notebook

#######

#---------------------------------------  TEMPORARY SOLUTION
# Load files on the local computer
#---------------------------------------

# import necessary packages

import os
import matplotlib.pyplot as plt
import geopandas as gpd
#import earthpy as et


SHAPE_FILE_FOLDER = os.getcwd() + "/data/sample_shape_file/" # loaded files gar2015* to my local computer
os.chdir(SHAPE_FILE_FOLDER)

gar2015 = gpd.read_file(SHAPE_FILE_FOLDER + 'gar2015.shp')

# View attributes of a shape file
gar2015.head(6)

# Explore the file metadata:
# view the spatial extent: the geographic "edge" or location that is the furthest north, south east and west.
gar2015.total_bounds

# CRS code
gar2015.crs

# Dimensions ?
gar2015.shape # (267, 194)


# Attributes ?
gar2015.columns.values # (267, 194)

# Describe
gar2015.describe()

# BASIC DATA PLOTS (extend later)

fig, ax = plt.subplots(figsize = (10,10))
attr = 'EQ_AAL'
gar2015.loc[ gar2015[attr].isnull() == False, ].plot(column=attr,
             # categorical=True,
             legend=True,
             #figsize=(10,6),
             #markersize=45,
             cmap='OrRd', #"Set2",
             scheme='quantiles',
             ax=ax)

#ax.set_title('SJER Plot Locations\nMadera County, CA', fontsize=16);
plt.show()



#---------------------------------------  DOES NOT WORK YET
# How to access data from google drive?
# see https://www.javacodemonk.com/google-colab-import-data-from-google-drive-as-pandas-dataframe-079a3609
#---------------------------------------

import pandas as pd
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
# from google.colab import auth
from oauth2client.client import GoogleCredentials


import logging
logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)


gauth = GoogleAuth()
gauth.credentials = GoogleCredentials.get_application_default()
drive = GoogleDrive(gauth)
