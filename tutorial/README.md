# Tutorial and dataset overview

This folder contains some piece of codes to read the IDMC dataset and explore the feature of climata classes. Info of each dataset and class can be found below.

## Useful links

Data location (from 09/10/2019): [link](https://drive.google.com/drive/folders/147J_zRReudE4TS0m9DcFYTAygFB5NAFV)

##Classes

# Exposure

Class documentation: [link](https://drive.google.com/drive/folders/147J_zRReudE4TS0m9DcFYTAygFB5NAFV)

Data location: [link](https://drive.google.com/drive/folders/1JDGNtr-lxSKNQ6P3C255GpDMIK1-uwIi)

Data: documentation: [link](https://drive.google.com/drive/folders/1paSsNrXVS_UirXG5uMz8gDXDO7Ql2CSY)

Two scripts are available to build exposure classes and go through some features, Exposure_from_GeoPandas.py and Exposure_from_csv.py.

# Hazards

To read hazard from grd file, see the script Read_flooding.py.
