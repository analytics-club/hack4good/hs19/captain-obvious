import os
import sys
import time

import numpy as np
import pandas as pd
import geopandas as gpd
from datetime import datetime
import matplotlib.pyplot as plt
import re

from tutorial.parse_gdacs_hazard import clean_gdacs_data
# from tutorial.parse_gdacs_hazard import check_broken_geojson

sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))
from ConfigurationClass import Configuration
config = Configuration()
path = config.param["DATA_DIRECTORY"]

#-- Helper functions
def check_and_summarize_geojson(datadirec):
    """
    Check if there is any geojson file broken, i.e., they do not contain geometry information.
    Create a map where each row contains a gdacs event(tropical cyclone). It uses eventid as key, and a list of
    iso3, eventname, fromdate, todate,and html description as values.
    Param:
    ======
    datadirec: str, directory to saved gdacs files

    Return:
    ======
    broken: dict,
    events: dict,
    """
    s = time.time()
    broken = {}
    events = {}
    for f in os.listdir(datadirec):
        if f.endswith(".geojson"):
            filename = os.path.join(datadirec, f)
            gjsontable = gpd.read_file(filename)
            cols = gjsontable.columns
            eventid = f.split("_")[1]
            if pd.isnull(gjsontable['geometry']).sum():
                broken.setdefault(eventid, []).append(filename)
            elif eventid not in events.keys():
                if 'iso3' in cols and 'eventname' and 'fromdate' in cols and 'todate' in cols:
                    events.setdefault(eventid, []).append(gjsontable.loc[0, 'iso3'])
                    events[eventid].append(gjsontable.loc[0, 'eventname'])
                    events[eventid].append(gjsontable.loc[0, 'fromdate'])
                    events[eventid].append(gjsontable.loc[0, 'todate'])
                    events[eventid].append(gjsontable.loc[0, 'htmldescription'])
    e = time.time()
    print("Total time used: {}".format(e - s))
    return broken, events

#-- Read in IDMC
filename_displ_country = path + "/idmc/idmc_displacement_all_dataset.xlsx"
filename_displ_event = path + "/idmc/idmc_disaster_all_dataset.xlsx"
# df_displace = pd.read_excel(filename_displ_country, header=[0,1]) # both names
# df_disaster = pd.read_excel(filename_displ_event, header=[0,1])
df_displace = pd.read_excel(filename_displ_country).loc[1:,:] # df_disaster = pd.read_excel(filename_displ_event, header=[1])
df_disaster = pd.read_excel(filename_displ_event).loc[1:,:] # df_disaster = pd.read_excel(filename_displ_event, header=[1])
storm = df_disaster[df_disaster.loc[:,'Hazard Type'] == "Storm"]
print("Total events {}".format(len(df_disaster)))

#-- Clean IDMC (thanks Asy)
n_disaster_null = len(df_disaster[df_disaster['New Displacements'].isnull()==True])
df_disaster = df_disaster.loc[df_disaster[('New Displacements', '#affected+idps+ind+newdisp+disaster')].isnull()==False]
print("Excluded {} events".format(n_disaster_null))

#-- Read in GDACs
# Check geojson data and create events map
# broken, events = check_broken_geojson(path + "/gdacs/TC/")

test = gpd.read_file(path + "/gdacs/1000102_1.geojson")  # Mian please make this work for you :)  - I can help :)
test2 = clean_gdacs_data(test)