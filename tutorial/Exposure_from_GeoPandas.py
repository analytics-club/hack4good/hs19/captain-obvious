###
#Script to read the Exposure dataset from GeoDataFram and go over the feature of the Exposure class
#Author: Gael Ludocvic Perrin

#Datasets: Download africa_ago_costal.tar. Unzip in the current directory.

import numpy as np
import geopandas as gpd
from climada.entity import Exposures

# Reading input GeoDataFrame  file
#input_file = "./africa_ago_costal/africa_ago_costal.shp"
input_file = "/Users/gaelperrin/Downloads/reference_grid_GIS_points5x5/pt_5x5.shp"


# Fill GeoDataFrame
costal = gpd.read_file(input_file)
print('\x1b[1;03;30;30m' + 'costal is a GeoDataFrame:', str(type(costal)) + '\x1b[0m')
print('\x1b[1;03;30;30m' + 'costal looks like:' + '\x1b[0m')

# First look at the data
print(costal.head())
#print(costal[0])

# Write it into a json file (to be read by a human)
print('\x1b[1;03;30;30m' + 'Making json:', str(type(costal)) + '\x1b[0m')
costal.to_file("read_exposure.json", driver="GeoJSON")

# Generate Exposures
exp_costal = Exposures(costal)
print('\n' + '\x1b[1;03;30;30m' + 'exp_costal is now an Exposures:', str(type(exp_costal)) + '\x1b[0m')

# Have a first look on the exposure
print('\n' + '\x1b[1;03;30;30m'  + 'exp_df looks like:' + '\x1b[0m')
print(exp_costal.head())

# Provide value (here population)
exp_costal['value'] = costal["VALHUM"]
exp_costal.value_unit = "Population"

# You can add the year as it is not read from the data (not mendatory). Adding dummy value here.
exp_costal.ref_year = "2084"

# Set latitude and longitude attributes from geometry
exp_costal.set_lat_lon()

# You can add an impact function (if_) for any hazard. Here we add for Tropical Cyclon (TC) with dummy values.
exp_costal['if_TC'] = np.ones(costal.shape[0], int)


# Check and assign the features
print('\n' + '\x1b[1;03;30;30m' + 'check method logs:' + '\x1b[0m')
exp_costal.check()

# Have a look on the exposure's features
print('\n' + '\x1b[1;03;30;30m'  + 'exp_costal looks like:' + '\x1b[0m')
exp_costal.head()

# Plot the population of a map
plt = exp_costal.plot_basemap()[0] # exposures in africa
plt.savefig('Exposure_from_GeoPandas_map.pdf')

