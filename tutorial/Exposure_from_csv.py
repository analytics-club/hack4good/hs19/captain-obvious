###
#Script to read the Exposure dataset from csv file and go over the feature of the Exposure class.
#An additional input file is needed to get the lat/long coordinates.
#Author: Gael Ludocvic Perrin

#Datasets: Download pt_5x5.* and GED2015_Asia_30_jan2015_5x5.zip. Unzip in the current directory.
#Some doc about the dataset:
#https://drive.google.com/drive/folders/1paSsNrXVS_UirXG5uMz8gDXDO7Ql2CSY


import numpy as np
import pandas as pd
import geopandas as gpd
from climada.entity import Exposures
from ConfigurationClass import Configuration

config = Configuration()
path = config.param["DATA_DIRECTORY"]
path_out=config.param["OUT_DIRECTORY"]

ex_continent = "GED2015_tzun_Africa_30jan2014"
ex_country = "COM"


# Reading input .csv  file
input_file =  path + "/exposure/OutputsCapra1x1/"+ex_continent+"/"+"ged2015expo_"+ex_country+".csv"


# Fill GeoDataFrame
exposure_data = pd.read_csv(input_file, delimiter=',', header=None,engine="python")

# Put nice names
#pd_asia.rename(columns={"0": 'id_5x'})
#As no header, column have to be renamed.

gpf_exposure = gpd.GeoDataFrame(exposure_data)
gpf_exposure.columns = ['id_5x', 'iso3', 'cpx', 'sector', 'se_sismo', 'bs_value_r', 'bs_value_nr', 'VALHUM', 'VALIS']
print(gpf_exposure.head())


# The file we just loaded doesn't contain the lat. anb long. coordinates. Those are located in another file.
# (This step can take a few minutes to run)


file_GRID5x5 = path+"/exposure/pt_5x5.shp"  # Not versioned because definitely too heavy for that.
GRID5x5 = gpd.GeoDataFrame(gpd.read_file(file_GRID5x5))
print(GRID5x5.head())

#print(pd.merge(gpf_asia, GRID5x5, on='id_5x').head())

# Merging the two files
gpf_withXY = pd.merge(gpf_exposure, GRID5x5, on='id_5x')

# Create the exposure
exp_asia = Exposures(gpf_withXY)

exp_asia['value'] = gpf_exposure["VALHUM"]
exp_asia.value_unit = "Population"

print(exp_asia.head(10))

exp_asia.set_lat_lon()

exp_asia.check()
exp_asia.to_csv(path_out+"merged.csv")
exp_asia.loc[[1000,2000,3000,4000,5000,6000],:]
# Make map plot
# fixme: overlapping points are not adding correclty on the plot, on windows the plotting just does not give a result
import matplotlib.pyplot as plt
exp_asia.loc[[1000,2000,3000,4000,5000,6000],:].plot_basemap()# exposures in africa
plt.savefig(path_out+'Exposure_from_csv_map.pdf')










