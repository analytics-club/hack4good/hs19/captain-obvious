import rasterio

fp = r"D:\Downloads\MWP_2019225_090E030N_3D3OT.tif"

# Open the file:
raster = rasterio.open(fp)

# Check type of the variable 'raster'
type(raster.read())

import geopandas as gpd

import pandas as pd
raster_file=raster.read(1)
raster_file[raster_file==1]=0


from matplotlib import pyplot
pyplot.imshow(raster_file, cmap='pink')
pyplot.show()
import numpy as np
lat=np.arange(20,30,10/4552)
lon=np.arange(90,100,10/4552)

df=pd.DataFrame(raster_file,index=lat,columns=lon)
df2=pd.DataFrame(df.unstack())
df3=pd.DataFrame(df2.to_records())

import geopandas as gpd


df3.columns=["lat","lon","val"]
gdf = gpd.GeoDataFrame(
    df3, geometry=gpd.points_from_xy(df3.lon, df3.lat))

gdf = gpd.GeoDataFrame(
    df, geometry=gpd.points_from_xy(df.columns, df.))