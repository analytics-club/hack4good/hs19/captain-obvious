import geopandas as gpd
from climada.entity import Exposures
import pandas as pd
import sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))
from ConfigurationClass import Configuration
config = Configuration()
path = config.param["DATA_DIRECTORY"]

ex_continent = "GED2015_Africa_27_jan2015_5x5"
country_lookup=pd.read_csv(path+"country_code.csv",sep=";",dtype=str)
country_lookup.set_index("Country",inplace=True)

ex_country = country_lookup.loc["Burundi","Alpha_3"]

# The following inputs come from https://drive.google.com/drive/folders/1JDGNtr-lxSKNQ6P3C255GpDMIK1-uwIi
# input_file =  path + "/exposure/OutputsCapra1x1/GED2015_tzun_Africa_30jan2014/ged2015expo_BEN.csv"
input_file =  path + "/exposure/OutputsCapra5x5/"+ex_continent+"/"+"ged2015expo_"+ex_country+".csv"

pd_AGO = pd.read_csv(input_file, delimiter=',', header=None,engine="python")
gpd_AGO=gpd.GeoDataFrame(pd_AGO)
gpd_AGO.columns = ['id_5x', 'iso3', 'cpx', 'sector', 'se_sismo', 'bs_value_r', 'bs_value_nr', 'VALHUM', 'VALIS']

ex_country = "GNB"
input_file_2 =  path + "/exposure/OutputsCapra5x5/"+ex_continent+"/"+"ged2015expo_"+ex_country+".csv"
pd_GNB = pd.read_csv(input_file_2, delimiter=',', header=None,engine="python")
gpd_GNB=gpd.GeoDataFrame(pd_GNB)
gpd_GNB.columns = ['id_5x', 'iso3', 'cpx', 'sector', 'se_sismo', 'bs_value_r', 'bs_value_nr', 'VALHUM', 'VALIS']


file_GRID5x5 = path+"/exposure/pt_5x5.shp"  # Not versioned because definitely too heavy for that.
GRID5x5 = gpd.GeoDataFrame(gpd.read_file(file_GRID5x5))

import matplotlib.pyplot as plt

# Merging the two files
gpf_withXY = pd.merge(gpd_AGO, GRID5x5, on='id_5x')

gpd_GNB=pd.merge(gpd_GNB,GRID5x5,on="id_5x")
exp_GNB= Exposures(gpd_GNB)
exp_GNB["value"]=exp_GNB["VALHUM"]
exp_GNB.set_lat_lon()
exp_GNB.check()

exp_AGO=Exposures(gpf_withXY)

exp_AGO["value"]=exp_AGO['VALHUM']

exp_AGO.set_lat_lon()

exp_AGO.check()

#exp_AGO.plot_basemap()[0] # exposures in africa
#plt.show()

import numpy as np
from climada.entity import ImpactFunc, ImpactFuncSet

#create different impcat functions depending on building type as defined in
#gpd_AGO["se_sismo"].unique()


imp_fun_1 = ImpactFunc()
imp_fun_1.haz_type = 'FL'
imp_fun_1.id = 1
imp_fun_1.name = 'A'
imp_fun_1.intensity_unit = 'mm/m2'
imp_fun_1.intensity = np.linspace(0, 800, num=15)
imp_fun_1.mdd = np.sort(np.random.rand(15)) #mean value harmed
imp_fun_1.paa = np.sort(np.random.rand(15)) #mean value affected

imp_fun_2 = ImpactFunc()
imp_fun_2.haz_type = 'FL'
imp_fun_2.id = 2
imp_fun_2.name = 'C'
imp_fun_2.intensity_unit = 'mm/m2'
imp_fun_2.intensity = np.linspace(0, 800, num=15)
imp_fun_2.mdd = np.sort(np.random.rand(15))
imp_fun_2.paa = np.sort(np.random.rand(15))

imp_fun_3 = ImpactFunc()
imp_fun_3.haz_type = 'FL'
imp_fun_3.id = 3
imp_fun_3.name = 'INF'
imp_fun_3.intensity_unit = 'mm/m2'
imp_fun_3.intensity = np.linspace(0, 800, num=15)
imp_fun_3.mdd = np.sort(np.random.rand(15))
imp_fun_3.paa = np.sort(np.random.rand(15))


# fill ImpactFuncSet
if_set = ImpactFuncSet()
if_set.append(imp_fun_1)
if_set.append(imp_fun_2)
if_set.append(imp_fun_3)

# ADD A HAZARD






import numpy as np
from climada.hazard import Hazard

#change the path to your configuration  # TODO HELP where do these files come from?
cur_path =path+"/hazard/Flood"
haz_ven = Hazard('FL')
haz_ven.read_excel(path+"TC_BGD_29_Oct_2019.xlsx")
#haz_ven.set_raster([cur_path+"/"+ex_country+"/Hazard_"+ex_country+"__1000.grd"])
haz_ven.units='mm/m2'
haz_ven.check()

#The hazard is called 1
import matplotlib.pyplot as plt
print('event name', haz_ven.get_event_name(1))
haz_ven.plot_intensity([1]) # plot intensity of hurricanes Andrew
plt.show()

from climada.engine import Impact

imp_fl = Impact()
imp_fl.calc(exp_AGO, if_set, haz_ven)
imp_fl.calc(exp_GNB, if_set, haz_ven)

imp_fl.tot_value
np.sum(imp_fl.eai_exp)

haz_ven.fraction.todense().reshape(361,211)


np.max(haz_ven.intensity.todense())
haz_ven.centroids

#Analyse Exposure, following Daniels comments


fig, ax = plt.subplots()
ax.imshow(haz_ven.fraction.todense().reshape(211,361))
plt.show()

