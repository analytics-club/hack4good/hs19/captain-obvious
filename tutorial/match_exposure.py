import numpy as np
import pandas as pd
import geopandas as gpd
from climada.entity import Exposures
import timeit
import os

import sys
sys.path.append('../')
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))
from ConfigurationClass import Configuration

config = Configuration()
path = config.param["DATA_DIRECTORY"]

class MatchExposure:

    def __init__(self, geometry, input_path, output_path=None, verbose = True, grid = "5x5"):
        self.geometry = geometry
        self.input_path = input_path
        if output_path==None:
            self.output_path = path + "geomatched_exposure\\"
        else:
            self.output_path = output_path
        if not os.path.exists(self.output_path):
            os.makedirs(self.output_path)
        self.verbose = verbose
        self.GRID = gpd.GeoDataFrame()
        if not (grid == "5x5" or grid == "1x1"):
            print("Error: grid option must be 5x5 or 1x1. You have used", grid)
            sys.exit()

        self.grid = grid

        #loading the geometry file. This can take several minutes
        if self.verbose:
            print("Starting to load the geometry")
            print("Using grid:", self.grid)
        start = timeit.default_timer()
        self.GRID = gpd.GeoDataFrame(gpd.read_file(geometry))
        stop = timeit.default_timer()

        if self.grid == "1x1":
            self.GRID.columns = ['id_1x', 'MAX', 'BU_CPX', 'geometry']


        if verbose:
            print("Finished to load the geometry file in %f seconds"%(stop - start))
            print(self.GRID.head())
            self.GRID.to_csv("check_grid.csv")




    def getExposureList(self):
        self.exposures = []
        for exp in os.listdir(self.input_path):
            if exp.endswith(".csv"):
                self.exposures.append(exp)

        print('Full exposure list is',self.exposures)

    def doMatching(self, exposure_file):

        if self.verbose: print("Start to match", exposure_file)
        exposure_data = pd.read_csv(self.input_path + exposure_file, delimiter=',', header=None, engine="python")
        gpf_exposure = gpd.GeoDataFrame(exposure_data)
        if self.grid == "5x5":
            gpf_exposure.columns = ['id_5x', 'iso3', 'cpx', 'sector', 'se_sismo', 'bs_value_r', 'bs_value_nr', 'VALHUM', 'VALIS']
            gpf_withXY = pd.merge(gpf_exposure, self.GRID, on='id_5x')
        if self.grid == "1x1":
            gpf_exposure.columns = ['id_1x', 'iso3', 'cpx', 'sector', 'se_sismo', 'bs_value_r', 'bs_value_nr', 'VALHUM', 'VALIS']
            gpf_withXY = pd.merge(gpf_exposure, self.GRID, on='id_1x')



        exp = Exposures(gpf_withXY)
        exp['value'] = gpf_exposure["VALHUM"]
        exp.value_unit = "Population"

        #print(exp.head(10))

        exp.set_lat_lon()
        exp.check()

        #writin file
        exp.write_hdf5(self.output_path + exposure_file.replace('.csv','.h5'))

        del exposure_data
        del gpf_exposure
        del gpf_withXY
        del exp

    def runMatchExposure(self):
        self.getExposureList()
        for exp in self.exposures:
            if os.path.isfile(self.output_path+exp.replace('.csv','.h5')):
                if self.verbose: print("Exposure file %s was already processed, will be skipped."%exp.replace('.csv','.h5'))
                continue
            print("Will start to match", exp)
            self.doMatching(exp)



if __name__ == '__main__':
    ##
    #5x5
    ##
    #input_file = path + "/exposure/OutputsCapra5x5/GED2015_Africa_27_jan2015_5x5/"
    #input_file = path + "/exposure/OutputsCapra5x5/GED2015_Asia_30_jan2015_5x5/"
    #input_file = path + "/exposure/OutputsCapra5x5/GED2015_W_Asia_30_jan2015_5x5/"


    ##
    #1x1
    ##
    #input_file = path + "/exposure/OutputsCapra1x1/GED2015_tzun_Africa_30jan2014/"
    input_file = path + "/exposure/OutputsCapra1x1/GED2015_tzun_Europe_3feb2014/"


    #file_GRID = path + "/exposure/pt_5x5.shp"
    file_GRID = path + "/exposure/tzun_1x1_pt.shp"

    ##For 1x1 matching
    #ME = MatchExposure(file_GRID, input_file, grid = "1x1")

    ##For 5x5 matching
    ME = MatchExposure(file_GRID, input_file, grid = "1x1")
    ME.runMatchExposure()



    ##Read geo file
    #ME = MatchExposure(file_GRID, input_file,".")

    ##Get list of all exposures
    #ME.getExposureList()

