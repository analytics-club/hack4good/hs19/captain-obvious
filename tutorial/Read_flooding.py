########
#Script to read Hazard .grd data. Descriebe flowed hazard


#######


import numpy as np
from climada.hazard import Hazard
import sys
import os
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))
from ConfigurationClass import Configuration
config = Configuration()
path = config.param["DATA_DIRECTORY"]


ex_continent = "GED2015_tzun_Africa_30jan2014"
ex_country = "BEN"




#change the path to your configuration
#input_file = "PHL_Flloding-00085/SC00085_PH__M1.grd"

cur_path =path+"/hazard/Flood"
haz_ven = Hazard('FL')
haz_ven.set_raster([cur_path+"/"+ex_country+"/Hazard_"+ex_country+"__1000.grd"])
haz_ven.check()

#The hazard is called 1
print('event name', haz_ven.get_event_name(1))
haz_ven.plot_intensity("1") # plot intensity of hurricanes Andrew




print('event_id: ', haz_ven.event_id)
print('event_name: ', haz_ven.event_name)
print('date: ', haz_ven.date)
print('frequency: ', haz_ven.frequency)
print('orig: ', haz_ven.orig)
print('min, max fraction: ', haz_ven.fraction.min(), haz_ven.fraction.max())


#getting coordinates and intensity
haz_ven.intensity.todense()
haz_ven.centroids.coord

