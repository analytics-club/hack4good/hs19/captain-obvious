import pandas as pd
import geopandas as gpd
import json
import sys
import os
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))
from ConfigurationClass import Configuration

#-- Read the data
config = Configuration()
path = config.param["DATA_DIRECTORY"]
first = gpd.read_file(path + "/gdacs/TC/1000102_1.geojson")  # Mian please make this work for you :)  - I can help :)
# first=gpd.read_file("D:\\Downloads\\data_download\\1000102_1.geojson")

#-- Interpret data
first = first[["id","name","eventtype","eventid","fromdate","todate","iso3","polygonlabel",'Class','geometry']]
first.loc[:,"eventid"]=first.loc[0,"eventid"]
first.loc[:,"fromdate"]=first.loc[0,"fromdate"]
first.loc[:,"todate"]=first.loc[0,"todate"]
first.loc[:,"iso3"]=first.loc[0,"iso3"]


def clean_gdacs_data(gpd_gdacs):
    df_gdacs = gpd_gdacs.copy()
    df_gdacs = df_gdacs.loc[0,:]
    df_gdacs = pd.DataFrame(df_gdacs)
    return(df_gdacs)


#flood1 = "D:/Downloads/MFW_2019080_030E010S_A14x3D3OT_V/MFW_2019080_030E010S_A14x3D3OT_V.shp"  # Not versioned because 240 MB, definitely too heavy for that.
#flood1 = gpd.GeoDataFrame(gpd.read_file(flood1))

#-- Read in (heavy) grid files
# Find the following shp and shx files here: https://drive.google.com/drive/folders/1JDGNtr-lxSKNQ6P3C255GpDMIK1-uwIi
file_GRID5x5 = path+"/exposure/pt_5x5.shp"  # Not versioned because definitely too heavy for that.
GRID5x5 = gpd.GeoDataFrame(gpd.read_file(file_GRID5x5)) # 3 mins.
print(GRID5x5.head())
within_polies = gpd.sjoin(GRID5x5,first,how="inner",op="within") # 3 mins.
within_polies.set_index("id_5x",inplace=True)

def save_hazard_xlsx(haz_gpd,iso3,date,path):
    writer = pd.ExcelWriter(path+'TC_{}_{}.xlsx'.format(iso3,date), engine='xlsxwriter')

    # Write each dataframe to a different worksheet.
    haz_gpd.loc[:,["latitude","longitude"]].to_excel(writer, sheet_name='centroids')
    haz_gpd.loc[:,["intensity"]].to_excel(writer, sheet_name='hazard_intensity')
    pd.DataFrame(index=["event_id", "event_name", "frequency", "orig_event_flag", "event_date"], columns=[1],
                 data=[1, "event001", 1, 1, "11052016"]).transpose().to_excel(writer, sheet_name='hazard_frequency')

    # Close the Pandas Excel writer and output the Excel file.
    writer.save()

within_polies["latitude"]=within_polies["geometry"].centroid.x
within_polies["longitude"]=within_polies["geometry"].centroid.y
within_polies["intensity"]=within_polies["polygonlabel"]

save_hazard_xlsx(within_polies,within_polies.loc[:,"iso3"].values[0],"11052016",path)


# #relevant information:
# metadata=data["metadata"]
# metadata["eventtype"]
# metadata["eventid"]
# metadata["eventname"]
# metadata["fromdate"]
# metadata["todate"]
# metadata["country"]
# metadata["iso3"]
# metadata["geometries"]
#
# features=data["features"][1:]
# features[1]["properties"]["polygonlabel"]
# features[1]["geometry"]["type"]
#
#
# gpd_features1=gpd.GeoDataFrame(features)
# Polygon(features[0]["geometry"]["coordinates"][0])
# Polygon(features[1]["geometry"]["coordinates"][0])
#
# from ConfigurationClass import Configuration
#
# config = Configuration()
# path = config.param["DATA_DIRECTORY"]
#
#
#
#
# features2=data["features"][2]
# from geojson import Polygon
#
#
#
#
#
#
# from shapely.geometry import Point
# from shapely.geometry.polygon import Polygon
# import numpy as np
#
#
# #coords = [(2,2), (2,4), (4,4), (4,2)]
#
#
# point = Point(145.5,15) # create point
# print(gpd_features1.contains(point)) # check if polygon contains point
# print(point.within(first["geometry"][1])) # check if a point is in the polygon
#
#
# x,y = first["geometry"][1].exterior.xy
# x2,y2 = first["geometry"][2].exterior.xy
#
# import matplotlib.pyplot as plt
# fig=plt.figure(figsize=(10,10))
#
# ax = fig.add_axes([0.1,0.1,0.8,0.8])
# ax.plot(x, y, color='#6699cc', alpha=0.7,
#     linewidth=3, solid_capstyle='round', zorder=2)
# ax.plot(x2, y2, color='#6500cc', alpha=0.7,
#     linewidth=3, solid_capstyle='round', zorder=2)
# ax.scatter(145.5,15)
# ax.set_title('Polygon')
# plt.show()