###
#Script to get the climada prediction (displacement) for a given hazard file.
#The corresponding exposurs is loaded from the dataset
#Author: Gael Ludovic Perrin

import numpy as np
import pandas as pd
import geopandas as gpd
from climada.entity import Exposures
import timeit
import os
from climada.hazard import Hazard
from climada.engine import Impact
from climada.entity import ImpactFunc, ImpactFuncSet

#import all the impact functions
from climada.entity.impact_funcs.bushfire import IFBushfire
from climada.entity.impact_funcs.drought import IFDrought
from climada.entity.impact_funcs.flood import IFRiverFlood
from climada.entity.impact_funcs.trop_cyclone import IFTropCyclone





import sys
sys.path.append('../')
from ConfigurationClass import Configuration

config = Configuration()
path = config.param["DATA_DIRECTORY"]

class getPrediction:
    def __init__(self, hazard_file, hazard_type, exposure_file, continent=None, verbose = True):
        '''

        :param hazard_file:
        :param hazard_type:
        :param exposure_file:
        :param verbose:
        '''

        #Retrieving the hazard
        self.hazard_type = hazard_type
        self.haz = Hazard(hazard_type)
        self.haz.read_excel(hazard_file)
        self.haz.check()

        #plt = self.haz.plot_intensity("1")[0]  # plot intensity of hurricanes Andrew
        #plt.savefig('check_hazard.pdf')

        #Retrieving the exposure

        self.exp = Exposures()
        self.exp.read_hdf5(exposure_file)
        self.exp.set_lat_lon()
        self.exp.check()
        self.exp.head()


        #plt = self.exp.plot_basemap(buffer=50000.0)[0]
        #plt.savefig('check_exp.pdf')

        self.continent = continent

    def getImpactFunction(self):
        '''Define the impact function'''

        self.IFS = ImpactFuncSet()
        IF = None

        if self.hazard_type == 'TC':
            IF = IFTropCyclone()
            IF.set_emanuel_usa()
        elif self.hazard_type == 'RF':
            IF = IFRiverFlood()
            if self.continent == None:
                print("ERROR: RF hazard type require a continent argument")
            getattr(IF, 'set_RF_IF_%s'%self.continent)()
        elif self.hazard_type == 'B':
            IF = IFBushfire()
            IF.set_default()
        elif self.hazard_type == 'D':
            IF = IFDrought()
            IF.set_default()
        else:
            print("Error: hazard type has to be TC, RF, B or D, not %s"%hazard_type)
            import sys
            sys.exit()


        #IF.set_default()

        IF.haz_type = self.hazard_type

        self.IFS.append(IF)

    def run(self):
        '''Calculate the impact'''
        imp = Impact()
        self.getImpactFunction()
        imp.calc(self.exp, self.IFS, self.haz)

        print ("Sum:", np.sum(imp.eai_exp))
        print ("Sum:", np.sum(imp.at_event))
        print('frequency: ', self.haz.frequency)
        #print("Total value",imp.tot_value)

        #plt = imp.plot_basemap_eai_exposure(buffer=50000.0)[0]
        #plt.savefig('check_imp.pdf')


#
#
# if __name__ == '__main__':
#     hazard_file = path + "hazard/Flood/BEN/Hazard_BEN__1000.grd"
#     exposure_file = path + "/exposure/OutputsCapra5x5/GED2015_Africa_27_jan2015_5x5/geoMatched/ged2015expo_BEN.h5"
#     pred = getPrediction(hazard_file, 'RF', exposure_file, 'Africa')
#     pred.run()
