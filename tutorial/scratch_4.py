import numpy as np
import pandas as pd
import geopandas as gpd
from climada.entity import Exposures
import timeit
import os

import sys
sys.path.append('../')
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))

from ConfigurationClass import Configuration

config = Configuration()
path = config.param["DATA_DIRECTORY"]

exp_read = Exposures()
exp_read.read_hdf5("/Users/gaelperrin/PycharmProjects/Climada/captain-obvious/data/exposure/OutputsCapra1x1/GED2015_tzun_Africa_30jan2014/geoMatched/ged2015expo_BEN.h5")
exp_read.head()