"""
this script stores the collection of functions I use to find
correspondence between the events in GDACS and IDMC displacement database


TODO: 1) additional criterium - not only fromdate, but also todate
      2) search by event name for TC: can significantly improve speed and matching quality
"""
# General:
import os
from datetime import datetime, timedelta
import time

# Data Analysis
import numpy as np
import pandas as pd
from pandas import Series, DataFrame
import geopandas as gpd

# Working with spatial data
import shapely
from shapely.geometry import Point, Polygon, MultiPolygon

# Web Scrapping
import requests
import urllib.request
from bs4 import BeautifulSoup

#------------------# Constants / Datasets

# FODLERNAMES: TO BE CHANGED ON OTHER COMPUTER
local_gdacs_index_filename = "/Users/sychevaa/Desktop/Hack4Good/GitLabRepository/captain-obvious/gdacs_index_all.csv" #'/Users/sychevaa/Desktop/Hack4Good/GitLabRepository/gdacs_TC_index.csv'
local_filename_displ_event = "/Users/sychevaa/Desktop/Hack4Good/idmc_disaster_all_dataset.csv"
GDACS_TC_FOLDER = "/Users/sychevaa/Desktop/Hack4Good/GitLabRepository/Data/gdacstc/"

# DATAFRAMES
WORLD = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
# gdacs
gdacs_index = pd.read_csv(local_gdacs_index_filename)
gdacs_index = gdacs_index.loc[gdacs_index["fromdate"].isnull() == False]
# idmc
idmc_df = pd.read_csv(local_filename_displ_event, header=[1], encoding = "ISO-8859-1")
idmc_df.rename(columns={'#country+code': "iso3",
                        '#country+name': "country"}, inplace=True)


CONNECTION_TIMEOUT = 20

# MISCELLANEOUS
IDMC_DATE_FMT = "%Y-%m-%d"
GDACS_DATE_FMT = "%d/%b/%Y  %H:%M:%S"
# TO BE CHANGED - MY PERSONAL JUDGEMENT !!!
# key: one of the values in the idmc_df '#crisis+type'
# value: one of the values in the  gdacs 'type'

idmc_gdacs_event_types = {
    "Flood": ['FL'], 'Storm': ['TC'],
    'Earthquake': ['EQ'],
    'Volcanic eruption' : ['VO'],
    'Drought': ['DR'],
    'Wet Mass Movement': ['FL', 'TC'],
    'Mass Movement': ['FL', 'TC', 'VO', 'EQ'],
    'Dry mass movement': ['FL', 'TC', 'VO', 'EQ']
    }

# DEFINE VARIOUS VARIABLE GORUPS:
vars_unique = ['fromdate', 'todate','Class', 'polygontype',
               'severity', 'alertlevel', 'latitude', 'longitude']

hazard_vars_flood = ['latitude', 'longitude', 'severity']
hazard_vars_storm_short = ['latitude', 'longitude', 'windspeed']

hazard_vars_storm = ['latitude', 'longitude', 'windspeed', 'pressure'
                     'stormstatus', 'infostormsurge', 'hasstormsurge',
                     'stormsurge_maxheight', 'stormsurge_city', 'stormsurge_country']



#------------------# SMALL HELPER FUNCTIONS

def get_event_id_from_geojson_url(geojson_url_full):
    """

    >>> geojson_url_full = 'https://www.gdacs.org/datareport/resources/FL/3889/geojson_3889_1.geojson'

    """
    return int(str(geojson_url_full).split("/")[-2])


def extract_event_id_from_filename(geojson_filename):
    """

    :param geojson_filename:
    :return:
    """
    try:
        res = int(str(geojson_filename).split("_")[-2])
        #print(res)
    except Exception as e:
        print(e)
        print(geojson_filename)
        res = np.nan
    finally:
        return res


def extract_event_name(gdacs_eventid, gdacs_event_type="TC"):
    """

    :param gdacs_eventid:
    :param gdacs_event_type:
    :return:

    gdacs_eventid = 1000552

    """
    url_tmpl = "https://www.gdacs.org/report.aspx?eventid={}&eventtype={}"
    url_loc = url_tmpl.format(int(gdacs_eventid), gdacs_event_type)
    response = requests.get(url_loc)
    soup = BeautifulSoup(response.text, "html.parser")
    tst = soup.findAll("div", {"class": "cell_value_summary"})



def get_all_json_filnames_from_url(url_gdasc):
    """

    :param url_gdasc:
    :return:

    >>> url = 'https://www.gdacs.org/datareport/resources/FL/4191/'
    """
    # -----------------------------------------
    try:
        response = requests.get(url_gdasc)
    except Exception as e:
        print(e)
        time.sleep(CONNECTION_TIMEOUT)
        response = requests.get(url_gdasc)
    # ----------------------------------------
    soup = BeautifulSoup(response.text, "html.parser")
    one_a_tag2 = soup.findAll('a', href=True)
    links2 = [one_a_tag2[x]["href"] for x in range(len(one_a_tag2))]
    json_filenames = [url_gdasc + str(x).split("/")[-1] for x in
                      list(filter(lambda x: str(x).endswith('geojson'), links2))]
    return json_filenames




def get_all_json_filenames_loc(eventid, gdacs_folder_loc=GDACS_TC_FOLDER):
    """

    :param eventid: id of the gdacs event
    :param gdacs_folder_loc: folder where the json files are located
                             1) it is assumed that one folder contains information about
                             only one event type
                             2) it is assumed that there are only json files
    :return:

    >>> eventid = 31997  // get_all_json_filenames_loc(1000444)
    """
    local_files = os.listdir(gdacs_folder_loc)
    # Select files with corresponding event_id
    geojson_files = list(filter(lambda x: str(x).endswith('geojson'), local_files))
    relevant_files = list(filter(lambda x: extract_event_id_from_filename(x)==eventid, geojson_files))
    relevant_files_full = [gdacs_folder_loc + f for f in relevant_files]
    return relevant_files_full


#------------------# FUNCTIONS TO SELECT CORRECT DATES


def try_convert_date(date_string, date_fmt):
    """convert a string with data information to a datetime object

    :param date_fmt:
    :param date_string:

    :return: a datetime object or
             np.nan if the formatting is unsuccessufl
    """
    try:
        res = datetime.strptime(str(date_string), date_fmt)
    except Exception as e:
        print(e)
        res = np.nan
    finally:
        return res


def get_date(fromtodate, with_seconds=False):
    """
    extract date from the string provided in the json files in GDACS

    :param fromtodate:  a date string, that can be found in the geojson files
    :param with_seconds: convert the H:M:S information as well or just use Y-M-D?
    :return: datetime object

    Notes:
        1. datetime formating in python:  https://stackabuse.com/how-to-format-dates-in-python/
        2. why to use with_seconds:

    TODO: 1) check the format is the same for all geojson files in GDACS

    Example:
    fromtodate = "20/Aug/2013 18:00:00"
    get_date(fromtodate) == "20-08-2013" (or something like this)
    """
    time_tmp = "%d/%b/%Y  %H:%M:%S" if with_seconds else "%d/%b/%Y"
    fromtodate = fromtodate if with_seconds else str(fromtodate).split(" ")[0]
    res = datetime.strptime(fromtodate, time_tmp)   # from datetime import datetime
    return res


def is_relevant_event(event_date, idx, gdacs_index):    #gdacs_url):
    """
    check if the timespan of the gdacs event, defined by fromdate-todate,  contains event date from idmc file

    :param event_date (str): date from the IDMC file, typically in the format "Y-m-d"
    :param gdacs_index (str): index file with methadata about all files in gdacs folder
    :return: (bool):

    """
    try:
        start_date = get_date(gdacs_index.loc[idx, 'fromdate'], with_seconds=False)   # df_event['fromdate'].values[0]
        end_date = get_date(gdacs_index.loc[idx, 'todate'], with_seconds=False)
    except KeyError:
        pass
        # print('No attributes fromdate, todate among:\n{}'.format(df_event.columns.values))
    # Date
    date_idmc = datetime.strptime(event_date, IDMC_DATE_FMT)
    is_relevant = ((date_idmc-start_date).days >= 0) & ((end_date-date_idmc).days >=0)
    return is_relevant



def is_relevant_date(idmc_date, gdacs_idx, gdacs_df=gdacs_index, tol_forward=10, tol_backward=10):
    """does the event from gdacs took place

    :param idmc_date: date of the event from idmc dataframe
    :param gdacs_idx: index of record in the gdacs_df, that contains metainformation about all gdacs events
    :param tol_forward: (days)
    :param tol_backward: (days)
    :return (bool):

    TODO: define timespan of relevant events based on both fromdate and todate, not only on fromdate

    >>> idmc_date = "2008-08-01"
    >>> gdacs_date = "11/Jan/2015 18:00:00"
    """
    # Define potentially relevant timeframe
    date_latest = datetime.strptime(str(idmc_date), IDMC_DATE_FMT) + timedelta(days=tol_forward)
    date_earliest = datetime.strptime(str(idmc_date), IDMC_DATE_FMT) - timedelta(days=tol_backward)
    # Extract information about gdacs event
    gdacs_fromdate = gdacs_df.loc[gdacs_idx, "fromdate"]
    gdacs_todate = gdacs_df.loc[gdacs_idx, "todate"]
    gdacs_formdatetime = try_convert_date(gdacs_fromdate, GDACS_DATE_FMT)
    gdacs_todatetime = try_convert_date(gdacs_todate, GDACS_DATE_FMT)
    # Results
    res_todate = ((date_latest - gdacs_todatetime).days >= 0) & ((gdacs_todatetime - date_earliest).days >= 0)
    res_fromdate = ((date_latest - gdacs_formdatetime).days >= 0) & ((gdacs_formdatetime - date_earliest).days >= 0)
    res = res_todate or res_fromdate
    return res


#------------------# FUNCTIONS TO SELECT BY COUNTRY

def is_in_country(idx, idx_world, gdacs_index):
    """
    idx: index in gdacs_index data frame
    idx_world: index in the world data frame

    """
    try:
        poly = Polygon(WORLD.loc[idx_world, "geometry"])
    except NotImplementedError:
        poly = MultiPolygon(WORLD.loc[idx_world, "geometry"])
    point = Point(gdacs_index.loc[idx, 'longitude'], gdacs_index.loc[idx, "latitude"])
    return point.within(poly)


def find_country(idx, gdacs_index):
    idx_world_all = WORLD.index
    indexes = list(filter(lambda x: is_in_country(idx, x, gdacs_index), idx_world_all))
    assert len(indexes) < 2, "Multiple countries"
    res = WORLD.loc[indexes, 'iso_a3'].values[0] if len(indexes) == 1 else np.nan
    return res




def is_relevant_country(poly, country_iso):
    """is there an overlap between the polygon and the country,
    defined by iso

    >>> country_iso = "AGO"
    """
    idx_world = WORLD.loc[WORLD["iso_a3"] == country_iso].index
    try:
        assert len(idx_world) == 1, "multiple countries found for {}".format(country_iso)
    except:
        # print("problem with {} return false".format(country_iso))
        return False
    else:
        try:
            poly_country = Polygon(WORLD.loc[idx_world, "geometry"].values[0])
        except NotImplementedError:
            poly_country = MultiPolygon(WORLD.loc[idx_world, "geometry"].values[0])
    return poly.intersects(poly_country)


def find_relevant_countries_for_poly(poly):
    """find countries crossed by poly

    :param: poly
    """
    iso_countries = np.unique(WORLD['iso_a3'])
    return list(filter(lambda iso: is_relevant_country(poly, country_iso=iso), iso_countries))



def find_relevant_countries_for_geojson(geojson_url_full):
    """find all countries crossed by polygons in the json files

    :param: poly
    """
    print(geojson_url_full)
    try:
        json_df = gpd.read_file(geojson_url_full)
    except Exception as e:
        print(e)
        time.sleep(CONNECTION_TIMEOUT)
        json_df = requests.get(geojson_url_full)
    try:
        json_df.columns.values
    except:
        time.sleep(CONNECTION_TIMEOUT)
        res = []
        return res
    # print("Number of observations: {}".format(len(json_df)))
    if 'geometry' not in json_df.columns.values:
        # print("Only {} found".format(json_df.columns.values))
        res = []
    else:
        rel_countries = []
        for idx in json_df.index:
            tst = find_relevant_countries_for_poly(json_df.loc[idx, 'geometry'])
            # print("added {} for {}".format(tst, idx))
            rel_countries = rel_countries + tst
        res = list(np.unique(rel_countries))
    return res


def find_relevant_countries_for_dir(gdacs_url, is_local=False):
    """

    :param gdacs_url:
    Note:
        1. currently only information from geojson files
    >>> gdacs_url = 'https://www.gdacs.org/datareport/resources/TC/10001/geojson_10001_1.geojson'
    """
    # Get name of the gdacs directory
    url_directory = "/".join(gdacs_url.split("/")[:-1]) + "/"
    # Get the names of all geojson files in the directory
    if is_local:
        event_id = extract_event_id_from_filename(gdacs_url)
        json_filenames = get_all_json_filenames_loc(eventid=event_id)
    else:
        json_filenames = get_all_json_filnames_from_url(url_directory)
    # initialize
    # print("Number of relevant files: {}".format(len(json_filenames)))
    rel_countries = []
    for i, f in enumerate(json_filenames):
        # print("Iteration: {} File {}".format(i, f.split("/")[-1]))
        tmp = find_relevant_countries_for_geojson(f)
        # print("Countires {}".format(tmp))
        rel_countries = rel_countries + tmp
    res = list(np.unique(rel_countries))
    return res



def find_relevant_gdacs_simple(idmc_idx,
                               idmc_df=idmc_df,
                               gdacs_df=gdacs_index,
                               tol_forward=10,
                               tol_backward=10):
    """

    """
    event_date = idmc_df.loc[idmc_idx, '#date+start']
    event_iso = idmc_df.loc[idmc_idx, 'iso3']
    # get rid of repetitive event ids(!!!!)
    gdacs_df_loc = gdacs_df.copy()  # to make sure the observations are not deleted from the main file
    gdacs_df_loc.drop_duplicates(["event_id"], inplace=True)
    # STEP 1: extract potential relevant dates:
    dates_cand = list(filter(lambda x: is_relevant_date(idmc_date=event_date,
                                                        gdacs_idx=x,
                                                        gdacs_df=gdacs_df_loc,
                                                        tol_forward=tol_forward,
                                                        tol_backward=tol_backward),
                             gdacs_df_loc.index))
    # STEP 2: get directories you need to explore
    dirs_to_explor = gdacs_df_loc.loc[dates_cand, "url"]
    #print("Relevant dates for {}\n{}".format(event_date, gdacs_df_loc.loc[dates_cand, "fromdate"]))
    # STEP 3: collect country info for all relevent events:
    country_dict = {get_event_id_from_geojson_url(key):
                        find_relevant_countries_for_dir(key)
                    for key in dirs_to_explor}
    # STEP 4: filter event ids by countr
    rel_events = {k: v for k, v in country_dict.items() if event_iso in v}
    cand_event_id = list(rel_events.keys())
    res = cand_event_id[0] if len(rel_events) == 1 else cand_event_id
    return res


# Define global variables, used to observe the function execution
count = 0               # number of function calls
found = 0               # number of potential mathces found so far


def find_relevant_gdacs_recursive(idmc_idx,
                                  idmc_df=idmc_df,
                                  gdacs_df=gdacs_index):
    """

    """
    global count, found
    try:
        count
    except NameError:
        count = 0
        found = 0
    #--------------------------- for the first run, try exact matching
    tol_forward = tol_backward = 0
    # TODO: Select by event type
    event_type_idmc = idmc_df.loc[idmc_idx, '#crisis+type']
    event_type_gdacs = idmc_gdacs_event_types.get(event_type_idmc, 'None')
    if event_type_gdacs != "None":
        # gdacs_df_loc = gdacs_df.loc[gdacs_df['type'].isin(event_type_gdacs)]
        gdacs_df_loc = gdacs_df.copy()
        res = find_relevant_gdacs_simple(idmc_idx,
                                         idmc_df=idmc_df,
                                         gdacs_df=gdacs_df_loc,
                                         tol_forward=tol_forward,
                                         tol_backward=tol_backward)
        while (len(res) == 0) & (tol_forward < 10):
            # print("Increase search range by 5: {}".format(tol_forward))
            tol_forward += 5
            tol_backward += 5
            res = find_relevant_gdacs_simple(idmc_idx,
                                             idmc_df=idmc_df,
                                             gdacs_df=gdacs_df_loc,
                                             tol_forward=tol_forward,
                                             tol_backward=tol_backward)
    else:
        res = np.nan
    # ---- Output logging
    if len(res) > 0:
        found += 1
    print("Count {} Found {} Index {} result: {}".format(count, found, idmc_idx, res))
    count += 1
    return res


#---------------------------- IDMC db:


def collect_geojson(gdacs_event_id, gdacs_event_type='FL', vars_unique=vars_unique):
    """collect information from all geojson files in the directory

    TODO: handle the case when column attributes are different between files.

    :param gdacs_event_id:
    :param gdacs_event_type:

    :return: geopandas dataframe with all information from geojson files in the directory.
    """
    url_gdacs = gdacs_index.loc[(gdacs_index['type'] == gdacs_event_type)
                                & (gdacs_index['event_id'] == gdacs_event_id), 'url'].values[0]
    url_dir = "/".join(url_gdacs.split("/")[:-1]) + "/"
    # Get geojson files
    json_files =  get_all_json_filnames_from_url(url_gdasc=url_dir)
    print(json_files[0])
    # Collect info in 1 dataframe
    geo_df = DataFrame()
    for jf in json_files:
        try:
            tmp_df = gpd.read_file(jf)
        except Exception as e:
            print(e)
            time.sleep(CONNECTION_TIMEOUT)
            tmp_df = gpd.read_file(jf)
        geo_df = pd.concat([geo_df, tmp_df])
    try:
        geo_df.drop_duplicates(vars_unique, inplace=True)
    except KeyError:
        print(geo_df.columns.values)
        pass
    return geo_df



def unambiguous_matching(dict_elem, event_type="FL"):
    """if there are several potential mathches for idmc event,
    select the best candidate.

    Selection criteria:
        1) has same 'country' attribute as idmc event
        2) has a closer date

    NOTE: potentially could use event description for mathcing.
          seems to be a promising approach for TC

    TODO: add try_catch decorator

    :param dict_elem: (idmc_idx : [gdacs_index_1, gdacs_index_2, ...]) output of running find_relevant_gdacs_recursive
    :return: (int) - gdacs_index of the "best" matching gdacs event

    >>> dict_elem = list(tc_dict.items())[2]
    >>> idmc_gdacs_fl_fin = [(x[0], try_unambiguous_matching(x, event_type="FL")) for x in list(flood_dict.items())]
    """
    if len(dict_elem[1]) == 0:
        res = np.nan
    elif len(dict_elem[1]) == 1:
        res = dict_elem[1][0]
    else:
        print("Need to disambiguate {}".format(dict_elem[1]))
        idx_idmc = dict_elem[0]
        iso_idmc = idmc_df.loc[idx_idmc, 'iso3'].strip().upper() # CHANGED
        country_idmc = idmc_df.loc[idx_idmc, 'country'].strip().title()
        datetime_idmc = try_convert_date(idmc_df.loc[idx_idmc, '#date+start'], date_fmt=IDMC_DATE_FMT)
        # INITIALIZE
        best_event_id = -1
        best_time_difference = 30    # the difference between idmc event date and gdacs event date is less than 15 days
        # Initialize res
        out_df = DataFrame()
        for event_id in dict_elem[1]: # event_id = 1000144
            # gdacs_url = gdacs_index.loc[(gdacs_index['type'] == event_type)&(gdacs_index['event_id'] == event_id), 'url']
            tmp = collect_geojson(gdacs_event_id=event_id, gdacs_event_type=event_type)
            # out_df =
            try:
                tmp.drop_duplicates(['fromdate', 'eventid', 'iso3'], inplace=True)
            except Exception as e:
                print(e)
            else:
                if (iso_idmc in tmp['iso3']) or (country_idmc in [str(x).strip().title() for x in tmp['country']]):
                    assert len(np.unique(tmp['fromdate'].dropna())) == 1, "Multiple dates for event {}".format(event_id)
                    dgacs_datetime = try_convert_date(tmp['fromdate'].values[0], GDACS_DATE_FMT)
                    diff_loc = abs((dgacs_datetime - datetime_idmc).days)
                    if diff_loc < best_time_difference:
                        best_event_id = event_id
                        print("SELECT BY COUNTRY AND DATE:\nBetter difference: {} days\n{}\n{}".format(diff_loc,
                                                                   idmc_df.loc[idx_idmc, '#description'],
                                                                   np.unique(tmp['description'].dropna())[0]))
        #  IF DID NOT FINE SAME COUTNRY ISO -> SELECT CLOSEST DATE
        if best_event_id == -1:
            for event_id in dict_elem[1]:
                # gdacs_url = gdacs_index.loc[(gdacs_index['type'] == event_type)&(gdacs_index['event_id'] == event_id), 'url']
                tmp = collect_geojson(event_id, gdacs_event_type=event_type)
                # out_df =
                try:
                    tmp.drop_duplicates(['fromdate', 'eventid', 'iso3'], inplace=True)
                except Exception as e:
                    print(e)
                else:
                    dgacs_datetime = try_convert_date(tmp['fromdate'].values[0], GDACS_DATE_FMT)
                    diff_loc = abs((dgacs_datetime - datetime_idmc).days)
                    if diff_loc < best_time_difference:
                        best_event_id = event_id
                        best_time_difference = diff_loc
                        print("SELECT BY DAYS: Better difference: {} days country {} country gdacs {}\n{}\n{}".format(diff_loc,
                                                                          country_idmc,  np.unique(tmp['country'].dropna())[0],
                                                                          idmc_df.loc[idx_idmc, '#description'],
                                                                          np.unique(tmp['description'].dropna())[0]))
        res = best_event_id
    return res




def try_unambiguous_matching(dict_elem, event_type="FL"):
    """wrapper around the unambiguous_matching, that allows to
    continue the execution despite potential connection errors

    :param dict_elem:
    :param event_type:
    :return:


    >>> idmc_gdacs_tc_fin = [(x[0], try_unambiguous_matching(x, event_type="TC")) for x in list(tc_dict.items())]
    """
    try:
        res = unambiguous_matching(dict_elem=dict_elem, event_type=event_type)
    except Exception as e:
        print(e)
        time.sleep(CONNECTION_TIMEOUT)
        try:
            res = unambiguous_matching(dict_elem=event_type, event_type=event_type)
        except Exception as e:
            print("Second interruption due to:\n{}".format(e))
            res = np.nan
    return res


#-----------------------------# PRODUCE HAZARD FILES



def create_hazard_files(idmc_gdacs_fin, hazard_vars=hazard_vars_storm_short):
    """

    :param idmc_gdacs_fin (DataFrame):
    :return:

    # Sheet 1: centroid_id, longitude latitude
    # Sheet 2: centroid_id
    idmc_gdacs_fin = newdf_tc

    >>> idmc_gdacs_fin = pd.read_csv(os.getcwd() + "/" + "idmc_gdacs_fl_fin.csv")
    >>> idmc_gdacs_tc_fin = pd.read_csv(os.getcwd() + "/" + "idmc_gdacs_tc_dirty.csv")
    """
    # Initialize pandas dataframe, that will later be transformed
    all_harard_vars = hazard_vars + ['idmc_index','gdacs_event_id', '']
    hazard_helper_df = DataFrame(columns=all_harard_vars)
    cnt = 0
    for idx in idmc_gdacs_fin.index:  # idx = 0 [:200]  idx = idmc_gdacs_fin.index[2663]
        print("Iteration {}".format(cnt))
        gdacs_event = idmc_gdacs_fin.loc[idx, 'gdacs_event_id']       #,
        gdace_ = idmc_gdacs_fin.loc[idx,  'event_type']
        if str(gdacs_event) != "nan":
            gdacs_event = int(gdacs_event)
            out_df = collect_geojson(gdacs_event_id=gdacs_event, gdacs_event_type=gdace_event_type)
            if 'polygonlabel' in out_df.columns.values:
                # DROP DUPLICATES ?
                out_df_relevant = out_df.loc[out_df['polygonlabel'] == 'Centroid', hazard_vars]
            elif "Class" in out_df.columns.values:
                out_df_relevant = out_df.loc[out_df["Class"] == 'Point_Centroid', hazard_vars]
            else:
                print("Can't find selection attributes for ")
                print(out_df.columns.values)
                out_df_relevant = out_df[hazard_vars]
            # ADD INFORMATION ABOUT INDEX
            for varname in ['idmc_index','gdacs_event_id', 'event_type']:
                out_df_relevant[varname] = idmc_gdacs_fin.loc[idx, varname]
            hazard_helper_df = pd.concat([hazard_helper_df, out_df_relevant])
        else:
            res = np.nan
        cnt += 1
        # SAVE hazard_helper_df in the correct format
    return hazard_helper_df
    # hazard_helper_df.to_csv(os.getcwd() + "/" + "idmc_gdacs_fl_hazard.csv", index=False)



#-----------------------------------------# Sample usage script
'''
This is a small example how to use the functions
Goal: do gdacs - idmc matching for tc events
Note: I have divided idmc index in 3 parts: [:val1], [val1:val2], 
'''

if __name__ == "__main__":
    # LOAD GDACS INDEX FOR STORMS
    # _full = contains information of all geojson files in the relevant directory, not only with unique event_ids
    # index_tc_full = "/Users/sychevaa/Desktop/Hack4Good/GitLabRepository/captain-obvious/GDACS_index_tc_full.csv"
    # index_tc_df = pd.read_csv(index_tc_full)
    # index_tc_df_full = index_tc_df.loc[index_tc_df["fromdate"].isnull() == False]
    index_ts_alt = gdacs_index.loc[gdacs_index['type'] == "EQ"] # Earthquake
    # SELECT ONLY STORM EVENTS
    idx_input = idmc_df.loc[idmc_df['#crisis+type'] == "Earthquake"].index
    val1 = 552
    val2 = 1104  # 782 error:
    tc_dict = {idx: find_relevant_gdacs_recursive(idx,
                                                 gdacs_df=index_ts_alt, # index_tc_df_full,
                                                 idmc_df=idmc_df)
               for idx in idx_input}
            # Second run: idx_input[val1:val2], Third run: idx_input[val2:]


