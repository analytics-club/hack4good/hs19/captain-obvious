"""
this script stores the collection of functions I use to find
correspondence between the events in DGACS and IDMC displacement database

"""
# General:
from datetime import datetime, timedelta
import time

# Data Analysis
import numpy as np
import pandas as pd
from pandas import Series, DataFrame
import geopandas as gpd

# Working with spatial data
import shapely
from shapely.geometry import Point, Polygon, MultiPolygon

# Web Scrapping
import re
import requests
import urllib.request
from bs4 import BeautifulSoup
import sys
import os

# Local
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))
from ConfigurationClass import Configuration
import src.utils.subtasks as utils

#-- Initializations
config = Configuration()
path = config.param["REPO_DIRECTORY"]
path_data = config.param["DATA_DIRECTORY"]
WORLD = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
IDMC_DATE_FMT = "%Y-%m-%d"
GDACS_DATE_FMT = ["%d/%b/%Y %H:%M:%S", "%d/%b/%Y  %H:%M:%S"]
# TO BE CHANGED - MY PERSONAL JUDGEMENT !!!
# key: one of the values in the idmc_df '#crisis+type'
# value: one of the values in the  gdacs 'type'
idmc_gdacs_event_types = { "Flood": ['FL'], 'Storm': ['TC'],
                           'Earthquake': ['EQ'],
                           'Volcanic eruption' : ['VO'],
                           'Drought': ['DR'],
                           'Wet Mass Movement': ['FL', 'TC'],
                            'Mass Movement': ['FL', 'TC', 'VO', 'EQ'],
                            'Dry mass movement': ['FL', 'TC', 'VO', 'EQ']
                            }
CONNECTION_TIMEOUT = 20

#------------------# SMALL HELPER FUNCTIONS

def get_event_id_from_geojson_url(geojson_url_full):
    """

    >>> geojson_url_full = 'https://www.gdacs.org/datareport/resources/FL/3889/geojson_3889_1.geojson'

    """
    return int(str(geojson_url_full).split("/")[-2])

#------------------# FUNCTIONS TO SELECT CORRECT DATES

def try_convert_date(date_string, date_fmt):
    """convert a string with data information to a datetime object
    BEWARE, you might want to change your machines time format to English, else strptime might not work (it uses the
    shell's date format for the abbreviation of the month ("%b").
    :param date_fmt:
    :param date_string:

    :return: a datetime object or
             np.nan if the formatting is unsuccessufl
    """
    if type(date_fmt) == 'str':
        date_fmt = [date_fmt]
    for this_format in date_fmt:
        try:
            res = datetime.strptime(str(date_string), this_format)
            return res
        except Exception as e:
            print(e)
            res = np.nan
    return res


def get_date(fromtodate, with_seconds=False):
    """
    extract date from the string provided in the json files in GDACS

    :param fromtodate:  a date string, that can be found in the geojson files
    :param with_seconds: convert the H:M:S information as well or just use Y-M-D?
    :return: datetime object

    Notes:
        1. datetime formating in python:  https://stackabuse.com/how-to-format-dates-in-python/
        2. why to use with_seconds:

    TODO: 1) check the format is the same for all geojson files in GDACS

    Example:
    fromtodate = "20/Aug/2013 18:00:00"
    get_date(fromtodate) == "20-08-2013" (or something like this)
    """
    time_tmp = "%d/%b/%Y  %H:%M:%S" if with_seconds else "%d/%b/%Y"
    fromtodate = fromtodate if with_seconds else str(fromtodate).split(" ")[0]
    res = datetime.strptime(fromtodate, time_tmp)   # from datetime import datetime
    return res


def is_relevant_event(event_date, idx, gdacs_index):    #gdacs_url):
    """
    check if the geojson file is relevant

    :param event_date (str): date from the IDMC file, typically in the format "Y-m-d"
    :param gdacs_url (str):
    :return: (bool):

    TODO: instead of url, use event_id ?

    Test:
    event_date = "2018-07-01"
    gdacs_url = "https://www.gdacs.org/datareport/resources/TC/39568/geojson_39568_3.geojson"
    is_relevant_event(event_date, gdacs_u rl)
    """
    #df_event = gpd.read_file(gdacs_url)
    #assert len(df_event) == 1, "Multiple rows for the event {}".format(gdacs_url)
    #assert len(np.unique(df_event['fromdate'])) == 1, "Multiple starting dates in the file {}".format(gdacs_url)
    #assert len(np.unique(df_event['todate'])) == 1, "Multiple ending dates in the file {}".format(gdacs_url)
    # Problem: can have different attribute name
    try:
        start_date = get_date(gdacs_index.loc[idx, 'fromdate'], with_seconds=False)   # df_event['fromdate'].values[0]
        end_date = get_date(gdacs_index.loc[idx, 'todate'], with_seconds=False)
    except KeyError:
        pass
        # print('No attributes fromdate, todate among:\n{}'.format(df_event.columns.values))
    # Date
    date_idmc = datetime.strptime(event_date, IDMC_DATE_FMT)
    is_relevant = ((date_idmc-start_date).days >= 0) & ((end_date-date_idmc).days >=0)
    return is_relevant


def is_relevant_date(idmc_date, gdacs_idx, gdacs_df, tol_forward=10, tol_backward=10):
    """

    :param idmc_date:
    :param tol_forward: (days) 0 - exact matching.
    :param tol_backward: (days)

    >>> idmc_date = "2008-08-01"
    >>> gdacs_date = "11/Jan/2015 18:00:00"
    """
    date_latest = datetime.strptime(str(idmc_date), IDMC_DATE_FMT) + timedelta(days=tol_forward)
    date_earliest = datetime.strptime(str(idmc_date), IDMC_DATE_FMT) - timedelta(days=tol_backward)
    gdacs_date = gdacs_df.loc[gdacs_idx, "fromdate"]
    gdacs_datetime = try_convert_date(gdacs_date, GDACS_DATE_FMT)
    res = ((date_latest - gdacs_datetime).days >= 0) & ((gdacs_datetime - date_earliest).days >= 0)
    return res


#------------------# FUNCTIONS TO SELECT BY COUNTRY

def is_in_country(idx, idx_world, gdacs_index):
    """
    idx: index in gdacs_index data frame
    idx_world: index in the world data frame


    idx_world = 4
    idx = 10
    """
    try:
        poly = Polygon(WORLD.loc[idx_world, "geometry"])
    except NotImplementedError:
        poly = MultiPolygon(WORLD.loc[idx_world, "geometry"])
    point = Point(gdacs_index.loc[idx, 'longitude'], gdacs_index.loc[idx, "latitude"])
    return point.within(poly)


def find_country(idx):
    idx_world_all = WORLD.index
    indexes = list(filter(lambda x: is_in_country(idx, x, gdacs_index), idx_world_all))
    assert len(indexes) < 2, "Multiple countries"
    res = WORLD.loc[indexes, 'iso_a3'].values[0] if len(indexes) == 1 else np.nan
    return res


def is_relevant_country(poly, country_iso):
    """

    >>> country_iso = "AGO"
    """
    idx_world = WORLD.loc[WORLD["iso_a3"] == country_iso].index
    try:
        assert len(idx_world) == 1, "multiple countries found for {}".format(country_iso)
    except:
        # print("problem with {} return false".format(country_iso))
        return False
    else:
        try:
            poly_country = Polygon(WORLD.loc[idx_world, "geometry"].values[0])
        except NotImplementedError:
            poly_country = MultiPolygon(WORLD.loc[idx_world, "geometry"].values[0])
    return poly.intersects(poly_country)


def find_relevant_countries_for_poly(poly):
    """find countries crossed by poly

    :param: poly
    """
    iso_countries = np.unique(WORLD['iso_a3'])
    return list(filter(lambda iso: is_relevant_country(poly, country_iso=iso), iso_countries))


def find_relevant_countries_for_geojson(geojson_url_full):
    """find countries crossed by poly

    :param: poly
    """
    try:
        json_df = gpd.read_file(geojson_url_full)
    except Exception as e:
        print(e)
        time.sleep(CONNECTION_TIMEOUT)
        json_df = requests.get(geojson_url_full)
    try:
        json_df.columns.values
    except:
        time.sleep(CONNECTION_TIMEOUT)
        res = []
        return res
    # print("Number of observations: {}".format(len(json_df)))
    if 'geometry' not in json_df.columns.values:
        # print("Only {} found".format(json_df.columns.values))
        res = []
    else:
        rel_countries = []
        for idx in json_df.index:
            tst = find_relevant_countries_for_poly(json_df.loc[idx, 'geometry'])
            # print("added {} for {}".format(tst, idx))
            rel_countries = rel_countries + tst
        res = list(np.unique(rel_countries))
    return res


def find_relevant_countries_for_dir(gdacs_url):
    """

    Note:
        1. currently only information from geojson files

    """

    url_directory = "/".join(gdacs_url.split("/")[:-1]) + "/"
    # -----------------------------------------
    try:
        response = requests.get(url_directory)
    except Exception as e:
        print(e)
        time.sleep(CONNECTION_TIMEOUT)
        response = requests.get(url_directory)
    # ----------------------------------------
    soup = BeautifulSoup(response.text, "html.parser")
    one_a_tag2 = soup.findAll('a', href=True)
    links2 = [one_a_tag2[x]["href"] for x in range(len(one_a_tag2))]
    json_filenames = [url_directory + str(x).split("/")[-1] for x in
                      list(filter(lambda x: str(x).endswith('geojson'), links2))]
    # initialize
    # print("Number of relevant files: {}".format(len(json_filenames)))
    rel_countries = []
    for i, f in enumerate(json_filenames):
        # print("Iteration: {} File {}".format(i, f.split("/")[-1]))
        tmp = find_relevant_countries_for_geojson(f)
        # print("Countires {}".format(tmp))
        rel_countries = rel_countries + tmp
    res = list(np.unique(rel_countries))
    return res


def find_relevant_gdacs_simple(idmc_idx,
                               idmc_df,
                               gdacs_df,
                               tol_forward=10,
                               tol_backward=10,
                               local_data = False):
    """

    """
    event_date = idmc_df.loc[idmc_idx, '#date+start']
    event_iso = idmc_df.loc[idmc_idx, 'iso3']
    # get rid of repetitive event ids(!!!!)
    gdacs_df_loc = gdacs_df.copy()  # to make sure the observations are not deleted from the main file
    gdacs_df_loc.drop_duplicates(["event_id"], inplace=True)
    # STEP 1: extract potential relevant dates:
    dates_cand = list(filter(lambda x: is_relevant_date(idmc_date=event_date,
                                                        gdacs_idx=x,
                                                        gdacs_df=gdacs_df_loc,
                                                        tol_forward=tol_forward,
                                                        tol_backward=tol_backward),
                             gdacs_df_loc.index))
    if len(dates_cand) == 0:
        return []
    # STEP 2: get directories you need to explore
    dirs_to_explor = gdacs_df_loc.loc[dates_cand, "url"]
    this_event = get_event_id_from_geojson_url(key)

    #print("Relevant dates for {}\n{}".format(event_date, gdacs_df_loc.loc[dates_cand, "fromdate"]))
    # STEP 3: collect country info for all relevant events:
    if local_data:
        country_dict = {get_event_id_from_geojson_url(key):
                            find_relevant_countries_for_dir(key)
                        for key in dirs_to_explor}
    else: country_dict = {get_event_id_from_geojson_url(key):
                        find_relevant_countries_for_dir(key)
                    for key in dirs_to_explor}
    # STEP 4: filter event ids by countr
    rel_events = {k: v for k, v in country_dict.items() if event_iso in v}
    cand_event_id = list(rel_events.keys())
    res = cand_event_id[0] if len(rel_events) == 1 else cand_event_id
    return res


# Define global variables
count = 0               # number of function calls
found = 0               # number of potential mathces found so far

def find_event_type_from_gdacs_url(url):
    event_type = re.findall('resources/(\w+)/.*?', url)[0]
    return event_type

def enrich_gdacs_idx_with_event_type(gdacs_df):
    gdacs_df_enriched = gdacs_df.copy()
    # all_possible_events = [item for sublist in idmc_gdacs_event_types.values() for item in sublist]
    gdacs_df_enriched['event_type'] = gdacs_df_enriched.apply(lambda row: find_event_type_from_gdacs_url(row.url), axis=1)
    return gdacs_df_enriched

def find_relevant_gdacs_recursive(idmc_idx,
                                  idmc_df,
                                  gdacs_df,
                                  local_data = True):
    """

    """
    global count, found
    tol_forward = tol_backward = 0
    # TODO: Select by event type
    event_type_idmc = idmc_df.loc[idmc_idx, '#crisis+type']
    event_type_gdacs = idmc_gdacs_event_types.get(event_type_idmc, 'None')
    if event_type_gdacs != "None":
        for this_event_type in event_type_gdacs: # TODO implement growing dataframe by event type
            # local_data =
            gdacs_df_loc = gdacs_df[gdacs_df.loc[:, "event_type"] == this_event_type]
            # gdacs_df_loc = gdacs_df.loc[gdacs_df['type'].isin(event_type_gdacs)]
            res = find_relevant_gdacs_simple(idmc_idx,
                                             idmc_df=idmc_df,
                                             gdacs_df=gdacs_df_loc,
                                             tol_forward=tol_forward,
                                             tol_backward=tol_backward)
            while (len(res) == 0) & (tol_forward < 10):
                # print("Increase search range by 5: {}".format(tol_forward))
                tol_forward += 5
                tol_backward += 5
                res = find_relevant_gdacs_simple(idmc_idx,
                                                 idmc_df=idmc_df,
                                                 gdacs_df=gdacs_df_loc,
                                                 tol_forward=tol_forward,
                                                 tol_backward=tol_backward)
    else:
        res = np.nan
    # ---- Output logging
    if len(res) > 0:
        found += 1
    print("Count {} Found {} Index {} result: {}".format(count, found, idmc_idx, res))
    count += 1
    return res


#-----------------------------------------# Sample usage script
'''
This is a small example how to use the functions
Goal: do gdacs - idmc matching for tc events
Note: I have divided idmc index in 3 parts: [:val1], [val1:val2], 
'''

if __name__ == "__main__":
    # LOAD GDACS INDEX
    index_tc_full = "/Users/sychevaa/Desktop/Hack4Good/GitLabRepository/captain-obvious/GDACS_index_tc_full.csv"
    index_tc_df = pd.read_csv(index_tc_full)
    index_tc_df_full = index_tc_df.loc[index_tc_df["fromdate"].isnull() == False]
    # LOAD IDMC DISPLACEMENTS
    idmc_df = utils.read_in_idmc_disaster()
    # SELECT ONLY STORM EVENTS
    idx_input = idmc_df.loc[idmc_df['#crisis+type'] == "Storm"].index
    val1 = 552
    val2 = 1104
    tc_dict = {idx: find_relevant_gdacs_recursive(idx,
                                                 gdacs_df=index_tc_df_full,
                                                 idmc_df=idmc_df)
               for idx in idx_input[:val1]}
    # Second run: idx_input[val1:val2], Third run: idx_input[val2:]

'''

index_tc_full = "/Users/sychevaa/Desktop/Hack4Good/GitLabRepository/captain-obvious/GDACS_index_tc_full.csv"
index_tc_df = pd.read_csv(index_tc_full)
index_tc_df_full = index_tc_df.loc[index_tc_df["fromdate"].isnull()==False]



#gdacs_tc = pd.read_csv(index_tc_full)







#----------------------------------#
pdb.run(find_relevant_gdacs_recursive(47,gdacs_df=index_tc_df_full, 
                                     idmc_df=idmc_df))

global count, found
tol_forward = tol_backward = 0
# TODO: Select by event type
event_type_idmc = idmc_df.loc[idmc_idx, '#crisis+type']
event_type_gdacs = idmc_gdacs_event_types.get(event_type_idmc, 'None')

tol_forward = tol_backward = 0
gdacs_df_loc = index_tc_df_full
res = find_relevant_gdacs_simple(idmc_idx,
                                 idmc_df=idmc_df,
                                 gdacs_df=gdacs_df_loc,
                                 tol_forward=tol_forward,
                                 tol_backward=tol_backward)

event_date = idmc_df.loc[idmc_idx, '#date+start']
event_iso = idmc_df.loc[idmc_idx, 'iso3']
# get rid of repetitive event ids(!!!!)

gdacs_df_loc = gdacs_df.copy()  # to make sure the observations are not deleted from the main file
gdacs_df_loc.drop_duplicates(["event_id"], inplace=True)
# STEP 1: extract potential relevant dates:

dates_cand = list(filter(lambda x: is_relevant_date(idmc_date=event_date,
                                                    gdacs_idx=x,
                                                    gdacs_df=gdacs_df_loc,
                                                    tol_forward=tol_forward,
                                                    tol_backward=tol_backward),
                         gdacs_df_loc.index))
                         
                         
dirs_to_explor = gdacs_df_loc.loc[dates_cand, "url"]
print("Relevant dates for {}\n{}".format(event_date, gdacs_df_loc.loc[dates_cand, "fromdate"]))
# STEP 3: collect country info for all relevent events:
country_dict = {get_event_id_from_geojson_url(key):
                    find_relevant_countries_for_dir(key)
                for key in dirs_to_explor.values}
                
'''
