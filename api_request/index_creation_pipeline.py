'''SAMPLE PIPLINE OF GDACS INDEX FILE CREATION AND IDMC EVENT SUBSEQUENT MATCHING

This script contains examples of

    * how to create an index file with meta information about all disaster events mentioned by GDACS
    * how to match the events from IDMC with event from GDACS
    * how to extract additional information from GDACS about the number of displace people for Flood events

TODO: add command line user interface
'''


#---- Imports
# General
import os

# Data analysis
import numpy as np
import pandas as pd
from pandas import Series, DataFrame

# Local module imports
# TODO: add imports from hazard_api.py and idmc_gdacs_query.py

#---------- Define important constants:

# URL for GDACS index creation
url_tc = 'https://www.gdacs.org/datareport/resources/TC/'   # tropical cyclone, done
url_eq = 'https://www.gdacs.org/datareport/resources/EQ/'   # earthquakes to-do
url_fl = 'https://www.gdacs.org/datareport/resources/FL/'   # floods, done
url_vo = 'https://www.gdacs.org/datareport/resources/VO/'   # volcanic erruption, done
url_dr = 'https://www.gdacs.org/datareport/resources/DR/'   # drought,  done
url_vw = 'https://www.gdacs.org/datareport/resources/VW/'   # don't know what it is but why not?, done

#-------- CREATE GDACS INDEX FILE
# file with meta infomration from all disaster events, listed in theis web page


create_index_file(outputfilename="gdacs_index_all.csv")
gdacs_index = pd.read_csv(os.getcwd() + "/" + "gdacs_index_all.csv")

# In many geojson files iso3 code of the country is missing -> infer it based on latitude/longitude information
gdacs_index['my_iso'] = [find_country(idx, gdacs_index=index_gdacs_all) for idx in index_gdacs_all.index]



#-------- Find idmc-gdacs event matches based on the gdacs_index file
# idmc_df is locally stored: Disaster-related new displacements by event in 2018
# dowloaded from http://www.internal-displacement.org/database/displacement-data

local_filename_displ_event = "/Users/sychevaa/Desktop/Hack4Good/idmc_disaster_all_dataset.csv" # to be changed on other computers
idmc_df = pd.read_csv(local_filename_displ_event, header=[1], encoding = "ISO-8859-1")
idmc_df.rename(columns={'#country+code': "iso3",
                        '#country+name': "country"}, inplace=True)
idmc_df['idx_idmc'] = idmc_df.index

idmc_gdacs_event_types = {"Earthquake":"EQ", "Floods":"FL"}
# see more correpsondence in idmc_gdacs_query in variable idmc_gdacs_event_types


# For every event in idmc database, potetntially relevant gdacs events
for (key,value) in idmc_gdacs_event_types.items():
    # concentrate only on idmc events of certain category
    idx_input = idmc_df.loc[idmc_df['#crisis+type'] == key].index
    # concentrate only on gdacs events from certain category
    gdacs_index_loc = gdacs_index.loc[gdacs_index['type'] == value]
    print("Number of events of type {} in idmc: {} in gdacs: {}".format(key, len(idx_input), len(gdacs_index_fl)))
    # variables to monitore the function execution
    count = 0
    found = 0
    flood_dict_3 = {idx:
           find_relevant_gdacs_recursive(idx, gdacs_df=gdacs_index_loc)
           for idx in idx_input}
    try_to_save_as_pickle(obj=flood_dict_3,
                          filepath=os.getcwd() + "/" + "idmc_gdacs_{}.csv".format(value.lower()))


# The procedure is really slow for TC, since information about every event is stored in many geojson files
# -> alternative approach

idmc_tc = idmc_df.loc[idmc_df['#crisis+type'] == "Storm"]
gdacs_tc = gdacs_index.loc[gdacs_index['type'] == "TC"]


idmc_merge = idmc_tc.merge(gdacs_tc,
                           on=['iso3', 'start_year', 'start_month'],
                           how = "inner" )


corresp = idmc_merge.groupby(["idx_idmc"])['event_id'].agg(lambda x: np.unique(x))
corresp_dict = {x[0]: append_help(x) for x in list(corresp.items())}

try_to_save_as_pickle(obj=corresp_dict,
                      filepath=os.getcwd() + "/" + "idmc_gdacs_tc_lazy.csv")


# PART 2: if the matching procedure yields several gdacs events, select the best match.

tc_dict = try_to_load_from_pickle(filepath=os.getcwd() + "/" + "idmc_gdacs_tc_lazy.csv")
idmc_gdacs_tc_fin = [(x[0], try_unambiguous_matching(x, event_type="TC")) for x in list(tc_dict.items())]

match_tc = DataFrame(idmc_gdacs_tc_fin)
match_tc.columns = ['idmc_index', 'gdacs_event_id']
match_tc['event_type'] = "TC"
match_tc.to_csv(os.getcwd() + "/" + "idmc_gdacs_tc_dirty.csv", index=False)



#-------- QUERY NUMBER OF DISPLACED PEOPLE FOR FLOOD EVENTS

gdacs_index_fl = DataFrame(gdacs_index.loc[gdacs_index['type']=="FL", "event_id"])

# Extract number of displaced people from GDACS from Floods
flood_displ_gdacs = [(x, try_extract_number_of_displaced_gdacs(x)) for x in gdacs_index_fl]

displ_gdacs = DataFrame(columns=["event_id", "displaced"])
displ_gdacs["event_id"] = [x[0] for x in flood_displ_gdacs]
displ_gdacs["displaced"] = [x[1] for x in flood_displ_gdacs]

displ_gdacs_notnull = displ_gdacs.loc[displ_gdacs["displaced"].isnull() == False]
print("Information about the number of displaced people was present for {} events out of {}".format(len(displ_gdacs_notnull),
                                                                                                    len(displ_gdacs)))

# Add metadata from gdacs_index file
displ_gdacs_notnull = displ_gdacs_notnull.merge(gdacs_index.loc[gdacs_index['type'] == "FL"], on="event_id", how="left")
displ_gdacs_notnull.to_csv(os.getcwd() + "/" + "additional_displcement_fl.csv", index=False)
