"""FUNCTIONS TO EXTRACT VARIOUS INFORMATION FROM GDACS WEB PAGE

this collection of functions allows the user to extract various information
about disaster events from https://www.gdacs.org/

Most important functions:

    * create_index_file:                 collect metadata about all files in the directory base_url
                                         this metadata is useful later for idmc-gdacs event matching

    * extract_number_of_displaced_gdacs: extract information about the number of displaced people for an event from
                                         GDACS database

TODO 1) separate functions and the script into two files, put function script into a package
     2) write a class tat
     3) Do same event id's correspond to the same datasets?
"""
#---------------------------# IMPORTS
# General
import re
import os
import pickle
import random
from datetime import datetime
import time

# Data Analysis
import numpy as np
import pandas as pd
from pandas import Series, DataFrame
import geopandas as gpd

# Web Scrapping
import requests
import urllib.request
from bs4 import BeautifulSoup



# ------------- DEFINE SOME IMPORTANT CONSTANTS

CONNECTION_TIMEOUT = 20

# URL of the main resource web pages
url_tc = 'https://www.gdacs.org/datareport/resources/TC/'   # tropical cyclone, done
url_eq = 'https://www.gdacs.org/datareport/resources/EQ/'   # earthquakes to-do
url_fl = 'https://www.gdacs.org/datareport/resources/FL/'   # floods, done
url_vo = 'https://www.gdacs.org/datareport/resources/VO/'   # volcanic erruption, done
url_dr = 'https://www.gdacs.org/datareport/resources/DR/'   # drought,  done
url_vw = 'https://www.gdacs.org/datareport/resources/VW/'   # don't know what it is but why not?, done


MISSING_ISO = ['', '   ', '%  ', 'nan', 'None']
INFO_TO_EXTRACT_DGACS = ["fromdate", "todate", "description",
                         'latitude', 'longitude', 'country', 'iso3']


#------------- SMALL HELPER FUNCTIONS

def convert_iso(x):
    res = np.nan if x in MISSING_ISO else x
    return res


def extract_event_id_url(gdac_url):
    """
    extract the event id, based on the url of a gdacs resource directory

    :param str gdac_url:  gdac_url with the address of the geojson file in https://www.gdacs.org/datareport/resources/
    :return int: gdacs event_id

    # EXAMPLE
    >>> gdac_url = "https://www.gdacs.org/datareport/resources/TC/39568/geojson_39568_3.geojson"
    >>> extract_event_id_url(gdac_url) == 39568
    """
    return int(str(gdac_url).split("/")[-2])

def append_help(x):
    try:
        len(x[1])
    except:
        res = []
        res.append(x[1])
    else:
        res = list(x[1])
    return res


def try_to_save_as_pickle(obj, filepath):
    """
    Try to dump an object as a pickle file.

    :param obj: Any object (should be serializable).
    :param str filepath: Any valid filepath.
    :return: None.

    TODO: transfer to some utils folder and import from there
    """
    try:
        with open(filepath, 'wb') as fh:
            pickle.dump(obj, fh)
            fh.close()
    except IOError:
        print("Could not save to file: ", filepath)
        return


def try_to_load_from_pickle(filepath):
    """
    Try to load an object from a pickle file.

    :param filepath: Must be a valid pickle file.
    :return: Loaded object.
    """
    try:
        with open(filepath, 'rb') as fh:
            obj = pickle.load(fh)
            fh.close()
            return obj
    except IOError:
        print("Could not read from file: ", filepath)
        quit()




#------------- FUNCTIONS

def extract_geojson_url_tc(event_date, soup, base_url): #base_url = url_tc):
    """
    extract links of geojson files for TC corresponding to
    loading ("event_date"). repeatedly called during the run of extract_geojson_url

    :param str event_date: date of the event, formatted as "month/date/Year"
    :param str base_url: contain the url of the web page from which we want to get information
    :param soup: obtained  by
                               response = requests.get(base_url)
                               soup = BeautifulSoup(response.text, "html.parser")
                 used as a separate parameter to decrease the total number of calls to base_url

    :return list(str): addresses of all json files, corresponding to the event_date
             -1 if no relevant information is found

    TODO: 1) the funcion is not recursive, can't handle the case if there are further sudirecotries
          2) automatically reformat the date
          3) check that geojson files contain information about the time, specified in the subdirectories. if no - add

    # EXAMPLE
    >>> event_date_test = "6/20/2018"
    >>> tst_address = extract_geojson_url_tc(event_date, base_url = url_tc)
    """
    # Find id of the subdirectories correpsonding to the loading date

    event_query = "{}[\s,\w]*:[\s,\w]*<[\s,\w]*>\s[\d]*".format(event_date)
    dir_id_tmp = re.findall(event_query, soup.text)
    dir_id = [str(x).split('<dir>')[1].strip() for x in  dir_id_tmp]

    # Initialize output
    geojson_links = []

    if len(dir_id) == 0:        # if no matches are discovered
        print("Did not find any information for {} in {}".format(event_date, base_url))
        eq_dates = np.unique(re.findall("[\d]*/[\d]*/[\d]*", soup.text))
        print("The folder contains information about {} dates:\n{}".format(len(eq_dates), eq_dates))
        geojson_links = -1

    else:   # if there are subdirectories, loaded at this date
        print("Found {} subdirectories".format(len(dir_id)))
        new_dirs = ["{}{}/".format(base_url, x) for x in dir_id]
        for next_url in new_dirs:
            # explore the elements in the next directory
            try:
                next_response = requests.get(next_url)
            except requests.exceptions.ConnectionError:
                # time_wait = 10
                print("Wait {} seconds".format(CONNECTION_TIMEOUT))
                time.sleep(CONNECTION_TIMEOUT)
                next_response = requests.get(next_url)
            finally:
                next_soup = BeautifulSoup(next_response.text, "html.parser")
                # find links corresponding to geojson files
                one_a_tag2 = next_soup.findAll('a', href=True)
                links2 = [one_a_tag2[x]["href"] for x in range(len(one_a_tag2))]
                # return valid url addresses of the geojson files
                json_filenames = [next_url + str(x).split("/")[-1] for x in
                        list(filter(lambda x: str(x).endswith('geojson'), links2))]
                print("Found {} geojson files in {}".format(len(json_filenames), next_url))
                geojson_links += json_filenames
    if geojson_links != -1:
        print("Found {} files for {}".format(len(geojson_links), event_date))
    return geojson_links


def extract_geojson_url(base_url=url_tc):
    """
    extract addresses of all geojson files in a directory

    :param str base_url:   url address of the gdacs page with all resources corresponding
                           to certain hazard type.
                           Expected on of the following addresses:
                           url_tc, url_eq, url_fl, url_vo, url_dr, url_vw (see the top of the script for definition)

    :return list(str): url addresses of all geojson files in the directory

    TODO: (potential improvements)
          1) extract information from xml files (some events only have xml)
          2) make it able to work with a different folder structure

    .. note: if subdirectory has too many folders, can get connection error before
             the processing is finished, need to work with this manually.
    """
    # query the web page
    response = requests.get(base_url)
    soup = BeautifulSoup(response.text, "html.parser")
    # extract all loading dates of the files
    eq_dates = np.unique(re.findall("[\d]*/[\d]*/[\d]*", soup.text))
    # initialize output
    all_json = []
    processed_dataes = []
    cnt = 0
    # loop over all loading dates
    for d in eq_dates:
        try:
            processed_dataes_loc = processed_dataes
            if d not in processed_dataes_loc:  # had a lot of problems with '4/8/2019' since there was too many file in corresponding subdirectory and  the execution was interrupted
                tmp = extract_geojson_url_tc(d, soup, base_url)
                all_json += tmp
                processed_dataes.append(d)
                print("Iteration {}, added {} total {}".format(cnt, len(tmp), len(all_json)))
                cnt += 1
            else:
                print("Already processed {}".format(d))
        except Exception as e:
            print(e)
    return all_json


def create_index_file_event_type(base_url, outputfilename, info_to_extract=INFO_TO_EXTRACT_DGACS, unique_id=True):
    """collect metadata about all files in the directory base_url
    this metadata is useful later for idmc gdacs event matching

    :param str base_url: url address of the gdacs resouce directory, containing information about all
                         events of certain type (i.e. 'TC' - tropicyl cyclone, 'FL' - flood, 'EQ' - earhquake )

                         Expected on of the following addresses:
                         url_tc, url_eq, url_fl, url_vo, url_dr, url_vw (see the top of the script for definition)

    :param list info_to_extract: what information from geojson files corresponding to certain event id should be
                                 extracted and included in the index file with metadata?

                                 Recommended: fromdate, todate, description, iso3, country, longitude, latitude

    :param bool unique_id: for every GDACS event id there might be several geojson files: file_eventid_1.geojson,
                           file_eventid_2.geojson, ...

                           True - for every unique event id, extract information from only 1 geojson file in the directory
                           False - extract information from all geojson files

    :param string outputfilename: filename of the created gdacs_index_file
                                  automatically stored in  os.getcwd() + "/" + outputfilename

    :return: None

    ..note: setting unique_id=False might significantly increase running time, expecially for TC or other hazards with
            many files per event

    TODO: (potential improvements)
           1) make sure the same directories are not explored multiple times. This can be achieved by having a local
              index file and checking whether an entry for the event id already exists
           2) extract event names for tropical storms provided in summary pages of gdacs
    """
    # extract url addresses of all geojson files in the directory
    all_json = extract_geojson_url(base_url=base_url)
    # initialize output dataframe
    GDACS_index = DataFrame(columns=info_to_extract)
    GDACS_index["url"] = all_json
    GDACS_index["event_id"] = [extract_event_id_url(x) for x in all_json]
    # How many unique event_ids do we have?
    event_num = len(np.unique(GDACS_index["event_id"]))
    print("Contains information about {} events".format(event_num))
    # Pragmatic solution: get unique event id.
    if unique_id:
        GDACS_index = GDACS_index.drop_duplicates("event_id").copy()
    GDACS_index.index = range(len(GDACS_index))
    # collect information from geojson files
    for i in range(len(GDACS_index)):
        url_loc = GDACS_index.loc[i, "url"]
        print("{} Iteration {} out of {}".format(time.time(), i, len(GDACS_index)))
        try:
            df_event = gpd.read_file(url_loc)
        except Exception as e:
            print(e)
            print("Wait {} seconds".format(CONNECTION_TIMEOUT))
            time.sleep(CONNECTION_TIMEOUT)
            try:  # second attempt
                df_event = gpd.read_file(url_loc)
            except Exception as e:
                print(e)
                print("Go for a new iteration")
                continue
        finally:                                           # if everything is fine, extract relevant information from geojson
            if "fromdate" not in df_event.columns.values:  # some files do not contain information about the event date - drop them
                print(df_event.columns.values)
                pass
            else:
                # assert len(np.unique(df_event["fromdate"])) == 1, "Multiple dates"
                for varname in info_to_extract:
                    try:                                   # different geojson files contain slightly different attributes, make sure that execution is not interrupted if some of vars_of_interest are not present
                        GDACS_index.loc[i, varname] = np.unique(df_event[varname])[0]  # .values
                    except KeyError:
                        continue
                    except Exception as e:                 # for unexpected cases
                        print(e)
                    else:
                        df_event[varname] = [str(x) for x in df_event[varname]]
                        GDACS_index.loc[i, varname] = np.unique(df_event[varname])[0]  # .values
                print("From/to date {} {}".format(GDACS_index.loc[i, "fromdate"],
                                                  GDACS_index.loc[i, "todate"]))
    if "iso3" in info_to_extract:                           # clean information about country iso code in the files
        try:
            GDACS_index["iso3"] = [convert_iso(x) for x in GDACS_index["iso3"]]
        except:
            pass
    GDACS_index.to_csv(os.getcwd() + "/" + outputfilename, index=False)


def create_index_file(outputfilename="gdacs_index_all.csv", info_to_extract=INFO_TO_EXTRACT_DGACS, unique_id=True):
    """create a gdacs index file that contains metadata of all disaster events
    with types 'FL', 'VO', 'TC', 'EQ', 'DR', 'VW'
    stored in the https://www.gdacs.org/datareport/resources/ directory

    :param list info_to_extract: same as in create_index_file_event_type
    :param bool unique_id: same as in create_index_file_event_type

    :return:  None: saves a dataframe with outputfilename to os.getcwd()

    .. note: see create_index_file_event_type for more details
    """
    create_index_file_event_type(base_url=url_fl, outputfilename="GDACS_index_fl.csv",
                                 info_to_extract=info_to_extract, unique_id=unique_id)
    # alternatively: create_index_file(base_url=url_fl, outputfilename="GDACS_index_fl_full.csv", unique_id=False)
    create_index_file_event_type(base_url=url_tc, outputfilename="GDACS_index_tc.csv",
                                 info_to_extract=info_to_extract, unique_id=unique_id)
    create_index_file_event_type(base_url=url_eq, outputfilename="GDACS_index_eq.csv",
                                 info_to_extract=info_to_extract, unique_id=unique_id)
    create_index_file_event_type(base_url=url_vo, outputfilename="GDACS_index_vo.csv",
                                 info_to_extract=info_to_extract, unique_id=unique_id)
    create_index_file_event_type(base_url=url_dr, outputfilename="GDACS_index_dr.csv",
                                 info_to_extract=info_to_extract, unique_id=unique_id)
    create_index_file_event_type(base_url=url_vw, outputfilename="GDACS_index_vw.csv",
                                 info_to_extract=info_to_extract, unique_id=unique_id)
    # COMBINE ALL INFO
    FILE_FOLDER = os.getcwd() + "/"
    # Tropical cyclones
    index_tc = pd.read_csv(FILE_FOLDER + "GDACS_index_tc.csv")
    index_tc['type'] = "TC"
    # Floods
    index_fl = pd.read_csv(FILE_FOLDER + "GDACS_index_fl.csv")
    index_fl['type'] = "FL"
    # Earthuquakes
    index_eq = pd.read_csv(FILE_FOLDER + "GDACS_index_eq.csv")
    index_eq['type'] = "EQ"
    # Volcanic eruptions
    index_vo = pd.read_csv(FILE_FOLDER + "GDACS_index_vo.csv")
    index_vo['type'] = "VO"
    # Droughts
    index_dr = pd.read_csv(FILE_FOLDER + "GDACS_index_dr.csv")
    index_dr['type'] = "DR"
    # Not sure what it is
    index_vw = pd.read_csv(FILE_FOLDER + "GDACS_index_vw.csv")
    index_vw['type'] = "VW"
    # Merge together and save
    index_gdacs_all = pd.concat([index_tc, index_fl, index_eq, index_vo, index_dr, index_vw])
    index_gdacs_all.to_csv(os.getcwd() + "/" + outputfilename, index=False)

#---------------------------------------# FUNCTIONS TO EXTRACT ADDITIONAL INFORMATION FROM GDACS webpage

# variables that help tracing execution of the program
count = 0
found = 0

def extract_number_of_displaced_gdacs(event_id, event_type="FL"):
    """extract information about the number of displaced people for an event from
    GDACS database

    :param int event_id: numeric event_id of the event assigned by GDACS
    :param int event_type: event type category assigned by GDACS
                           one of 'TC', 'FL', 'EQ' ... see https://www.gdacs.org/datareport/resources/

    ..note: currently only the event summary for Floods (FL) contain information about the number of displaced people
            that is why I set the event_type default value to 'FL'

    EXAMPLE
    >>> extract_number_of_displaced_gdacs(1000047)
    >>> extract_number_of_displaced_gdacs(1000055)
    """
    global count, found
    try:
        count
    except NameError:
        count = 0
        found = 0
    # Url template for various flood events
    url_loc = "https://www.gdacs.org/report.aspx?eventtype={}&eventid={}".format(event_type, event_id)
    # Pattern to be matched
    search_string = 'Displaced:[\s]*</td><td[\s]*class="cell_value_summary">[\s]*[-|\d]*'
    try:
        response = requests.get(url_loc)
    except Exception as e:
        print(e)
        time.sleep(CONNECTION_TIMEOUT)
        response = requests.get(url_loc)
    tst = re.findall(search_string, response.text)
    if len(tst) > 1:
        print("PROBLEM !!! multiple entries found in {} for {}".format(tst, event_id))
    #assert len(tst) == 1, "multiple entries found in {}".format()
    tst = tst[0]
    if tst[-1] == "-":
        res = np.nan
    else:
        found += 1
        res = [int(s) for s in tst.split(">") if s.isdigit()]
        if len(res) > 1:
            print("PROBLEM !!! multiple entries found in {} for {}".format(res, event_id))
        #assert len(res) == 1, "multiple numbers discovered in {}".format(res)
        res = res[0]
    count += 1
    print("Iteration: {}, Found {} Event id {} Result {}".format(count, found, event_id, res))
    return res


def try_extract_number_of_displaced_gdacs(event_id, event_type="FL"):
    """wrapper around the function extract_number_of_displaced_gdacs
    to ensure the execution is not interrupted by a single error

    :param int event_id: see extract_number_of_displaced_gdacs
    :param str event_type: see extract_number_of_displaced_gdacs

    :return: int Number of displaced people reported in GDACS summary page
             np.nan if an error was encountered
    """
    try:
        res = extract_number_of_displaced_gdacs(event_id, event_type=event_type)
    except Exception as e:
        print(e)
        print("PROBLEM WITH {}".format(event_id))
        res = np.nan
    return res