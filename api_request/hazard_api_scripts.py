
'''
PROBLEM WITH 1100014;
PROBLEM WITH 1100128;
PROBLEM WITH 3310;
PROBLEM WITH 1100165;
PROBLEM WITH 1100117;
'''


res1 = [(x, extract_number_of_displaced_gdacs(x)) for x in gdacs_index_fl["event_id"][:1000]]
try_to_save_as_pickle(obj=res1, filepath=os.getcwd()+"/"+"displaced_new_1.csv")

res2 = [(x, extract_number_of_displaced_gdacs(x)) for x in gdacs_index_fl["event_id"][1000:2000]]
try_to_save_as_pickle(obj=res2, filepath=os.getcwd()+"/"+"displaced_new_2.csv")

res3 = [(x, try_extract_number_of_displaced_gdacs(x)) for x in gdacs_index_fl["event_id"][2000:3000]]
try_to_save_as_pickle(obj=res3, filepath=os.getcwd()+"/"+"displaced_new_3.csv")

res4 = [(x, try_extract_number_of_displaced_gdacs(x)) for x in gdacs_index_fl["event_id"][3000:4000]] # 902
try_to_save_as_pickle(obj=res4, filepath=os.getcwd()+"/"+"displaced_new_4.csv")

res5 = [(x, try_extract_number_of_displaced_gdacs(x)) for x in gdacs_index_fl["event_id"][4000:]]
try_to_save_as_pickle(obj=res5, filepath=os.getcwd()+"/"+"displaced_new_5.csv")


# ---------------#

res_final_displacement = res1

 = try_to_load_from_pickle(filename1)
flood_dict_2 = try_to_load_from_pickle(filename2)
flood_dict_3 = try_to_load_from_pickle(filename3)
flood_dict_4 = try_to_load_from_pickle(filename4)
flood_dict_5 = try_to_load_from_pickle(filename5)

# Combine them all together
displacement_dict = res1
len(displacement_dict)
displacement_dict = displacement_dict + res2
len(displacement_dict)
displacement_dict = displacement_dict + res3
len(displacement_dict)
displacement_dict = displacement_dict + res4
len(displacement_dict)
displacement_dict = displacement_dict + res5
len(displacement_dict)

disp_new = DataFrame(columns = ["event_id", "displaced"])
disp_new["event_id"] = [x[0] for x in displacement_dict]
disp_new["displaced"] = [x[1] for x in displacement_dict]

sum(disp_new["displaced"].isnull() == False) # 3201
disp_new_notnull = disp_new.loc[disp_new["displaced"].isnull() == False]
disp_new_notnull_full = disp_new_notnull.merge(gdacs_index.loc[gdacs_index['type'] == "FL"], on = "event_id", how = "left")

len(disp_new_notnull_full)
sum(disp_new_notnull_full["iso3"].isnull()==False)


gdacs_fl = gdacs_index.loc[gdacs_index['type'] == "FL"]
disp_new_notnull_full['my_iso'] = [find_country(idx, gdacs_index=disp_new_notnull_full) for idx in disp_new_notnull_full.index]

disp_new_notnull_full.to_csv(os.getcwd() + "/" + "additional_displcement_fl.csv", index=False)

#----------------# get number of dipslaced people

gdacs_index = pd.read_csv(local_gdacs_index_filename)
gdacs_index = gdacs_index.loc[gdacs_index["fromdate"].isnull() == False]
gdacs_index_fl = DataFrame(gdacs_index.loc[gdacs_index['type']=="FL", "event_id"])

extract_number_of_displaced_gdacs(event_id)

# TOTAL: 4622
count = 0
found = 0
gdacs_index_fl["displaced"] = np.nan
gdacs_index_fl["displaced"] = [extract_number_of_displaced_gdacs(x) for x in gdacs_index_fl["event_id"]]

#----------------# GET NUMBER OF DISPLACED PEOPLE

gdacs_index.loc[gdacs_index['type'] == 'FL', ['event_id']].head()

#----------------# Matching IDMC - GDACS:

filename1 = "/Users/sychevaa/Desktop/Hack4Good/GitLabRepository/captain-obviousidmc_gdacs_ts_565.csv"
filename2 = "/Users/sychevaa/Desktop/Hack4Good/GitLabRepository/captain-obviousidmc_gdacs_ts_1130.csv"
filename3 = "/Users/sychevaa/Desktop/Hack4Good/GitLabRepository/captain-obvious/idmc_gdacs_ts_1695.csv"
filename4 = "/Users/sychevaa/Desktop/Hack4Good/GitLabRepository/captain-obvious/idmc_gdacs_ts_2260.csv"
filename5 = "/Users/sychevaa/Desktop/Hack4Good/GitLabRepository/captain-obvious/idmc_gdacs_ts_last.csv"
# LOAD ALL FLOOD DICT
flood_dict_1 = try_to_load_from_pickle(filename1)
flood_dict_2 = try_to_load_from_pickle(filename2)
flood_dict_3 = try_to_load_from_pickle(filename3)
flood_dict_4 = try_to_load_from_pickle(filename4)
flood_dict_5 = try_to_load_from_pickle(filename5)


tc_dict = try_to_load_from_pickle('/Users/sychevaa/Desktop/Hack4Good/GitLabRepository/captain-obvious/idmc_gdacs_tc_lazy.csv')

tc_dict_unique = try_unambiguous_matching(tc_dict , event_type="TC")
# 174

# ADD INFORMATION ABOUT LOCAL FILE LOCATION of TC FILES

gdacs_index


# Combine them all together
flood_dict = flood_dict_1
len(flood_dict)
flood_dict.update(flood_dict_2)
len(flood_dict)
flood_dict.update(flood_dict_3)
len(flood_dict)
flood_dict.update(flood_dict_4)
len(flood_dict)
flood_dict.update(flood_dict_5)
len(flood_dict)

# Save result in 1 object:
try_to_save_as_pickle(obj=flood_dict,
                      filepath=os.getcwd() + "/" + "idmc_flood_index.csv")


# dict_elem = list(flood_dict.items())[0]
# dict_elem = list(flood_dict.items())[1]
# dict_elem = list(flood_dict.items())[11]

# Test Japan: Typhoon Jebi - 31/8/2018 - will I find it?  "Tropical Cyclone JEBI-18"

id = idmc_df.loc[idmc_df["#description"] == "Japan: Typhoon Jebi - 31/8/2018"].index
gdacs_tc = gdacs_index.loc[gdacs_index['type'] == "TC"]
find_relevant_gdacs_recursive(3440], gdacs_df=gdacs_tc)
# 3440



# SAVE UNAMBIGUOUG INDEXES
# FLOOD
idmc_gdacs_fl_fin = [(x[0], try_unambiguous_matching(x, event_type="FL")) for x in list(flood_dict.items())]
newdf=DataFrame(idmc_gdacs_fl_fin)
newdf.columns = ['idmc_index', 'gdacs_event_id']
newdf['event_type'] = "FL"
newdf.to_csv(os.getcwd() + "/" + "idmc_gdacs_fl_fin.csv", index=False)


# TC
idmc_gdacs_tc_fin = [(x[0], try_unambiguous_matching(x, event_type="TC")) for x in list(tc_dict.items())]
newdf_tc=DataFrame(idmc_gdacs_tc_fin)
newdf_tc.columns = ['idmc_index', 'gdacs_event_id']
newdf_tc['event_type'] = "TC"
newdf_tc.to_csv(os.getcwd() + "/" + "idmc_gdacs_tc_dirty.csv", index=False)
len(np.unique(newdf_tc["idmc_index"]))

hazard_tc = create_hazard_files(idmc_gdacs_fin=newdf_tc)

create_hazard_files(idmc_gdacs_fin)

# General statistics on the number of matches
flood_idx = Series(flood_dict)

flood_idx.value_counts()   # 2167 unknown, found? - 658
len(flood_idx)             # 2825

# How many matches are unambiguous:  249
sum(flood_idx.value_counts() == 1)

# Good cases: 1 correspondence:

vars_idmc =  ['#date+start', 'iso3', '#description', '#affected+idps+ind+newdisp+disaster']
vars_gdacs = ['country', 'description', 'event_id', 'fromdate', 'iso3']


vars_gdacs_orig_1 = ['country', 'description', 'fromdate', 'todate', 'name', 'alertlevel', 'severity']
vars_gdacs_orig_2 = ['Class', 'polygontype', 'severity', 'latitude', 'longitude', 'geometry']
vars_unique = ['fromdate', 'todate','Class', 'polygontype',
               'severity', 'alertlevel', 'latitude', 'longitude']
climada_hazard_vars = ['latitude', 'longitude', 'severity']


#---------------------------------------------------------#
#---------------------------------------------------------#

idmc_idx = 28
gdacs_event_id = 4052
gdacs_event_type = "FL"

#idmc_idx = 5796
idmc_df.loc[idmc_idx, vars_idmc]
gdacs_index.loc[(gdacs_index['type'] == "FL")&(gdacs_index['event_id'] == gdacs_event_id), vars_gdacs]

out_df = collect_geojson(gdacs_event_id, gdacs_event_type='TC')
out_df[vars_gdacs_orig_1]
out_df[vars_gdacs_orig_2]


# Earthquake:           Index 776 result: [1065849, 1065792, 1065923]
# Tropical Cyclone:     Index 308 result: [1000150, 1000151]
# TC:
# Save two dataframes: all relevant info + like they want us

out_df.loc[out_df['Class'] == 'Point_Centroid', climada_hazard]

#-------- Problem Case a) 1 gdacs event-many idmc events:
flood_idx.loc[flood_idx ==  list([1000122])]
sum(flood_idx.values == list([1000122]))
# out_df[1000122]
list(flood_idx.items())
# (1039, [1000122]), (1041, [1000122]), (1071, [1000122])
# (1044, [1000122]), (1032, [1000122]), (1047, [1000122])
# (1093, [1000122]), (1108, [1000122, 1000130]), (1109, [1000122]),  (1078, [1000122])

gdacs_event_id = 1000105 # [1000103, 1000105, 1000108, 1000132] #1000130 #1000122
idx_idmc = 1042 #[1039, 1041, 1071, 1044, 1032, 1047, 1093, 1078, 1109, 1108]
idmc_df.loc[idx_idmc, vars_idmc]
gdacs_index.loc[(gdacs_index['type'] == "FL")&(gdacs_index['event_id'].isin(gdacs_event_id)), vars_gdacs]

tmp = collect_geojson(gdacs_event_id=1000105, gdacs_event_type='FL')
out_df[vars_gdacs_orig_1]
out_df[vars_gdacs_orig_2]

#-------- Problem Case b) 1 idmc events many gdac events
# solution 1: compare countries provided in gdacs with idmc,
(1042, [1000103, 1000105, 1000108, 1000132])


#--------- Other event types:
# Earthquake:           Index 776 result: [1065849, 1065792, 1065923]
# Tropical Cyclone:     Index 308 result: [1000150, 1000151]  #!

gdacs_event_id = 1000151 # [1000103, 1000105, 1000108, 1000132] #1000130 #1000122
idx_idmc = 308
gdacs_event_type = 'TC'
idmc_df.loc[308, vars_idmc]

gdacs_url = gdacs_index.loc[(gdacs_index['type'] == gdacs_event_type)
                            & (gdacs_index['event_id'] == gdacs_event_id), 'url'].values[0]

url_gdasc = "/".join(gdacs_url.split("/")[:-1]) + "/"
# Get geojson files
files_json =  get_all_json_filnames(url_gdasc=url_gdasc)

tmp = gpd.read_file(files_json[0])
out_df[vars_gdacs_orig_1]
out_df[vars_gdacs_orig_2]

out_df = collect_geojson(gdacs_event_id=1000150, gdacs_event_type='TC', vars_unique=vars_unique)

'''
idmc_idx = 782
gdacs_df=index_ts_alt

global count, found
tol_forward = tol_backward = 0
# TODO: Select by event type
event_type_idmc = idmc_df.loc[idmc_idx, '#crisis+type']
event_type_gdacs = idmc_gdacs_event_types.get(event_type_idmc, 'None')

tol_forward = tol_backward = 0
gdacs_df_loc = index_tc_df_full

res = find_relevant_gdacs_simple(idmc_idx,
                                 idmc_df=idmc_df,
                                 gdacs_df=gdacs_df_loc,
                                 tol_forward=tol_forward,
                                 tol_backward=tol_backward)


find_relevant_gdacs_recursive(idmc_idx,
                                  idmc_df=idmc_df,
                                  gdacs_df=gdacs_index)


gdacs_df_loc = gdacs_df.copy()  # to make sure the observations are not deleted from the main file
gdacs_df_loc.drop_duplicates(["event_id"], inplace=True)
# STEP 1: extract potential relevant dates:

dates_cand = list(filter(lambda x: is_relevant_date(idmc_date=event_date,
                                                    gdacs_idx=x,
                                                    gdacs_df=gdacs_df_loc,
                                                    tol_forward=tol_forward,
                                                    tol_backward=tol_backward),
                         gdacs_df_loc.index))


dirs_to_explor = gdacs_df_loc.loc[dates_cand, "url"]
print("Relevant dates for {}\n{}".format(event_date, gdacs_df_loc.loc[dates_cand, "fromdate"]))
# STEP 3: collect country info for all relevent events:
country_dict = {get_event_id_from_geojson_url(key):
                    find_relevant_countries_for_dir(key)
                for key in dirs_to_explor.values}

'''

# MERGE WITH DISPLACEMENT:
# Tropical Cyclons
gdacs_index_tc = gdacs_index.loc[gdacs_index["type"] == "TC"].copy()
gdacs_index_tc.rename(columns={"event_id":"gdacs_event_id"}, inplace=True) #["gdacs_event_id"] == np.nan gdacs_index_tc["eventid"]

idmc_gdacs_tc_fin = pd.read_csv(os.getcwd() + "/" + "idmc_gdacs_tc_dirty.csv", index=false)
idmc_gdacs_tc_tst = idmc_gdacs_tc_fin.merge(idmc_df, on = ['idmc_index'], how = "left")


idmc_gdacs_fl_tst2 = idmc_gdacs_fl_tst.merge(gdacs_index_tc, on=["gdacs_event_id"], how="left")
idmc_gdacs_fl_tst2[["#date+start", "fromdate"]]
idmc_gdacs_fl_tst.to_csv(os.getcwd() + "/" + "idmc_gdacs_tc_dirty_with_displacement.csv")

# Floods
gdacs_index_fl = gdacs_index.loc[gdacs_index["type"] == "FL"].copy()
gdacs_index_fl.rename(columns={"event_id":"gdacs_event_id"}, inplace=True)

idmc_gdacs_fl_fin = pd.read_csv(os.getcwd() + "/" + "idmc_gdacs_fl_fin.csv")
idmc_gdacs_fl_fin_notnull = idmc_gdacs_fl_fin.loc[idmc_gdacs_fl_fin["gdacs_event_id"].isnull()==False]

idmc_gdacs_fl_tst = idmc_gdacs_fl_fin_notnull.merge(idmc_df, on = ['idmc_index'], how = "left")
idmc_gdacs_fl_tst.to_csv(os.getcwd() + "/" + "idmc_gdacs_fl_with_displacement.csv", index=False)

idmc_gdacs_fl_tst2 = idmc_gdacs_fl_tst.merge(gdacs_index_fl, on=["gdacs_event_id"], how="left")
idmc_gdacs_fl_tst2[["#date+start", "fromdate"]]

#  INITIALIZE INDEX FILE

response = requests.get(url_eq)
soup = BeautifulSoup(response.text, "html.parser")
eq_dates = np.unique(re.findall("[\d]*/[\d]*/[\d]*", soup.text))

event_query = "{}[\s,\w]*:[\s,\w]*<[\s,\w]*>\s[\d]*".format(event_date)
dir_id_tmp = re.findall(event_query, soup.text)
dir_id = [str(x).split('<dir>')[1].strip() for x in dir_id_tmp]

new_dirs = ["{}{}/".format(base_url, x) for x in dir_id]

next_url = new_dirs[0]
next_response = requests.get(next_url)
next_soup = BeautifulSoup(next_response.text, "html.parser")
# Find id of the subdirectories correpsonding to the date


all_json = extract_geojson_url(base_url=url_eq)  # url_eq = 'https://www.gdacs.org/datareport/resources/EQ/'
#
GDACS_index = DataFrame(columns = info_to_extract)
GDACS_index["url"] = all_json
GDACS_index["event_id"] = [extract_event_id_url(x) for x in all_json]
# How many events do we have?  805
event_num = len(np.unique( GDACS_index["event_id"]))
print("Contains information about {} events".format(event_num))
# Pragmatic solution: get unique event id. We assume 1 event id = 1 event
GDACS_index_short = GDACS_index.drop_duplicates("event_id").copy()
GDACS_index_short.index = range(len(GDACS_index_short))
VARS = ["fromdate", "todate", "description"]
# add latitude and longitude  latlon = [ 'latitude', 'longitude']
for i in range(len(GDACS_index_short)): #  range(len(GDACS_index_short)) # i = 3
    url_loc = GDACS_index_short.loc[i, "url"]
    print("Iteration {}".format(i))
    try:
        df_event = gpd.read_file(url_loc)
    except Exception as e: # requests.exceptions.ConnectionError:
        print(e)
        print("Wait 10 seconds")
        time.sleep(10)
        df_event = gpd.read_file(url_loc)
    finally:
        #vars_loc = list(filter(lambda x: x in df_event.columns.values, VARS))
        if "fromdate" not in df_event.columns.values:
            print(df_event.columns.values)
            pass
        else:
            #assert len(np.unique(df_event["fromdate"])) == 1, "Multiple dates"
            for name in ["fromdate","todate","description", "latitude", "longitude", 'iso3']:
                try:
                    GDACS_index_short.loc[i, name] = np.unique(df_event[name])[0] #.values
                except Exception as e:
                    print(e)
                finally:
                    df_event[name] = [str(x) for x in df_event[name]]
                    GDACS_index_short.loc[i, name] = np.unique(df_event[name])[0]  # .values
            print("From/to date {} {}".format(GDACS_index_short.loc[i, "fromdate"],
                                            GDACS_index_short.loc[i, "todate"]))
if "iso3" in info_to_extract:
    GDACS_index_short["iso3"] = [convert_iso(x) for x in GDACS_index_short["iso3"]]
    # sum(GDACS_index_short["fromdate"].isnull())
GDACS_index_short.to_csv(os.getcwd() + outputfilename, index=False)




#------------- SCRIPT  (Original version from Vincent)

response = requests.get(url)
soup = BeautifulSoup(response.text, "html.parser")
one_a_tag = soup.findAll('a')

for i in range(2,100):
    link = one_a_tag[i]['href']
    print(link)
    open_url =  'https://www.gdacs.org'+ link
    response2 = requests.get(open_url)

    soup2 = BeautifulSoup(response2.text, "html.parser")
    one_a_tag2 = soup2.findAll('a')
    link2 = one_a_tag2[1]["href"]
    print(link2)

    download_url = "https://www.gdacs.org" + link2
    link2_short = link2[link2.find('n_')+2:]
    urllib.request.urlretrieve(download_url, config.param["DATA_DIRECTORY"] + "/gdacs/" + link2[link2.find('n_')+2:])



"""
# Combine all index files together

FILE_FOLDER = os.getcwd() + "/"
index_tc = pd.read_csv(FILE_FOLDER + "GDACS_index_tc.csv")
index_tc['type'] = "TC"

index_fl = pd.read_csv(FILE_FOLDER + "GDACS_index_fl.csv")
index_fl['type'] = "FL"

index_eq = pd.read_csv(FILE_FOLDER + "GDACS_index_eq.csv")
index_eq['type'] = "EQ"

index_vo = pd.read_csv(FILE_FOLDER + "GDACS_index_vo.csv")
index_vo['type'] = "VO"

index_dr = pd.read_csv(FILE_FOLDER + "GDACS_index_dr.csv")
index_dr['type'] = "DR"

index_vw = pd.read_csv(FILE_FOLDER + "GDACS_index_vw.csv")
index_vw['type'] = "VW"

index_gdacs_all = pd.concat([index_tc, index_fl, index_eq, index_vo, index_dr, index_vw])

# sanity check
len(index_gdacs_all.drop_duplicates(['event_id', 'type'])) == len(index_gdacs_all)

index_gdacs_all.to_csv(os.getcwd() + "/" + "gdacs_index_all.csv", index=False)
# index_gdacs_all = pd.read_csv(os.getcwd() + "/" + "gdacs_index_all.csv")
"""


#  create_index_file(base_url=url_fl, outputfilename="GDACS_index_fl.csv")  #, info_to_extract=INFO_TO_EXTRACT_DGACS, unique_id=True)
#  create_index_file(base_url=url_eq, outputfilename="GDACS_index_eq.csv")
#  create_index_file(base_url=url_vo, outputfilename="GDACS_index_vo.csv")  #, info_to_extract=INFO_TO_EXTRACT_DGACS, unique_id=True)
#  create_index_file(base_url=url_dr, outputfilename="GDACS_index_dr.csv")  #, info_to_extract=INFO_TO_EXTRACT_DGACS, unique_id=True)
#  create_index_file(base_url=url_vw, outputfilename="GDACS_index_vw.csv")  #, info_to_extract=INFO_TO_EXTRACT_DGACS, unique_id=True)


#  create_index_file(base_url=url_eq, outputfilename="GDACS_index_eq_full.csv", unique_id=False)  #, info_to_extract=INFO_TO_EXTRACT_DGACS, unique_id=True)
#  create_index_file(base_url=url_fl, outputfilename="GDACS_index_fl_full.csv", unique_id=False)  #, info_to_extract=INFO_TO_EXTRACT_DGACS, unique_id=True)
#  interrupted on  2366
#  create_index_file(base_url=url_tc, outputfilename="GDACS_index_tc_full.csv", unique_id=False)  #, info_to_extract=INFO_TO_EXTRACT_DGACS, unique_id=True)



