"""
this script stores the collection of functions I use to find
correspondence between the events in DGACS and IDMC displacement database

"""
# General:
from datetime import datetime, timedelta
import time

# Data Analysis
import numpy as np
import pandas as pd
import geopandas as gpd
import sys
import os

# Local functions
sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from idmc_gdacs_query_2 import find_relevant_gdacs_recursive

if __name__ == "__main__":
    local_filename_displ_event = "/media/lionel/data/captain-obvious/data/idmc/idmc_disaster_all_dataset.xlsx"
    local_gdacs_index_filename = "/media/lionel/data/captain-obvious/gdacs_index_all.csv"

    # DATAFRAMES
    WORLD = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
    # gdacs
    gdacs_index = pd.read_csv(local_gdacs_index_filename)
    gdacs_index = gdacs_index.loc[gdacs_index["fromdate"].isnull() == False]
    # idmc
    idmc_df = pd.read_excel(local_filename_displ_event, header = [1])
    idmc_df.rename(columns={'#country+code': "iso3",
                            '#country+name': "country"}, inplace=True)

    # LOAD GDACS INDEX
    index_tc_full = "/media/lionel/data/captain-obvious/GDACS_index_tc_full.csv"
    index_tc_df = pd.read_csv(index_tc_full)
    index_tc_df_full = index_tc_df.loc[index_tc_df["fromdate"].isnull() == False]
    # SELECT ONLY STORM EVENTS
    idx_input = idmc_df.loc[idmc_df['#crisis+type'] == "Storm"].index
    val1 = 552
    val2 = 1104
    tc_dict = {idx: find_relevant_gdacs_recursive(idx,
                                                 gdacs_df=index_tc_df_full,
                                                 idmc_df=idmc_df)
               for idx in idx_input[:val1]}
    # Second run: idx_input[val1:val2], Third run: idx_input[val2:]
