import requests
import sys
import urllib.request
import urllib.parse
import time
import os
from bs4 import BeautifulSoup
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))
from ConfigurationClass import Configuration
config = Configuration()

def scrape_geojson_gdacs(hazard_type, local_direc = None):
    """
    Given the dataset url, recursively crawl to each subfolder, scrape and download all geojson files in local directory.
    When it catches a conneciton error, it will wait for 30 seconds and retry. When it catches a connecitonreseterror,
    it will wait for 3 seconds and retry.
    The time was tested with tropical
    cyclone directory and might cause bugs in future time.

    param:
    baseurl: str, url link to gdacs dataset, for example, https://www.gdacs.org/datareport/resources/TC/
    local_direc: str, directory path to save the downloaded data
    """
    if '/' not in 'hazard_type':
        baseurl = 'https://www.gdacs.org/datareport/resources/' + hazard_type
        local_direc = config.param["DATA_DIRECTORY"] + '/gdacs/' + hazard_type
    else:
        if local_direc == None:
            print('I do not know where to save the file, please specify.')
            return None

    if not os.path.exists(local_direc):
        os.mkdir(local_direc)
        print("Created directory to save data!")
    else:
        print("Directory {} already exists!".format(local_direc))

    print("Start to scrape all geojson files at: \n{}".format(baseurl))

    try:
        resp = requests.get(baseurl)
        soup = BeautifulSoup(resp.text, "html.parser")
        one_a_tag = soup.findAll('a')
        print("Try downloading geojson files from:\n {}".format(baseurl))
        for i in range(1, len(one_a_tag)): # ignore first as it's parent directory
            link = one_a_tag[i]['href']
            new_url= urllib.parse.urljoin(baseurl, link)
            if new_url.endswith(".geojson"):
                try:
                    filename = os.path.join(local_direc, link.split("/")[-1])
                    if not os.path.exists(filename):
                        urllib.request.urlretrieve(url=new_url, filename=filename)
                except ConnectionResetError:
                    print("ConnectionResetError, try again after 3 seconds to download: \n{}".format(new_url))
                    time.sleep(3)
                    urllib.request.urlretrieve(url=new_url, filename=filename)
                except FileNotFoundError:
                    print("File Not Found: {}".format(filename))
                    pass
            elif new_url.endswith("/"):
                 scrape_geojson_gdacs(new_url, local_direc)
    except requests.exceptions.ConnectionError:
        print("ConnectionError, and the last file retrieved is: \n{}".format(baseurl))
        time.sleep(30)
        scrape_geojson_gdacs(baseurl, local_direc)

# ====================================================================================================== #
# Debug

if __name__ == "__main__":
    # Set the URL you want to webscrape from
    tc_url = 'https://www.gdacs.org/datareport/resources/TC/'
    local_direc = config.param["DATA_DIRECTORY"] + '/gdacs/TC/'

    # if not os.path.exists(local_direc):
    #     os.mkdir(local_direc)
    #     print("Created directory to save data!")
    # else:
    #     print("Directory {} already exists!".format(local_direc))

    starttime = time.time()
    print("Start to scrape all geojson files at:\n{}".format(tc_url))
    scrape_geojson_gdacs(tc_url, local_direc)
    endtime = time.time()
    print("Total scraping time: {}".format(endtime-starttime))
