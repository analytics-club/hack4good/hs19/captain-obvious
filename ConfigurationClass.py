import toml
import os
import time

# ====================================================================================================== #
# GLOBALS

SRC_DIR = os.path.dirname(__file__)
TIME_AT_START = time.strftime("%Y-%m-%d-T%H%M", time.gmtime())

# ====================================================================================================== #
# LOCAL CONFIGURATION

class Configuration:

    def __init__(self, config_file_dir=SRC_DIR, config_file_name="configuration_local.toml"):

        print("Current config_file_dir is: " + os.path.abspath(config_file_dir))

        #-- Load configuration parameters from .toml file.
        self.config_full_path = os.path.join(config_file_dir, config_file_name)
        with open(self.config_full_path) as f:
            txt_toml = f.read()
        self.param = toml.loads(txt_toml)

        #-- Move into home directory
        os.chdir(os.path.dirname(config_file_dir))
        self.machine_type = self.param["MACHINE_TYPE"]
        for key, name in self.param.items():
            if "DIRECTORY" in key:
                if not (":" in name or name[0] == "/"):
                    self.param[key] = SRC_DIR + "/" + self.param[key]

        #-- Sanity checks --
        self._check_paths_existence()

    # def _silence_3rd_parties_logs(self):
    #     # the following implementation works even when the "NO_LOGS_LIST" is missing from toml
    #     for module in self.param.get("NO_LOGS_LIST", []):
    #         module_logger = logging.getLogger(module)
    #         module_logger.setLevel(logging.ERROR)
    #         logger.info("Logger of '{}' silenced".format(module))

    def _check_paths_existence(self):
        for directory in ["OUT_DIRECTORY", "DATA_DIRECTORY"]:
            if not os.path.isdir(self.param[directory]):
                raise ValueError("Invalid '{}' path in configuration: {}".format(directory, self.param[directory]))