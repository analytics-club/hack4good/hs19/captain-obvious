import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.filterwarnings("ignore", "FutureWarning: Series.base is deprecated and will be removed in a future version")

import time
from tqdm import tqdm
from pandas_datareader.wb import (
    WorldBankReader,
    download,
    get_countries,
    get_indicators,
    search,
)
import world_bank_data as wb

DEBUG = False

topics_of_interest = {1:"Agriculture & Rural Development", 2: "Aid Effectiveness", 3:"Economy & Growth",
                      9:"Infrastructure", 11:"Poverty"}
cntry_codes = ["CA", "MX", "USA", "KSV"]
cntry_codes = ['AFG', 'AGO', 'ARE', 'ARG', 'ASM', 'ATG',
               'AUS', 'AUT', 'AZE', 'BDI', 'BEL', 'BEN', 'BFA', 'BGD', 'BGR',
               'BHS', 'BIH', 'BLZ', 'BOL', 'BRA', 'BRB', 'BRN', 'BTN', 'BWA',
               'CAF', 'CAN', 'CHE', 'CHL', 'CHN', 'CIV', 'CMR', 'COD',
               'COL', 'COM', 'CPV', 'CRI', 'CUB', 'CYM', 'CYP', 'CZE', 'DEU',
               'DJI', 'DMA', 'DOM', 'DZA', 'ECU', 'EGY', 'ERI', 'ESP', 'ETH',
               'FIN', 'FJI', 'FRA', 'FSM', 'GAB', 'GBR', 'GEO', 'GHA', 'GIN',
               'GMB', 'GNB', 'GRC', 'GRD', 'GRL', 'GTM', 'GUM', 'GUY', 'HKG',
               'HND', 'HRV', 'HTI', 'HUN', 'IDN', 'IND', 'IRL', 'IRN', 'IRQ',
               'ISL', 'ISR', 'ITA', 'JAM', 'JOR', 'JPN', 'KAZ', 'KEN', 'KGZ',
               'KHM', 'KIR', 'KNA', 'KOR', 'LAO', 'LBN', 'LBR', 'LBY', 'LCA',
               'LKA', 'LSO', 'LTU', 'LVA', 'MAC', 'MAF', 'MAR', 'MDA', 'MDG',
               'MDV', 'MEX', 'MHL', 'MKD', 'MLI', 'MMR', 'MNE', 'MNG', 'MNP',
               'MOZ', 'MRT', 'MUS', 'MWI', 'MYS', 'NAM', 'NCL', 'NER',
               'NGA', 'NIC', 'NOR', 'NPL', 'NZL', 'OMN', 'PAK', 'PAN', 'PER',
               'PHL', 'PLW', 'PNG', 'POL', 'PRI', 'PRK', 'PRT', 'PRY', 'PSE',
               'PYF', 'ROU', 'RUS', 'RWA', 'SAU', 'SDN', 'SEN', 'SLB', 'SLE',
               'SLV', 'SOM', 'SRB', 'SSD', 'SUR', 'SVK', 'SVN', 'SWE', 'SWZ',
               'SXM', 'SYC', 'SYR', 'TCA', 'TCD', 'TGO', 'THA', 'TJK',
               'TLS', 'TON', 'TTO', 'TUN', 'TUR', 'TUV', 'TZA', 'UGA',
               'UKR', 'URY', 'USA', 'UZB', 'VCT', 'VEN', 'VGB', 'VIR', 'VNM',
               'VUT', 'WSM', 'XKX', 'YEM', 'ZAF', 'ZMB', 'ZWE']
indicators = ["NY.GDP.PCAP.KD",  # GDP per capita (constant 2010 US$)
        "SI.POV.GINI",  # GINI index (World Bank estimate)
        "NV.AGR.TOTL.ZS",  # Agriculture, forestry, and fishing, value added (% of GDP)
        "DT.ODA.ODAT.PC.ZS",  # Net ODA (Official Development Assistance) received per capita (current US$)
        "SI.POV.dday"  # Poverty headcount ratio at $1.90 a day (2011 PPP) (% of population)
        ]

def get_all_indicators_from_wb(start=None, end=None):
    # result = search("gdp.*capita.*constant")

    # Access World Bank API
    year, month, day, hour, min = map(int, time.strftime("%Y %m %d %H %M").split())
    results = download(country = cntry_codes, indicator = indicators, start = 2000, end = year, errors = "ignore",
                       freq = 'A')

    # Clean up world bank data
    results = results.reset_index()
    results['year'] = results['year'].astype(int)
    results = results.sort_values('year')

    # Fill in missing values (interpolation) TODO help
    all_cols = [x for x in results.columns if x not in ['year', 'country']]
    # results[column_name] = results[column_name].interpolate(method='polynomial', order=2)
    # results.groupby('country')[all_cols].apply(lambda group: group.interpolate(method='polynomial', order=2, limit_direction='forward'))
    # results4 = results.copy()
    # results4.set_index('country')
    # results4.groupby('country')[all_cols+['year']].apply(lambda group: group.interpolate(limit_direction='forward'))
    # results4.reset_index()


    return results

def get_single_indicator_from_wb(country, year):
    # This function should not be used in production.
    # result = search("gdp.*capita.*constant")
    # Access World Bank API
    year, month, day, hour, min = map(int, time.strftime("%Y %m %d %H %M").split())
    results = download(country = country, indicator = indicators, start = year, end = year, errors = "ignore", freq = 'A')

    # Clean up world bank data
    results = results.reset_index()

    return results


if __name__ == "__main__":
    wb.get_indicators(topic=3)
    test = get_indicators_from_wb()

    if DEBUG:
        # Trying to find faulty country codes
        nonvalid = []
        for country_cd in tqdm(cntry_codes):
            try:
                download(
                    country=[country_cd], indicator=indicators, start=2003, end=2004, errors="ignore", freq='A'
                )
            except:
                nonvalid.append(country_cd)
        print(nonvalid)
