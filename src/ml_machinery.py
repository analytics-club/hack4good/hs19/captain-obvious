import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.filterwarnings("ignore", "FutureWarning: Series.base is deprecated and will be removed in a future version")

import numpy as np
import pandas as pd
import os
from ConfigurationClass import Configuration
import matplotlib.pyplot as plt

np.random.seed(123)
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from xgboost import XGBRegressor
from sklearn.svm import SVR
from sklearn.model_selection import RandomizedSearchCV
from sklearn.linear_model import ElasticNet, Ridge, LinearRegression
from sklearn.metrics import mean_squared_error

from joblib import dump, load


def ml_with_matrix(data: pd.DataFrame, labelname, climada_predictions, floods, recompute=False):
    """
    First, the data gets split into training and test set. On the training set we choose a best model by
    evaluating several models by a Randomized Search Cross Validation. The best model gets trained on the
    whole training set and its performance on the test set gets evaluated. Subsequently, climadas raw
    prediction gets evaluated on the test set and the performances get compared.

    :param data: the whole data
    :param labelname: the column name of the label in data
    :param climada_predictions: the climada predictions
    :param floods: whether this is the model for floods or tc
    :param recompute: if True, the validation & training pipeline gets computed even if results already exist

    Quick and dirty ML skeleton. Choose cv_folds and random_parameter_samples as you wish.
    """

    # ------------------
    # configurations
    # ------------------
    cv_folds = 10
    random_parameter_samples = 10

    scoring_method = 'neg_mean_squared_error'
    random_state = 123

    # ------------------
    # data loading, splitting
    # ------------------
    data_path = Configuration().param["DATA_DIRECTORY"]
    model_path = os.path.join(data_path, "../models")
    if floods:
        our_final_model = os.path.join(model_path, "our_final_model_Floods.joblib")
        best_of_all_path = os.path.join(model_path, "best_of_all_floods.joblib")
    else:
        our_final_model = os.path.join(model_path, "our_final_model_TC.joblib")
        best_of_all_path = os.path.join(model_path, "best_of_all_TC.joblib")


    #if not recompute and os.path.exists(our_final_model):
    #    print("resulting model already computed. :)")
    #    return

    if not os.path.isdir(model_path):
        os.makedirs(model_path)


    y = data[labelname]
    X = data.loc[:, data.columns != labelname]

    indices = np.arange(len(y))
    X_train, X_test, y_train, y_test, idx1, idxtest = train_test_split(X, y, indices, test_size=0.20, random_state=random_state)

    climada_predictions = climada_predictions.iloc[idxtest]

    if not os.path.exists(best_of_all_path):

        # TODO imputing, scaling? decide after getting the matrix & after seeing the models that we want to try.

        # ------------------
        # model selection
        # ------------------

        # you can add models and parameter grids here as you wish
        modelgrids = list()

        xgb_params = {
            'max_depth': [2, 5, 8],
            'min_child_weight': [3, 6, 10],
            'gamma': [0.8, .9, 1],
            'n_estimators': [100, 300, 400],
            'learning_rate': [0.3, .1, .01],
            'random_state': [random_state]
        }

        rf_params = {
            'n_estimators': [300, 500, 700],
            'min_samples_split': [8, 10, 12],
            'min_samples_leaf': np.arange(3, 5, 1),
            'max_features': ['auto', 'log2', 'sqrt'],
            'max_depth': [8, 11, 14],
            'bootstrap': [True, False],
            'random_state': [random_state]
        }

        svr_params = {
            'kernel': ['rbf', 'sigmoid', 'linear'],
            'gamma': ['auto', 'scale'],
            'C': [0.75, 1, 1.25]
        }

        elastic_net_params = {
            "alpha": [1,1.5,3],
            "l1_ratio": [0, 0.5, 1],
        }

        ridge_params = {
            "alpha": [1,1.5,3],
            "normalize": [True, False]
        }

        modelgrids.append(('Random Forest', RandomForestRegressor(n_jobs=-1), rf_params))
        #modelgrids.append(('SVR', SVR(), svr_params))
        modelgrids.append(('XGBoost', XGBRegressor(njobs=-1, verbosity=0), xgb_params))
        modelgrids.append(("Elastic Net", ElasticNet(max_iter=5000), elastic_net_params))
        modelgrids.append(("Ridge", Ridge(), ridge_params))
        modelgrids.append(("Linear Regression", LinearRegression(), {}))

        gridCVResults = list()
        for description, model, paramgrid in modelgrids:
            print(description)
            reg = RandomizedSearchCV(estimator=model, param_distributions=paramgrid, cv=cv_folds, verbose=1,
                                     scoring=scoring_method, return_train_score=True, n_jobs=-1,
                                     n_iter=random_parameter_samples)
            reg.fit(X_train, y_train)
            gridCVResults.append((description, reg))
            zipped = zip(reg.cv_results_['mean_test_score'], reg.cv_results_['std_test_score'], reg.cv_results_['params'])
            zipped = sorted(zipped, key=lambda x: x[0], reverse=True)  # sort by mean test score
            for mean, std, params in zipped:
                print("%0.3f (+/-%0.03f) for %r" % (mean, std * 2, params))

        best_estimators = [(desc, reg.best_estimator_, np.mean(reg.cv_results_['mean_test_score'][reg.best_index_])) for
                           desc, reg in gridCVResults]
        cv_results = [(desc, np.mean(reg.cv_results_['mean_train_score'][reg.best_index_]),
                       np.mean(reg.cv_results_['mean_test_score'][reg.best_index_])) for desc, reg in gridCVResults]
        cv_results = pd.DataFrame(cv_results, columns=['model', 'cv_train_score (negative MSE)', 'cv_val_score (negative MSE)'])
        cv_results.index = cv_results['model']
        cv_results = cv_results.drop('model', axis=1)
        print("CV validation:")
        print(cv_results)
        print("\n")

        sorted_estimators = sorted(best_estimators, key=lambda x: x[2], reverse=True)
        best_of_all = sorted_estimators[0][1]
        best_performance = sorted_estimators[0][2]

        print("best model: %s" %best_of_all)
        print(" with validation performance: %s" %str(best_performance))
        dump(best_of_all, best_of_all_path)

    best_of_all = load(best_of_all_path)
    print(best_of_all)
    # ------------------
    # generalization estimation
    # ------------------
    best_of_all.fit(X_train, y_train)
    predictions = best_of_all.predict(X_test)
    mse = mean_squared_error(y_test, predictions)
    print("\nMSE on test set after our debiasing: %s, RMSE: %s" %(mse,np.sqrt(mse)))
    mse_climada = mean_squared_error(y_test, climada_predictions)
    print("MSE of raw climada prediction: %s, RMSE: %s" %(mse_climada, np.sqrt(mse_climada)))
    mse_train = mean_squared_error(y_train, best_of_all.predict(X_train))
    print("Train MSE: %s, RMSE: %s" %(mse_train, np.sqrt(mse_train)))

    ratio = (predictions - y_test)/y_test
    abs = np.abs(ratio)
    MSE_rel = np.mean(abs)
    print("Rel Abs percent-error: %s" %MSE_rel)

    mse_improvement = mse_climada - mse
    print("MSE improvement of our models VS climada: %s" %mse_improvement)

    feature_imp_ordered_cols = np.argsort(best_of_all.feature_importances_)[::-1]
    columns = data.columns[feature_imp_ordered_cols]
    feature_importances = best_of_all.feature_importances_[feature_imp_ordered_cols]
    feature_imp_df = pd.DataFrame({"column": columns, "feature_importance": feature_importances})
    print(feature_imp_df)

    residuals = (y_test - predictions)
    squared_residuals = residuals**2
    plt.hist(squared_residuals, bins=len(squared_residuals))
    plt.xlabel("Squared residuals")
    #plt.xticks(np.linspace(0,np.max(squared_residuals), num=10).astype(np.int))
    plt.show()

    plt.hist(residuals, bins=len(residuals))
    plt.xlabel("residuals")
    plt.show()

    # plot residuals

    # ------------------
    # train best model on all our data for IDMCs future predictions
    # ------------------
    best_of_all.fit(X,y)
    dump(best_of_all, our_final_model)


def load_our_model(flood):
    """
    Loads the model with which IDMC can predict future events
    :return:
    """
    data_path = Configuration().param["DATA_DIRECTORY"]
    model_path = os.path.join(data_path, "../models")
    if flood:
        our_model_path = os.path.join(model_path, "our_final_model_Floods.joblib")
    else:
        our_model_path = os.path.join(model_path, "our_final_model_TC.joblib")

    if os.path.exists(our_model_path):
        return load(our_model_path)
    else:
        raise AssertionError("model is not computed yet.")

def select_climada_predictions(data_matrix, pred_matrix):
    return pred_matrix.loc[data_matrix.index, :]


if __name__ == "__main__":
    #X = np.random.randint(1,10, (500,4))
    #X_df = pd.DataFrame(X, columns=["a","b", "c", "d"])
    data_path = Configuration().param["DATA_DIRECTORY"]

    # TC
    save_path_tc = os.path.join(data_path, "ml_matrix_tc.csv")
    df_tc = pd.read_csv(save_path_tc, index_col=0, header=0)
    climada_predictions_tc_path = os.path.join(data_path, "climada_TC.csv")
    climada_predictions_tc = pd.read_csv(climada_predictions_tc_path, index_col=0, header = 0)
    predictions = select_climada_predictions(df_tc, climada_predictions_tc)
    ml_with_matrix(df_tc, labelname="displaced_people", climada_predictions=predictions, floods=False)

    # Floods
    """
    save_path_flood = os.path.join(data_path, "ml_matrix_flood.csv")
    df_fl = pd.read_csv(save_path_flood, index_col=0, header=0)
    climada_predictions_fl_path = os.path.join(data_path, "climada_FL.csv")
    climada_predictions_fl = pd.read_csv(climada_predictions_fl_path, index_col=0, header = 0)
    predictions_fl = select_climada_predictions(df_fl, climada_predictions_fl)
    ml_with_matrix(df_fl, labelname="displaced_people", climada_predictions=predictions_fl, floods=True)
"""

#model = load_our_model()
#print(model)

#ml_with_matrix(X_df, "d", "a", recompute=False)
