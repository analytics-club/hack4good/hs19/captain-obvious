import pandas as pd
import geopandas as gpd
from ConfigurationClass import Configuration
import os

import time
import warnings
from pipeline.identified_labels import id_labels

from src.utils.subtasks_no_climada import check_if_files_exist


def read_and_prepare_matrix(path):
    """
    adaption of read_and_prepare_matrix from subtasks_no_climada to ignore errors
    """
    polygon_matrix = gpd.read_file(path)
    if "iso3" not in polygon_matrix.columns:
        return None
    else:
        cols = polygon_matrix.columns.values
        if "eventid" in cols:
        #try:
            polygon_matrix = polygon_matrix[["eventtype", "eventid", "fromdate", "iso3","todate","polygonlabel"]]
        #except:
        #    print("no match")
        #    return None
        else:
            print("no match")
            return
        polygon_matrix.loc[:, "eventid"] = polygon_matrix.loc[0, "eventid"]
        polygon_matrix.loc[:, "fromdate"] = polygon_matrix.loc[0, "fromdate"]
        polygon_matrix.loc[:, "todate"] = polygon_matrix.loc[0, "todate"]
        polygon_matrix.loc[:, "iso3"] = polygon_matrix.loc[0, "iso3"]
        return polygon_matrix

def get_country_start_end(list_of_polygon_filenames):
    """
    For a list of polygon filenames get their country information, their starting and their ending date

    :param list_of_polygon_filenames:
    :return:
    """
    # storage structure:
    # data/exposure/exposurefilename
    # data/gdacs_TC/gdacsfilenames

    start = time.time()

    config = Configuration()
    data_path = config.param["DATA_DIRECTORY"]
    print(list_of_polygon_filenames)
    polygon_paths = [os.path.join(data_path, "gdacs_TC", x) for x in list_of_polygon_filenames]

    # check if all given files exist
    check_if_files_exist(polygon_paths)


    before_polygon_matrix_creation = time.time()
    print("check if files exist: %s seconds" %(before_polygon_matrix_creation - start))

    list_of_polygon_matrices = [read_and_prepare_matrix(path) for path in polygon_paths]
    try:
        all_polygons_matrix = pd.concat(list_of_polygon_matrices)
    except:
        print("no match")
        return ["   "], "01/Jan/1000 00:00:00","01/Jan/1000 00:00:00"

    after_polygon_matrix_creation = time.time()
    print("polygon matrix creation: %s seconds" %(after_polygon_matrix_creation - before_polygon_matrix_creation))


    usable = list(all_polygons_matrix['polygonlabel'].str.endswith(" km/h").values)
    all_polygons_matrix_with_useful_intensities = all_polygons_matrix.loc[usable, :]

    all_polygons_matrix_with_useful_intensities.loc[:, 'polygonlabel'] = \
        all_polygons_matrix_with_useful_intensities.loc[:,'polygonlabel'].apply(lambda x: int(x[:-5]))

    country_list = all_polygons_matrix_with_useful_intensities.loc[:,"iso3"].unique()
    if all_polygons_matrix_with_useful_intensities.empty:
        return ["   "], "01/Jan/1000 00:00:00","01/Jan/1000 00:00:00"
    start=all_polygons_matrix_with_useful_intensities.loc[:,"fromdate"].values[0]
    end=all_polygons_matrix_with_useful_intensities.loc[:,"todate"].values[-1]
    return country_list,start,end