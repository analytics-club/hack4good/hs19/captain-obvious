import pandas as pd
import geopandas as gpd
from ConfigurationClass import Configuration
import os

import time
import warnings


def check_if_files_exist(files_to_check):
    """
    Does the sanity check if a file exists

    :param files_to_check: path of a file
    :return: Boolean if file exists
    """
    for file in files_to_check:
        if not os.path.isfile(file):
            warnings.warn("Given file %s doesn't exist" %file)
            return False
        else:
            return True
def check_if_hazard_processed(lab,path):
    """
    Does the sanity check if a hazard, exposure match was already performed
    :param lab: gdacs eventid
    :param path: path where the hazard, exposure matches are saved
    :return: Boolean if file exists
    """
    if lab in [x.split("_")[-1].split(".")[0] for x in os.listdir(path)]:
        return True
    else:
        return False

def load_single_exposurefile(exposure_path, whole_grid_file):
    """
    Loads and sets column names for exposure files from GED and merges id with latitude longitude coordinates
    :param exposure_path: path where the exposures are stored
    :param whole_grid_file: either pt_5x5.shp as geopandas or tzun_1x1_pt.shp as geopandas (when available we use the more precise pt_5x5.shp)
    :return: geopandas dataframe with geometry set
    """
    exposure_column_names = ['id_5x', 'iso3', 'cpx', 'sector', 'se_sismo', 'bs_value_r', 'bs_value_nr', 'VALHUM',
                             'VALIS']
    exposure_df = pd.read_csv(exposure_path)
    exposure_df.columns = exposure_column_names
    exposure_df["id_5x"]=exposure_df["id_5x"].astype("int64")
    exposure_df_merged = pd.merge(exposure_df, whole_grid_file, on='id_5x',how="inner")

    crs = {'init': 'epsg:4326'}  # Creating a Geographic data frame
    gdf = gpd.GeoDataFrame(exposure_df_merged.loc[:, exposure_df_merged.columns != 'geometry'], crs=crs, geometry=exposure_df_merged.loc[:,"geometry"])
    return gdf


def read_and_prepare_matrix(path):
    """
    Reads one Gdacs Hazard file (usually per event there are multiple) to a geopandas, selects relevant information, and copies it to all relevant polygons
    :param path: file path to be read
    :return: geopandas dataframe of polygons with information on the polys
    """
    polygon_matrix = gpd.read_file(path)
    if "iso3" not in polygon_matrix.columns:
        return None
    else:
        cols = polygon_matrix.columns.values
        if "eventid" in cols:
            # try:
            polygon_matrix = polygon_matrix[["eventtype", "eventid", "fromdate", "iso3", "todate", "polygonlabel","geometry"]]
        # except:
        #    print("no match")
        #    return None
        else:
            print("no match")
            return
        polygon_matrix.loc[:, "eventid"] = polygon_matrix.loc[0, "eventid"]
        polygon_matrix.loc[:, "fromdate"] = polygon_matrix.loc[0, "fromdate"]
        polygon_matrix.loc[:, "iso3"] = polygon_matrix.loc[0, "iso3"]
        return polygon_matrix

def adapt_string(input):
    """
    Change date to be addable to path
    :param input: date string
    :return: reformated date string
    """
    res = input.replace(" ", "_")
    res = res.replace("/", "_")
    res = res.replace(":", "_")
    return res[:-9]

def get_all_files_with_prefix(prefix,gdacs_path):
    """
    Loads all files of a certain hazard id
    :param prefix: hazard id
    :return: list of all files
    """
    list_of_polygon_filenames = [x for x in os.listdir(gdacs_path) if x.startswith("geojson_"+prefix)]
    return list_of_polygon_filenames
