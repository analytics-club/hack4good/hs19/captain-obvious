import time
import os
import pandas as pd
import geopandas as gpd
import numpy as np

from src.utils.subtasks_no_climada import adapt_string,get_all_files_with_prefix,check_if_files_exist,check_if_hazard_processed,load_single_exposurefile,read_and_prepare_matrix
from ConfigurationClass import Configuration

def read_and_prepare_matrix_fl(path):
    """
    Reads one Gdacs Hazard file (usually per event there are multiple) to a geopandas, selects relevant information, and copies it to all relevant polygons
    :param path: file path to be read
    :return: geopandas dataframe of polygons with information on the polys
    """
    polygon_matrix = gpd.read_file(path)
    if "iso3" not in polygon_matrix.columns:
        return None
    else:
        polygon_matrix = polygon_matrix[
            ["id", "name", "eventtype", "fromdate", "iso3", "severity", 'Class', 'geometry']]
        polygon_matrix.loc[:, "fromdate"] = polygon_matrix.loc[0, "fromdate"]
        polygon_matrix.loc[:, "iso3"] = polygon_matrix.loc[0, "iso3"]
        polygon_matrix.rename(columns={'severity': "polygonlabel"}, inplace=True)
        return polygon_matrix

def check_if_hazard_processed_fl(lab,country,path):
    """
    Does the sanity check if a hazard, exposure match was already performed
    :param lab: gdacs eventid
    :param path: path where the hazard, exposure matches are saved
    :return: Boolean if file exists
    """
    if lab in [x.split("_")[-1].split(".")[0] for x in os.listdir(path)]:
        if country in [x.split("_")[1] for x in os.listdir(path)]:
            return True
        else:
            return False
    else:
        return False

def compute_centroid_intensities_from_gdacs_fl(country,idmc_df,path,whole_grid_file,exposure_dir,emergency_grid_file,emergency_dir,gdacs_path):
    """
    Loads an exposure file, checks which flood hazard happened there, loads the hazard data,
    checks which exposure points inside the affected area defined in the hazard file,
    returns the affected exposure points disaggregated by building type, income and intensity of the hazard

    :param lab: gdacs eventid
    :param path: output_path
    :param whole_grid_file: pt_5x5.shp loaded as geopandas
    :param exposure_dir: directory of the exposure files with id_5x
    :param emergency_grid_file:
    :param emergency_dir:tzun_1x1_pt.shp loaded and brought into same structure (for consistency) as pt_5x5.shp
    :return: no return, save file to TC_<country>_<date>_<eventid>.csv
    """

    # storage structure:
    # data/exposure/exposurefilename
    # data/gdacs_TC/gdacsfilenames



    config = Configuration()
    data_path = config.param["DATA_DIRECTORY"]#load the general file path


    print(country)
    exposure_file_name = "ged2015expo_" + country + ".csv"
    exposure_file = os.path.join(exposure_dir, exposure_file_name)  # get the exposure of a country on pt_5x5
    emergency_file = os.path.join(emergency_dir, exposure_file_name)  # get the exposure of a country on 1x1

    if check_if_files_exist([exposure_file]):
        exposures = load_single_exposurefile(exposure_file, whole_grid_file)  # if the exposure exists on 5x5
    elif check_if_files_exist([emergency_file]):
        exposures = load_single_exposurefile(emergency_file, emergency_grid_file)  # if it does not exists 1x1
    else:
        return
    red_exposure = gpd.GeoDataFrame(exposures.loc[:, ["geometry", "id_5x"]].groupby("id_5x").first(),
                                    crs={'init': 'epsg:4326'},
                                    geometry="geometry").reset_index()  # reduce exposure to have only one id per place

    after_exposure_creation = time.time()

    list_of_events=idmc_df.loc[country, "gdacs_event_id"].astype("int").astype("str")
    if type(list_of_events)==np.str_:
        list_of_events=[list_of_events]
    else:
        list_of_events=list_of_events.values


    for lab in list_of_events:
        if check_if_hazard_processed_fl(lab, country, path):
            print("Hazard id already processed")
            return
        list_of_polygon_filenames = get_all_files_with_prefix(lab,gdacs_path)  # For many events there are multiple files, collect them all

        start = time.time()

          # check if all given files exist
        print(list_of_polygon_filenames)

        polygon_paths = [os.path.join(data_path, "gdacs_FL", x) for x in list_of_polygon_filenames]
        check_if_files_exist(polygon_paths)

        before_polygon_matrix_creation = time.time()
        print("check if files exist: %s seconds" %(before_polygon_matrix_creation - start))

        list_of_polygon_matrices = [read_and_prepare_matrix_fl(path) for path in polygon_paths] #load all geojson corresponding to one event

        all_polygons_matrix = pd.concat(list_of_polygon_matrices) # bring all geopandas dataframe together in one big file

        after_polygon_matrix_creation = time.time()
        print("polygon matrix creation: %s seconds" %(after_polygon_matrix_creation - before_polygon_matrix_creation))

        usable = list(all_polygons_matrix['Class'].str.endswith("_Affected").values) #only use the rows where the polygon structure has an intensity assigned
        all_polygons_matrix_with_useful_intensities = all_polygons_matrix.loc[usable, :]

        print("reduced exposure:", red_exposure.columns)
        points_within_polygons = gpd.sjoin(red_exposure, all_polygons_matrix_with_useful_intensities, how="inner", op="within") # check if exposure in polygon
        print("points within polygons:", points_within_polygons.columns)
        print(len(red_exposure.index.unique())==len(red_exposure.index))
        after_hazard_merge = time.time()

        idx = points_within_polygons.groupby(['id_5x'])["polygonlabel"].transform(max) == points_within_polygons['polygonlabel'] # transform to select only the maximal intensity at a place

        print("size before trafo",len(points_within_polygons))
        points_within_polygons=points_within_polygons[idx].groupby("id_5x").first()     # if multiple polygons with same intensity contain a exposure location only select one
        print("size after trafo", len(points_within_polygons))
        if len(points_within_polygons)==0:
            continue
        #print(exposures.columns)
        points_within_polygons=pd.merge(exposures.loc[:,["id_5x","iso3","VALHUM","se_sismo","sector"]],points_within_polygons.drop("iso3",axis=1),left_on="id_5x",right_on="id_5x",how="inner") #merge the intensity information to original exposure dataframe
        print(len(points_within_polygons))

        print(points_within_polygons.columns)
        output_file=os.path.join(path,'NFL_{}_{}_{}.csv'.format(country,adapt_string(points_within_polygons.loc[:,"fromdate"].values[0]),lab)) #define name as TC_<country>_<date>_<eventid>.csv
        points_within_polygons.loc[:, ["VALHUM", "polygonlabel", "se_sismo","sector"]].groupby(["polygonlabel", "se_sismo","sector"]).sum().to_csv(output_file) #aggregate over intensity and label
        after_saving_time= time.time()
        print("combined saving %s seconds" %(after_saving_time-after_hazard_merge))


def compute_centroid_intensities_from_gdacs_tc(lab,path,whole_grid_file,exposure_dir,emergency_grid_file,emergency_dir,gdacs_path):
    """
    Loads a hazard file, checks which countries it affected, loads the exposure data,
    checks which exposure points inside the affected area defined in the hazard file,
    returns the affected exposure points disaggregated by building type, income and intensity of the hazard

    :param lab: gdacs eventid
    :param path: output_path
    :param whole_grid_file: pt_5x5.shp loaded as geopandas
    :param exposure_dir: directory of the exposure files with id_5x
    :param emergency_grid_file:
    :param emergency_dir:tzun_1x1_pt.shp loaded and brought into same structure (for consistency) as pt_5x5.shp
    :return: no return, save file to TC_<country>_<date>_<eventid>.csv
    """

    # storage structure:
    # data/exposure/exposurefilename
    # data/gdacs_TC/gdacsfilenames



    config = Configuration()
    data_path = config.param["DATA_DIRECTORY"]#load the general file path

    list_of_polygon_filenames=get_all_files_with_prefix(lab,gdacs_path) #For many events there are multiple files, collect them all

    start = time.time()
    print(list_of_polygon_filenames)

    polygon_paths = [os.path.join(data_path, "gdacs_TC", x) for x in list_of_polygon_filenames]

    check_if_files_exist(polygon_paths)# check if all given files exist


    before_polygon_matrix_creation = time.time()
    print("check if files exist: %s seconds" %(before_polygon_matrix_creation - start))

    list_of_polygon_matrices = [read_and_prepare_matrix(path) for path in polygon_paths] #load all geojson corresponding to one event

    all_polygons_matrix = pd.concat(list_of_polygon_matrices) # bring all geopandas dataframe together in one big file

    after_polygon_matrix_creation = time.time()
    print("polygon matrix creation: %s seconds" %(after_polygon_matrix_creation - before_polygon_matrix_creation))

    usable = list(all_polygons_matrix['polygonlabel'].str.endswith(" km/h").values) #only use the rows where the polygon structure has an intensity assigned
    all_polygons_matrix_with_useful_intensities = all_polygons_matrix.loc[usable, :]

    all_polygons_matrix_with_useful_intensities.loc[:, 'polygonlabel'] = \
        all_polygons_matrix_with_useful_intensities.loc[:,'polygonlabel'].apply(lambda x: int(x[:-5])) # reformat the intensity to integers

    country_list = all_polygons_matrix_with_useful_intensities.loc[:,"iso3"].unique() # select which countries are affected

    print(country_list)


    for country in country_list:
        if check_if_hazard_processed_fl(lab, path, country):
            print("Hazard id already processed")
            continue
        after_intensity_filtering = time.time()
        print("intensity filtering: %s seconds" % (after_intensity_filtering - after_polygon_matrix_creation))
        print(country)
        exposure_file_name = "ged2015expo_"+country+".csv"
        exposure_file = os.path.join(exposure_dir, exposure_file_name) #get the exposure of a country on pt_5x5
        emergency_file = os.path.join(emergency_dir, exposure_file_name) #get the exposure of a country on 1x1

        if check_if_files_exist([exposure_file]):
            exposures=load_single_exposurefile(exposure_file,whole_grid_file) #if the exposure exists on 5x5
        elif check_if_files_exist([emergency_file]):
            exposures=load_single_exposurefile(emergency_file,emergency_grid_file) #if it does not exists 1x1
        else:
            continue
        after_exposure_creation = time.time()
        print("exposure creation: %s seconds" %(after_exposure_creation - after_intensity_filtering))

        red_exposure=gpd.GeoDataFrame(exposures.loc[:,["geometry","id_5x"]].groupby("id_5x").first(),crs={'init': 'epsg:4326'},geometry="geometry").reset_index() #reduce exposure to have only one id per place
        print("reduced exposure:", red_exposure.columns)
        points_within_polygons = gpd.sjoin(red_exposure, all_polygons_matrix_with_useful_intensities, how="inner", op="within") # check if exposure in polygon
        print("points within polygons:", points_within_polygons.columns)
        print(len(red_exposure.index.unique())==len(red_exposure.index))
        after_hazard_merge = time.time()
        print("exposure hazard matching %s seconds" %( after_hazard_merge-after_intensity_filtering))

        idx = points_within_polygons.groupby(['id_5x'])["polygonlabel"].transform(max) == points_within_polygons['polygonlabel'] # transform to select only the maximal intensity at a place

        print("size before trafo",len(points_within_polygons))
        points_within_polygons=points_within_polygons[idx].groupby("id_5x").first()     # if multiple polygons with same intensity contain a exposure location only select one
        print("size after trafo", len(points_within_polygons))
        if len(points_within_polygons)==0:
            continue
        #print(exposures.columns)
        points_within_polygons=pd.merge(exposures.loc[:,["id_5x","iso3","VALHUM","se_sismo","sector"]],points_within_polygons.drop("iso3",axis=1),left_on="id_5x",right_on="id_5x",how="inner") #merge the intensity information to original exposure dataframe
        print(len(points_within_polygons))

        print(points_within_polygons.columns)
        #TODO: Remove N
        output_file=os.path.join(path,'NTC_{}_{}_{}.csv'.format(points_within_polygons.loc[:,"iso3"].values[0],adapt_string(points_within_polygons.loc[:,"fromdate"].values[0]),points_within_polygons.loc[:,"eventid"].values[0])) #define name as TC_<country>_<date>_<eventid>.csv
        points_within_polygons.loc[:, ["VALHUM", "polygonlabel", "se_sismo","sector"]].groupby(["polygonlabel", "se_sismo","sector"]).sum().to_csv(output_file) #aggregate over intensity and label
        after_saving_time= time.time()
        print("combined saving %s seconds" %(after_saving_time-after_hazard_merge))

