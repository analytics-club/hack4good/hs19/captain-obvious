import pandas as pd
import geopandas as gpd
import os
import sys
from tqdm import tqdm
from climada.entity import Exposures
import time
import math
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/../..")) # sys.path.append(os.path.abspath(os.getcwd() + "/../.."))
# from data.additional_features_for_ml.CountryData import CountryData
from ConfigurationClass import Configuration
#import src.api_world_bank as api_wb
import warnings

config = Configuration()

def read_in_idmc_disaster():
    path_data = config.param["DATA_DIRECTORY"]
    local_filename_displ_event = path_data + "idmc/idmc_disaster_all_dataset.xlsx"
    if local_filename_displ_event.split(".")[-1] == "csv":
        idmc_df = pd.read_csv(local_filename_displ_event, header=[1], encoding = "ISO-8859-1")
    elif local_filename_displ_event.split(".")[-1] == "xlsx":
        # idmc_df = pd.read_excel(local_filename_displ_event).loc[1:, :]  # CHANGE ME!!!
        idmc_df = pd.read_excel(local_filename_displ_event, header=[1])
    idmc_df.rename(columns={'#country+code': "iso3",
                            '#country+name': "country"}, inplace=True)
    return idmc_df

def check_if_files_exist(files_to_check):
    for file in files_to_check:
        if not os.path.isfile(file):
            warnings.warn("Given file %s doesn't exist" %file)
            return False
        else:
            return True

def save_single_exposurefile(exposure_path, whole_grid_file, output_path):
    if os.path.exists(output_path):
        print("file %s was already converted, will skip its computation." %output_path)
        return
    exposure_column_names = ['id_5x', 'iso3', 'cpx', 'sector', 'se_sismo', 'bs_value_r', 'bs_value_nr', 'VALHUM',
                             'VALIS']
    exposure_df = pd.read_csv(exposure_path)
    exposure_df.columns = exposure_column_names
    exposure_df["id_5x"]=exposure_df["id_5x"].astype("int64")
    exposure_df_merged = pd.merge(exposure_df, whole_grid_file, on='id_5x')
    exp = Exposures(exposure_df_merged)
    exp['value'] = exposure_df_merged['VALHUM']
    exp.value_unit = "Population"
    exp.set_lat_lon()
    exp.check()
    exp.write_hdf5(output_path)


def add_lat_lon_to_exposures(directory_of_exposures_to_convert, whole_grid_file_path, output_dir):
    # load whole grid file
    #big_grid_file = gpd.GeoDataFrame(gpd.read_file(whole_grid_file_path))

    if not os.path.isdir(directory_of_exposures_to_convert):
        raise AssertionError("Directory of exposures %s doesn't exist" %directory_of_exposures_to_convert)

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    onlyfiles = [f for f in os.listdir(directory_of_exposures_to_convert) if os.path.isfile(os.path.join(directory_of_exposures_to_convert, f))]
    if len(onlyfiles) == 0:
        raise AssertionError("No files in exposure directory %s" %directory_of_exposures_to_convert)

    for exposure_file in tqdm(onlyfiles):
        save_single_exposurefile(os.path.join(directory_of_exposures_to_convert, exposure_file),
                                 whole_grid_file_path,
                                 os.path.join(output_dir, exposure_file))


def read_and_prepare_matrix(path):
    polygon_matrix = gpd.read_file(path)
    if "iso3" not in polygon_matrix.columns:
        return None
    else:
        polygon_matrix = polygon_matrix[
            ["id", "name", "eventtype", "eventid", "fromdate", "todate", "iso3", "polygonlabel", 'Class', 'geometry']]
        polygon_matrix.loc[:, "eventid"] = polygon_matrix.loc[0, "eventid"]
        polygon_matrix.loc[:, "fromdate"] = polygon_matrix.loc[0, "fromdate"]
        polygon_matrix.loc[:, "todate"] = polygon_matrix.loc[0, "todate"]
        polygon_matrix.loc[:, "iso3"] = polygon_matrix.loc[0, "iso3"]
        return polygon_matrix


def compute_centroid_intensities_from_gdacs(list_of_polygon_filenames,path):
    # storage structure:
    # data/exposure/exposurefilename
    # data/gdacs/gdacsfilenames

    start = time.time()

    data_path = config.param["DATA_DIRECTORY"]

    polygon_paths = [os.path.join(data_path, "gdacs_TC", x) for x in list_of_polygon_filenames]

    # check if all given files exist
    check_if_files_exist(polygon_paths)

    before_polygon_matrix_creation = time.time()
    print("check if files exist: %s seconds" %(before_polygon_matrix_creation - start))

    list_of_polygon_matrices = [read_and_prepare_matrix(path) for path in polygon_paths]

    all_polygons_matrix = pd.concat(list_of_polygon_matrices)

    after_polygon_matrix_creation = time.time()
    print("polygon matrix creation: %s seconds" %(after_polygon_matrix_creation - before_polygon_matrix_creation))


    usable = list(all_polygons_matrix['polygonlabel'].str.endswith(" km/h").values)
    all_polygons_matrix_with_useful_intensities = all_polygons_matrix.loc[usable, :]

    all_polygons_matrix_with_useful_intensities.loc[:, 'polygonlabel'] = \
        all_polygons_matrix_with_useful_intensities.loc[:,'polygonlabel'].apply(lambda x: int(x[:-5]))
    country_list = all_polygons_matrix_with_useful_intensities.loc[:,"iso3"].unique()
    after_intensity_filtering = time.time()
    print("intensity filtering: %s seconds" %(after_intensity_filtering - after_polygon_matrix_creation))

    for country in country_list:
        if country=="AUS":
            continue
        if country=="CHN":
            continue
        if country=="IND":
            continue
        exposures = Exposures()
        exposure_file_name="ged2015expo_"+country+".h5"
        exposure_path = os.path.join(data_path, "geomatched_exposure", exposure_file_name)
        if check_if_files_exist([exposure_path]):
            exposures.read_hdf5(exposure_path)

            after_exposure_creation = time.time()
            print("exposure creation: %s seconds" %(after_exposure_creation - after_intensity_filtering))


            points_within_polygons = gpd.sjoin(exposures, all_polygons_matrix_with_useful_intensities, how="inner", op="within") # each point is now in several polies

            after_join = time.time()
            print("Join: %s seconds" %(after_join - after_exposure_creation))

            grouped = points_within_polygons.groupby("id_5x")

            all = []
            for key, item in grouped:
                group = grouped.get_group(key)
                id = group.iloc[0,points_within_polygons.columns.get_loc('id_5x')]
                iso3_left = group.iloc[0,points_within_polygons.columns.get_loc('iso3_left')]
                latitude = group.iloc[0,points_within_polygons.columns.get_loc('latitude')]
                longitude = group.iloc[0,points_within_polygons.columns.get_loc('longitude')]
                fromdate = group.iloc[0,points_within_polygons.columns.get_loc('fromdate')]
                intensity = group['polygonlabel'].values.max()
                row = [id, iso3_left, latitude, longitude, fromdate, intensity]
                all.append(row)

            after_group_and_adaption = time.time()
            print("Grouping and adapting: %s seconds" %(after_group_and_adaption - after_join))

            result_matrix = pd.DataFrame(all, columns=['id_5x', 'iso3', 'latitude', 'longitude', 'fromdate', 'intensity'])
            result_matrix.loc[:,'fromdate'] = result_matrix.loc[:, 'fromdate'].apply(adapt_string)
            print("HAAAAALOOOO"+path)
            save_hazard_xlsx(result_matrix,path)
        else:
            continue
    end = time.time()

    total_time = end - start
    print("total time: %s seconds" %(total_time))


def adapt_string(input):
    res = input.replace(" ", "_")
    res = res.replace("/", "_")
    res = res.replace(":", "_")
    return res[:-9]

def save_hazard_xlsx(haz_gpd,path):
    out_dir=os.path.join(path,'TC_{}_{}.xlsx'.format(haz_gpd.loc[:,"iso3"].values[0],haz_gpd.loc[:,"fromdate"].values[0]))
    writer = pd.ExcelWriter(out_dir, engine='xlsxwriter')

    # Write each dataframe to a different worksheet.
    haz_gpd.loc[:,["latitude","longitude"]].to_excel(writer, sheet_name='centroids')
    haz_gpd.loc[:,["intensity"]].to_excel(writer, sheet_name='hazard_intensity')
    pd.DataFrame(index=["event_id", "event_name", "frequency", "orig_event_flag", "event_date"], columns=[1],
                 data=[1, "event001", 1, 1, "11052016"]).transpose().to_excel(writer, sheet_name='hazard_frequency')

    # Close the Pandas Excel writer and output the Excel file.
    writer.save()


def get_all_files_till_n(prefix, suffix, n):
    return [prefix + str(x) + suffix for x in range(1,n+1)]

def get_all_files_with_prefix(prefix):
    data_path = Configuration().param["DATA_DIRECTORY"]
    gdacs_path = os.path.join(data_path, "gdacs_TC")

    list_of_polygon_filenames = [x for x in os.listdir(gdacs_path) if x.startswith(prefix)]
    return list_of_polygon_filenames


def get_matched_matrices(save_path=None, labelfile_path=None, predictionfile_path=None):
    data_path = Configuration().param["DATA_DIRECTORY"]

    if not save_path:
        save_path = os.path.join(data_path, "adapted_matrix_out.csv")

    if not labelfile_path:
        labelfile_path = os.path.join(data_path, "idmc_TC_match.csv")
    #labelfile_path = os.path.join(data_path, "gdacs_idmc_merged.csv")

    if not predictionfile_path:
        predictionfile_path = os.path.join(data_path, "current_out.csv")

    # countryData = CountryData()

    if not os.path.exists(save_path):
        label_df = pd.read_csv(labelfile_path, header=0)
        predictions_df = pd.read_csv(predictionfile_path, header=0, index_col=0)

        columns = list(predictions_df.columns.values) + ["displaced_people"]

        index_list = []
        resultlist = []

        add_3rd_party_features = True

        for i in range(label_df.shape[0]):
            eventid = label_df.loc[label_df.index[i], "eventid"]
            if math.isnan(eventid):
                continue
            else:
                index = str(int(eventid)) + "_" + label_df.loc[label_df.index[i], "iso3"]
            try:
                row = list(predictions_df.loc[index, :].values)
                row.append(label_df.loc[label_df.index[i], "displacement"])
                if add_3rd_party_features:
                    country = label_df.loc[label_df.index[i], "country"]
                    year = label_df.loc[label_df.index[i], "year"]
                    row.append(country)
                    row.append(year)
                # row = row + countryData.get_attribute_for_country(country=country, year=year)
                index_list.append(index)
                resultlist.append(row)
            except:
                print("Index %s did not match." %index)
        if add_3rd_party_features:
            columns = columns + ['country', 'year']
        matrix = pd.DataFrame(resultlist, index=index_list, columns=columns)

        if add_3rd_party_features:
            # matrix.merge(api_wb.get_all_indicators_from_wb(), on=['country', 'year'], how='left')
            matrix = pd.merge(matrix, api_wb.get_all_indicators_from_wb(),
                              how='left', left_on=['country','year'], right_on = ['country','year'])
            # columns = columns + api_wb.indicators

        matrix.drop(columns=['country', 'year'])

        matrix_rows = matrix.shape[0]
        non_matched = label_df.shape[0] - matrix_rows
        print("%s label rows did not match" %non_matched)

        matrix.to_csv(save_path, index=True)

    matrix = pd.read_csv(save_path, index_col=0, header=0)
    return matrix

#config = Configuration()
#data_path = config.param["DATA_DIRECTORY"]
#exposure_dir = os.path.join(data_path, "exposures_to_get_converted")
#output_dir = os.path.join(data_path, "exposures_converted")
#big_grid_file_path = os.path.join(data_path, "exposure", "pt_5x5.shp")

#big_grid_file = gpd.GeoDataFrame(gpd.read_file(big_grid_file_path))
#add_lat_lon_to_exposures(exposure_dir, big_grid_file, output_dir)

if __name__ == "__main__":
    data_path = Configuration().param["DATA_DIRECTORY"]
    save_path_flood = os.path.join(data_path, "ml_matrix_flood.csv")
    labelfile_path_flood = os.path.join(data_path, "idmc_fl_match.csv")
    predictionfile_path_flood = os.path.join(data_path, "final_out_FL.csv")

    #save_path_tc = os.path.join(data_path, "ml_matrix_tc.csv")
    #labelfile_path_tc = os.path.join(data_path, "idmc_TC_match.csv")
    #predictionfile_path_tc = os.path.join(data_path, "final_out_TC.csv")

    flood = get_matched_matrices(save_path=save_path_flood, labelfile_path=labelfile_path_flood, predictionfile_path=predictionfile_path_flood)
    print(flood)

    #tc = get_matched_matrices(save_path=save_path_tc, labelfile_path=labelfile_path_tc,
    #                             predictionfile_path=predictionfile_path_tc)
    #print(tc)

