# pip install sparqlwrapper
# https://rdflib.github.io/sparqlwrapper/

from SPARQLWrapper import SPARQLWrapper, JSON
import numpy as np
import pandas as pd
import operator

def create_wikidata_city_query(population='100000', language=None, limit=None):
    if limit is not None or not "":
        limit = "LIMIT {}".format(limit)
    else:
        limit = ""
    if language == 'local' or language == None:
        language = '?langCode'
    else:
        language = "'{}'".format(language)
    query_local = """
    SELECT ?city ?cityLabel ?country ?countryLabel ?lang ?langLabel ?langCode ?population
    WHERE
    {{
      ?city wdt:P1082 ?population .
      FILTER(?population>{})
      ?city wdt:P31 wd:Q515;
            wdt:P17 ?country;
            rdfs:label ?cityLabel .
      ?country wdt:P37 ?lang;
               rdfs:label ?countryLabel .
      ?lang wdt:P424 ?langCode;
            rdfs:label ?langLabel .
      FILTER(lang(?cityLabel)={})
      FILTER(lang(?countryLabel)={})
      FILTER(lang(?langLabel)={})
    }} {}""".format(population,language,language,language,limit)
    return(query_local)

def get_wikidata(endpoint_url, query):
    sparql = SPARQLWrapper(endpoint_url)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    df_result = pd.DataFrame.from_dict(results["results"]["bindings"])
    df_result = df_result.applymap(lambda x: x.get('value', np.nan) \
        if isinstance(x, dict) else np.nan)
    return df_result

def get_wikidata_city(population='100000', language=None, limit=100): # TODO create function name parser
    query = create_wikidata_city_query(population, language, limit)
    endpoint_url = "https://query.wikidata.org/sparql"
    df_result = get_wikidata(endpoint_url, query)
    return df_result

if __name__ == "__main__":
    result = get_wikidata_city()
    print('a')