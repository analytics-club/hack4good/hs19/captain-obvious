# from src.data_extraction import load_data, save_data, merging, xml2df
# from src.preprocessing import text_process, anonymization, clean_up, detect_language
import sys, os
import time
import pandas as pd

# Local imports
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))
from api_request.hazard_api import create_index_file
from api_request.scrape_geojson_gdacs import scrape_geojson_gdacs
import src.utils.subtasks as utils
import api_request.idmc_gdacs_query_2 as gdacs_api
from ConfigurationClass import Configuration
config = Configuration()
path_data = config.param["DATA_DIRECTORY"]

DEBUG = True

COLD_START = True
EVENT_TYPES = ["Storm", "Flood", "Earthquake"]
GDACS_EVENT_ID_LIMIT = 1000
idmc_gdacs_event_dict = { "Flood": ['FL'], 'Storm': ['TC'],
                           'Earthquake': ['EQ'],
                           'Volcanic eruption' : ['VO'],
                           'Drought': ['DR'],
                           'Wet Mass Movement': ['FL', 'TC'],
                            'Mass Movement': ['FL', 'TC', 'VO', 'EQ'],
                            'Dry mass movement': ['FL', 'TC', 'VO', 'EQ']
                            }

def extract_all_geojson_gdacs(force = False):
    """
    """
    print('Starting data extraction ...')
    for hazard_type in ['TC', 'EQ', 'FL']:
        starttime = time.time()
        folder_hazard = os.path.join(path_data, hazard_type)
        if os.path.exists(folder_hazard):
            if force == True:
                print('Scraping GDACS data for {}, whose folder already exists at {}'.format(hazard_type, folder_hazard))
                scrape_geojson_gdacs(hazard_type)
            else:
                print('Ignoring {}, whose folder already exists at {}'.format(hazard_type, folder_hazard))
        else:
            print('Scraping GDACS data for {} and saving at {}'.format(hazard_type, folder_hazard))
            scrape_geojson_gdacs(hazard_type)
        endtime = time.time()
        print("Total scraping time for {}: {}".format(hazard_type, endtime - starttime))
    return True

if __name__ == "__main__":
    """
    This main function is the function that is to be run for the training and creation of the displacement model.
    It is an image of the main functions in the unique definition files.
    """
    # Read in IDMC displacement data, WHICH YOU HAVE TO HAVE LOCALLY (ask IDMC for access).
    idmc_df = utils.read_in_idmc_disaster()

    # Initialize current list of available files
    idx_files = os.listdir(config.param["DATA_DIRECTORY"] + '/gdacs/')

    # Create models by event-type.
    for idmc_event_type in EVENT_TYPES:
        # Create index file for the corresponding event descriptions, available thanks the GDACS.
        gdacs_event_type = idmc_gdacs_event_dict[idmc_event_type]
        if len(gdacs_event_type)>1:
            print("Error, we have not yet implemented the case where several GDACS flags correspond to one type of IDMC event")
            continue
        gdacs_event_type = gdacs_event_type[0]
        name_idx_file = "GDACS_index_{}.csv".format(gdacs_event_type.lower())
        if COLD_START:
            if name_idx_file in idx_files:
                print('{} already exists, we move on to next event type.'.format(name_idx_file))
            else:
                create_index_file(base_url='https://www.gdacs.org/datareport/resources/{}'.format(gdacs_event_type),
                          outputfilename=name_idx_file)

        # Read-in index from created file
        gdacs_index_df = pd.read_csv(path_data + '/gdacs/' + name_idx_file)
        gdacs_index_df = gdacs_api.enrich_gdacs_idx_with_event_type(gdacs_index_df)

        # Extract GDACS event description data from the web.
        idx_input = idmc_df.loc[idmc_df['#crisis+type'] == idmc_event_type].index
        if DEBUG:
            id_limit = GDACS_EVENT_ID_LIMIT
        else:
            id_limit = -1
        if COLD_START:
            event_dict = {idx: gdacs_api.find_relevant_gdacs_recursive(idx,
                                                          gdacs_df = gdacs_index_df,
                                                          idmc_df = idmc_df)
                            for idx in idx_input[:id_limit]}

        # Collect world bank data for relevant countries


        # Combine all data
        finished = False
        finished = extract_all_geojson_gdacs()
        if finished:
            print("All data has been extracted")

        # if not os.path.exists(local_direc):
        #     os.mkdir(local_direc)
        #     print("Created directory to save data!")
        # else:
        #     print("Directory {} already exists!".format(local_direc))

        starttime = time.time()
        print("Start to scrape all geojson files at:\n{}".format(tc_url))
        scrape_geojson_gdacs(tc_url, local_direc)
        endtime = time.time()
        print("Total scraping time: {}".format(endtime-starttime))
