from src.utils.compute_centroid_intensities_from_gdacs import compute_centroid_intensities_from_gdacs_fl
import os
from ConfigurationClass import Configuration
import geopandas as gpd
import pandas as pd

from pipeline.generate_feature_mat import generate_event_features

config=Configuration()
data_path = config.param["DATA_DIRECTORY"]
exposure_dir=os.path.join(data_path,"exposures_to_get_converted")
emergency_dir=os.path.join(data_path,"exposure_to_match")
out_path= os.path.join(data_path,"csv_FL")
gdacs_path = os.path.join(data_path, "gdacs_FL")


whole_grid_file_path = os.path.join(data_path, "exposure", "pt_5x5.shp")
whole_grid_file = gpd.GeoDataFrame(gpd.read_file(whole_grid_file_path))
emergency_grid_file_path= os.path.join(data_path, "exposure", "tzun_1x1_pt.shp")
emergency_grid_file = gpd.GeoDataFrame(gpd.read_file(emergency_grid_file_path))
emergency_grid_file.drop(["MAX","BU_CPX"],inplace=True,axis=1)
emergency_grid_file.columns=["id_5x","geometry"]

idmc_df=pd.read_csv(data_path+"additional_displacement_fl.csv")
idmc_df.drop("iso3",inplace=True,axis=1)
idmc_df.rename(columns = {'my_iso': "iso3",
                          "event_id": "gdacs_event_id"}, inplace=True)
idmc_df.set_index("iso3")
country_list=idmc_df.loc[:,"iso3"].unique()[1:]
idmc_df.set_index("iso3",inplace=True)
#PRY has some issue

for country in country_list:
    compute_centroid_intensities_from_gdacs_fl(country,idmc_df,out_path,whole_grid_file,exposure_dir,emergency_grid_file,emergency_dir,gdacs_path)

input_dir= os.path.join(data_path,"csv_FL")
feature_matrix=generate_event_features(input_dir)

feature_matrix=feature_matrix.transpose()

feature_matrix.reset_index(inplace=True)
feature_matrix["intensity_interval"]=pd.qcut(feature_matrix['polygonlabel'], q=4)
feature_matrix.drop("polygonlabel",inplace=True,axis=1)
feature_matrix.loc[:,"intensity_interval"]=(feature_matrix["intensity_interval"].astype("str")).replace({ "(-0.001, 4.66]":1,"(4.66, 5.51]":4,"(5.51, 6.19]":2,"(6.19, 8.57]":3})
feature_matrix.rename(columns={"intensity_interval":"polygonlabel"},inplace=True)

### too many features by building type
feature_matrix=feature_matrix.groupby(["polygonlabel","sector"]).sum()
feature_matrix=feature_matrix.transpose()


feature_matrix.to_csv(os.path.join(data_path,"final_out_FL_2.csv"))

