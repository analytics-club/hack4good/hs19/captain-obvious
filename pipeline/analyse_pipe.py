from src.utils.compute_centroid_intensities_from_gdacs import compute_centroid_intensities_from_gdacs_tc
import os
from ConfigurationClass import Configuration
from pipeline.identified_labels import id_labels
import geopandas as gpd
import pandas as pd
from pipeline.generate_feature_mat import generate_event_features, adapt_colname

config=Configuration()
data_path = config.param["DATA_DIRECTORY"]
exposure_dir=os.path.join(data_path,"exposures_to_get_converted")
emergency_dir=os.path.join(data_path,"exposure_to_match")
out_path= os.path.join(data_path,"csv_TC")
gdacs_path = os.path.join(data_path, "gdacs_TC")

whole_grid_file_path = os.path.join(data_path, "exposure", "pt_5x5.shp")
whole_grid_file = gpd.GeoDataFrame(gpd.read_file(whole_grid_file_path))
emergency_grid_file_path= os.path.join(data_path, "exposure", "tzun_1x1_pt.shp")
emergency_grid_file = gpd.GeoDataFrame(gpd.read_file(emergency_grid_file_path))
emergency_grid_file.drop(["MAX","BU_CPX"],inplace=True,axis=1)
emergency_grid_file.columns=["id_5x","geometry"]

#idmc_df=pd.read_csv(os.path.join(data_path,"idmc_TC_match.csv"))
#id_labels=idmc_df.loc[idmc_df["eventid"].notna()]["eventid"].astype("int64").astype("str").unique()

id_labels=pd.read_csv(data_path+"idmc_gdacs_tc_dirty.csv")
id_labels.dropna(inplace=True)
id_labels=id_labels["gdacs_event_id"].astype("int64").astype("str").values
id_labels=set(id_labels)

for lab in id_labels:
    compute_centroid_intensities_from_gdacs_tc(lab,out_path,whole_grid_file,exposure_dir,emergency_grid_file,emergency_dir,gdacs_path)



input_dir= os.path.join(data_path,"csv_TC")
feature_matrix=generate_event_features(input_dir)
feature_matrix=feature_matrix.transpose()
feature_matrix.reset_index(inplace=True)
feature_matrix=feature_matrix.groupby(["polygonlabel","sector"]).sum()
feature_matrix=feature_matrix.transpose()
#feature_matrix = adapt_colname(feature_matrix) # merge first row in colname and dropit
feature_matrix.to_csv(os.path.join(data_path,"final_out_TC.csv"))


climada_pred=feature_matrix.transpose().reset_index()
climada_pred=climada_pred.groupby("polygonlabel").sum()
import numpy as np
climada_pred=pd.DataFrame(climada_pred.transpose().dot(np.array([0,0,0.067534])),columns=["climada_pred"])
climada_pred.to_csv(os.path.join(data_path,"climada_TC.csv"))
