from src.utils.subtasks import get_all_files_with_prefix, compute_centroid_intensities_from_gdacs, save_hazard_xlsx
import os
from ConfigurationClass import Configuration
from tutorial.get_prediction import getPrediction
from pipeline.identified_labels import id_labels
from joblib import delayed, Parallel

config=Configuration()
out_path=os.path.join(config.param["DATA_DIRECTORY"],"xlsx_TC")

def compute_centroid(out_path,lab):
    compute_centroid_intensities_from_gdacs(get_all_files_with_prefix("geojson_"+lab+"_"),out_path)

def get_all_files_in_dir():
    data_path = Configuration().param["DATA_DIRECTORY"]
    TC_path = os.path.join(data_path, "xlsx_TC")
    list_of_hazard_filenames = [x for x in os.listdir(TC_path)]
    return list_of_hazard_filenames


def compute_pred(hazard_file,exposure_path):
    exposure_file=os.path.join(exposure_path,"ged_2015expo_"+hazard_file+".h5")
    pred = getPrediction(hazard_file, 'TC', exposure_file, 'Asia')
    pred.run()


Parallel(n_jobs=-1)(delayed(compute_centroid)(out_path,lab) for lab in id_labels)

# TODO: had to change sanity check in CLIMADA for read_execel shape of intensity /centroid check
#
# hazard_names = get_all_files_in_dir()
# # hazard_file = os.path.join(config.param["DATA_DIRECTORY"],"xlsx_TC","TC_IND_29_Oct_2019.xlsx")
# exposure_path = os.path.join(config.param["DATA_DIRECTORY"],"geomatched_exposure")
#
# Parallel(n_jobs=-1)(delayed(compute_pred)(haz_name,exposure_path) for haz_name in hazard_names)

import pandas as pd
ex=pd.DataFrame([["BGD","IND","JPN"],[2019,2018,2013],[1244450,250000,2323222]]).transpose()
ex.columns=["iso3","year","pred"]
ex.to_csv("D:/ex.csv")