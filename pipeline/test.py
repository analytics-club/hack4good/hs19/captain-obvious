from src.utils.subtasks_no_climada import *
from src.utils.compute_centroid_intensities_from_gdacs import compute_centroid_intensities_from_gdacs_fl
import os
from ConfigurationClass import Configuration
import geopandas as gpd
import pandas as pd
from shapely.ops import unary_union

from pipeline.generate_feature_mat import generate_event_features


def read_and_prepare_matrix_upd(path):
    """
    Reads one Gdacs Hazard file (usually per event there are multiple) to a geopandas, selects relevant information, and copies it to all relevant polygons
    :param path: file path to be read
    :return: geopandas dataframe of polygons with information on the polys
    """
    polygon_matrix = gpd.read_file(path)
    if "iso3" not in polygon_matrix.columns:
        return None
    else:
        return polygon_matrix[["geometry","polygonlabel"]]

config=Configuration()
data_path = config.param["DATA_DIRECTORY"]
exposure_dir=os.path.join(data_path,"exposures_to_get_converted")
emergency_dir=os.path.join(data_path,"exposure_to_match")
out_path= os.path.join(data_path,"csv_TC")
gdacs_path = os.path.join(data_path, "gdacs_TC")

lab="40084"
list_of_polygon_filenames =get_all_files_with_prefix(lab,gdacs_path)  # For many events there are multiple files, collect them all

start = time.time()
print(list_of_polygon_filenames)

polygon_paths = [os.path.join(data_path, "gdacs_TC", x) for x in list_of_polygon_filenames]

check_if_files_exist(polygon_paths )# check if all given files exist


before_polygon_matrix_creation = time.time()
print("check if files exist: %s seconds" %(before_polygon_matrix_creation - start))

list_of_polygon_matrices = [read_and_prepare_matrix_upd(path) for path in polygon_paths]  # load all geojs

all_polygons_matrix = pd.concat(list_of_polygon_matrices)  # bring all geopandas dataframe together in one big file

after_polygon_matrix_creation = time.time()
print("polygon matrix creation: %s seconds" % (after_polygon_matrix_creation - before_polygon_matrix_creation))

usable = list(all_polygons_matrix['polygonlabel'].str.endswith(
    " km/h").values)  # only use the rows where the polygon structure has an intensity assigned
all_polygons_matrix_with_useful_intensities = all_polygons_matrix.loc[usable, :]

all_polygons_matrix_with_useful_intensities.loc[:, 'polygonlabel'] = \
    all_polygons_matrix_with_useful_intensities.loc[:, 'polygonlabel'].apply(lambda x: int(x[:-5]))

all_polygons_matrix_with_useful_intensities=all_polygons_matrix_with_useful_intensities.dissolve(by="polygonlabel")
#tmp.to_file(data_path+"test.geojson", driver='GeoJSON')



