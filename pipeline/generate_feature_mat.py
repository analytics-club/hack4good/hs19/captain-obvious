import os
from ConfigurationClass import Configuration
import pandas as pd
config=Configuration()
data_path = config.param["DATA_DIRECTORY"]

input_dir= os.path.join(data_path,"csv_TC")

# wind_int=[60,90,120,150,180,210]
# displ_fact=[0.005,0.002,0.009,0.0242,0.25,0.8]
#
# impact_func=pd.DataFrame(displ_fact,index=wind_int,columns=["impact"])
# impact_func.index.name="polygonlabel"
def generate_event_features(input_dir):
    matched_files = os.listdir(input_dir)
    file_name=matched_files[0]
    pred_col = pd.read_csv(os.path.join(input_dir, file_name))
    pred_col.columns = ["polygonlabel", "se_sismo","sector", file_name[:-4].split("_")[-1]+"_"+file_name[:-4].split("_")[1]]
    pred_col.set_index(["polygonlabel","se_sismo","sector"],inplace=True)

    for file_name in matched_files[1:]:
        pred_base=pd.read_csv(os.path.join(input_dir,file_name))
        pred_base.columns=["polygonlabel","se_sismo","sector",file_name[:-4].split("_")[-1]+"_"+file_name[:-4].split("_")[1]]
        pred_base.set_index(["polygonlabel","se_sismo","sector"],inplace=True)

        pred_col=pred_col.merge(pred_base,left_index=True,right_index=True,how="outer")

    pred_col.fillna(value=0,inplace=True)
    return pred_col.transpose()


def adapt_colname(feature_matrix):
    """
    Rename columns with the first row, and drop it.

    Params
    =======
    feature_matrix: pd.DataFrame, feature matrix to be used in models

    Return
    =======
    result:pd.DataFrame, a copy with informative column names
    """
    result = feature_matrix.copy()
    origcolname = list(feature_matrix.columns)
    suffix = [""] + ["."+s for s in list(feature_matrix.iloc[0,1:])]
    newcolname = [origcolname[i]+suffix[i] for i in range(len(origcolname))]
    result.columns = newcolname
    result.drop(0, inplace=True)
    return result
