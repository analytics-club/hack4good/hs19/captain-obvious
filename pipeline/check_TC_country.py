from src.utils.match_finder import get_country_start_end
from src.utils.subtasks_no_climada import get_all_files_with_prefix
import os
from ConfigurationClass import Configuration
from datetime import datetime
import pandas as pd
import numpy as np

config=Configuration()
data_path = config.param["DATA_DIRECTORY"]

gdacs_path = os.path.join(data_path, "gdacs_TC")
idmc_file = os.path.join(data_path,"idmc_gdacs_tc_dirty_with_displacement.csv")

#unique_events=pd.Series([x.split("_")[1] for x in os.listdir(gdacs_path)]).unique()

idmc_df = pd.read_csv(idmc_file,sep=",",header=[0])
idmc_df.drop("Unnamed: 0",inplace=True,axis=1)
idmc_df.set_index("idmc_index",inplace=True)
idmc_df.rename(columns = {'#country+code': "iso3",
                          '#country+name': "country",
                          '#date+year': 'year',
                          "#date+start": "fromdate",
                          "#affected+idps+ind+newdisp+disaster": "displacement",
                          "#crisis+type" : "eventtype",
                          "gdacs_event_id": "eventid"}, inplace=True)

idmc_df["eventid"]=idmc_df["eventid"].astype("float64")
idmc_df.to_csv(data_path+"idmc_tc_match.csv")




gdacs_path = os.path.join(data_path, "gdacs_FL")
idmc_file = os.path.join(data_path,"idmc_gdacs_fl_with_displacement.csv")

#unique_events=pd.Series([x.split("_")[1] for x in os.listdir(gdacs_path)]).unique()

idmc_df = pd.read_csv(idmc_file,sep=",",header=[0])
idmc_df.set_index("idmc_index",inplace=True)
idmc_df.rename(columns = {'#country+code': "iso3",
                          '#country+name': "country",
                          '#date+year': 'year',
                          "#date+start": "fromdate",
                          "#affected+idps+ind+newdisp+disaster": "displacement",
                          "#crisis+type" : "eventtype",
                          "gdacs_event_id": "eventid"}, inplace=True)

idmc_df.to_csv(os.path.join(data_path,"idmc_fl_match.csv"))


idmc_df.set_index(["iso3","eventid"],inplace=True)
#idmc_df.to_csv(os.path.join(data_path,"idmc_TC_match.csv"))


idmc_df_2=pd.read_csv(data_path+"additional_displacement_fl.csv")
idmc_df_2.drop("iso3",inplace=True,axis=1)
idmc_df_2.rename(columns = {'my_iso': "iso3",
                          "event_id": "eventid",
                            "displaced":"displacement"}, inplace=True)
idmc_df_2.drop(["url"],axis=1,inplace=True)
idmc_df_2.dropna(axis=0,inplace=True)
idmc_df_2.set_index(["iso3","eventid"],inplace=True)
idmc_df_2.sort_index(inplace=True)
idmc_df_merge=pd.merge(idmc_df,idmc_df_2,left_index=True,right_index=True,how="outer")
idmc_df_merge["displacement_x"].fillna(idmc_df_merge.loc[idmc_df_merge["displacement_x"].isna()]["displacement_y"],inplace=True)
idmc_df_merge["country_x"].fillna(idmc_df_merge.loc[idmc_df_merge["country_x"].isna()]["country_y"],inplace=True)

idmc_df_merge["country_x"].fillna(idmc_df_merge.loc[idmc_df_merge["country_x"].isna()]["country_y"],inplace=True
                                  )
idmc_df_merge.loc[idmc_df_merge["fromdate_x"].notna(),"fromdate_x"]=idmc_df_merge[idmc_df_merge["fromdate_x"].notna()]["fromdate_x"].apply(lambda x: x.split("-")[0])
idmc_df_merge.loc[idmc_df_merge["fromdate_y"].notna(),"fromdate_y"]=idmc_df_merge[idmc_df_merge["fromdate_y"].notna()]["fromdate_y"].apply(lambda x: x.split("/")[2].split(" ")[0])

idmc_df_merge["fromdate_x"].fillna(idmc_df_merge.loc[idmc_df_merge["fromdate_x"].isna()]["fromdate_y"],inplace=True)

idmc_df_merge.drop("year",inplace=True,axis=1)
idmc_df_merge.rename(columns={
    "displacement_x":"displacement","country_x":"country","fromdate_x":"year"
},inplace=True)
idmc_df_merge.drop(["event_type","todate","type","displacement_y","fromdate_y","event_type","country_y"],inplace=True,axis=1)
idmc_df_merge.to_csv(data_path+"idmc_final_fl.csv")


# def check_time_fit(event_start,event_end,x,lab):
#     if(event_start.month <= x.month and x.month <= event_end.month):
#         return lab
#     else:
#         return np.NAN
#
#

#
# for lab in unique_events:
#     country_list,start,end = get_country_start_end(get_all_files_with_prefix(lab))
#     event_start=datetime.strptime(start,"%d/%b/%Y %H:%M:%S").date()
#     event_end=datetime.strptime(start,"%d/%b/%Y %H:%M:%S").date()
#     for country in country_list:
#         if lab == "1000200":
#             continue
#         if country =="   ":
#             continue
#         else:
#             try:
#                 idmc_df.loc[(country,event_end.year),"eventid"]=idmc_df.loc[(country, event_end.year),"fromdate"].apply(lambda x: check_time_fit(event_start,event_end,x,lab))
#             except KeyError:
#                 print("no match")
#                 continue
idmc_res=pd.read_csv(data_path+"idmc_final_fl.csv")
idmc_res.drop(["latitude","longitude"],inplace=True,axis=1)
idmc_res["eventid"]=idmc_res["eventid"].astype("int64").astype("str")
idmc_feat=pd.read_csv(data_path+"final_out_FL_2.csv")
idmc_feat.drop(0,inplace=True)
idmc_res.set_index(["iso3","eventid"],inplace=True)
idmc_feat["eventid"]=idmc_feat["polygonlabel"].apply(lambda x: x.split("_")[0])
idmc_feat["iso3"]=idmc_feat["polygonlabel"].apply(lambda x: x.split("_")[1])
idmc_feat.set_index((["iso3","eventid"]),inplace=True)

idmc_feat.drop("polygonlabel",axis=1,inplace=True)

final_merge=pd.merge(idmc_res[["displacement","country","year"]],idmc_feat,left_index=True,right_index=True,how="inner")

final_merge.to_csv(data_path+"merged.csv")
final_merge.columns




